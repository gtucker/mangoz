/* Mangoz - include/mangoz/boost/mangoz-boost.h
   Copyright (C) 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MANGOZ_BOOST_H
#define INCLUDE_MANGOZ_BOOST_H 1

#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>
#include <iostream>

namespace mangoz {

extern "C" {
#include <mangoz/libmz/libmz.h>
};

  typedef std::vector<std::string> strvec;
  typedef boost::shared_ptr<std::string> shared_string;
  typedef boost::shared_ptr<strvec> shared_strvec;

  class Printer{
  public:
    virtual void clear() = 0;
    virtual int produce(const char *str, size_t len) = 0;
    virtual const size_t length() = 0;
  };

  class StreamPrinter: public Printer {
  public:
    StreamPrinter(std::ostream &out);
    virtual ~StreamPrinter();

    virtual void clear();
    virtual int produce(const char *str, size_t len);
    virtual const size_t length();

  private:
    std::ostream &out;
    size_t total_length;
  };

  class StringPrinter: public Printer {
  public:
    StringPrinter();
    virtual ~StringPrinter();

    virtual void clear();
    virtual int produce(const char *str, size_t len);
    virtual const size_t length();
    virtual const std::string &getString();

  private:
    std::string str;
  };

  class Xnode {
  public:
    virtual const mz_xnode_type getNodeType() = 0;
    virtual const int produce(Printer &printer) = 0;
  };

  class Xtext: public Xnode {
  public:
    Xtext(const std::string &text, bool do_format=true);
    virtual ~Xtext();

    virtual const mz_xnode_type getNodeType();
    virtual const int produce(Printer &printer);
    virtual bool setText(const std::string &text);
    virtual const std::string getText();
    virtual const shared_string getSharedText();
    virtual const shared_strvec getSplitText();

  private:
    mz_xtext xtext;
  };

  class Xhtml: public Xnode {
  public:
    Xhtml(const std::string &tag);
    virtual ~Xhtml();

    virtual const mz_xnode_type getNodeType();
    virtual const int produce(Printer &printer);
    virtual shared_string getTag();

  private:
    struct mz_xhtml xhtml;
  };

} // namespace mangoz

#endif // INCLUDE_MANGOZ_BOOST_H

/* Mangoz - include/mangoz/tests/log.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MZ_TEST_LOG_H
#define INCLUDE_MZ_TEST_LOG_H 1

#include <stdio.h>

#ifndef LOG_FILE
# define LOG_FILE stderr
#endif

#ifdef LOG_TAG
# define LOG_PRINT(...) fprintf(LOG_FILE, ##__VA_ARGS__)
#else
# define LOG_PRINT(...)
#endif

# define LOG_N(msg, ...) \
	LOG_PRINT("[%-8s %4d] "msg, LOG_TAG, __LINE__, ##__VA_ARGS__)

#define LOG(msg, ...) \
	LOG_N(msg"\n", ##__VA_ARGS__)

#endif /* INCLUDE_MZ_TEST_LOG_H */

/* Mangoz - include/mangoz/libmz/private.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MZ_LIBMZ_PRIVATE_H
#define INCLUDE_MZ_LIBMZ_PRIVATE_H 1

#include "libmz.h"

extern int mz_make_indent_str(struct mz_string *s, size_t indent);

#endif /* INCLUDE_MZ_LIBMZ_PRIVATE_H */

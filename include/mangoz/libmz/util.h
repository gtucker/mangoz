/* Mangoz - include/mangoz/libmz/util.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MZ_UTIL_H
#define INCLUDE_MZ_UTIL_H 1

#include <stdlib.h>

#define MZE(cmd) do {			\
		const int res = (cmd);	\
		if (res < 0)		\
			return res;	\
	} while (0)

#define MZX(x, cmd) do {				\
		const int res = (cmd);			\
		if (res < 0) {				\
			cleanup(x);			\
			return res;			\
		}					\
	} while (0)

#define MZCB(cmd) do {					\
		const int res = (cmd);			\
		if (res < 0) {				\
			mz_callback_error = res;	\
			return -MZ_ECALLBACK;		\
		}					\
	} while (0)

#ifndef unlikely
# define unlikely(expr) __builtin_expect(expr, 0)
#endif

#ifndef likely
# define likely(expr) __builtin_expect(expr, 1)
#endif

#endif /* INCLUDE_MZ_UTIL_H */

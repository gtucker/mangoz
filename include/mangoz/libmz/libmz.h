/* Mangoz - include/mangoz/libmz/libmz.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_LIBMZ_H
#define INCLUDE_LIBMZ_H 1

#include <stdlib.h>
#include <string.h>

#define MZ_OFFSET(s, m) ((unsigned long) &((const struct s *) (0))->m)
#define MZ_GET_PARENT(x, s, m) ((void *) x - MZ_OFFSET(s, m))
#define MZ_STRLEN(str) str, (sizeof(str) - 1)
#define MZ_CSTRLEN(str) (sizeof(str) - 1)
#define MZ_ARRAY_SZ(array) (sizeof(array) / sizeof(array[0]))
#define MZ_ARRAY_END(array) (&array[MZ_ARRAY_SZ(array)])

#define MZ_CLEAR(x) do {				\
		(x).init = false;			\
	} while (0)

#define MZ_CLEAR_PTR(px) do {			\
		(px)->init = false;			\
	} while (0)


/* Set to 1 to enable logging related to object management */
#define MZ_LOG_ALLOC 0

/* ToDo: const char *MZ_INDENT = "  "; */
#define INDENT "  "

#ifndef __cplusplus
enum _mz_bool {
	false = 0,
	true = 1,
};
#endif /* not c++ */
typedef unsigned char mz_bool;

enum mz_error {
	MZ_EALLOC = 1, /* not enough memory */
	MZ_ENOTFOUND,  /* object not found */
	MZ_EINDEX,     /* index out of bounds */
	MZ_EIO,        /* I/O error */
	MZ_ESTD,       /* libc error (check errno or dedicated API) */
	MZ_ECALLBACK,  /* callback error */
	MZ_EXML,       /* XML handling error */
};

extern const char *mz_strerror(enum mz_error error);

extern void *(*mz_malloc)(size_t size);
extern void *(*mz_realloc)(void *ptr, size_t size);
extern void (*mz_free)(void *ptr);


/* -- contiguous vector -- */

struct mz_vector {
	void *data;
	const void *end;
	size_t n;
	size_t length;
	const size_t object_size;
};

extern int mz_vector_init(struct mz_vector *v, size_t object_size,
			  size_t init_length);
extern void mz_vector_free(struct mz_vector *v);
extern void mz_vector_clear(struct mz_vector *v);
extern int mz_vector_append(struct mz_vector *v, const void *o);
extern int mz_vector_insert(struct mz_vector *v, const void *o, int index);
extern void *mz_vector_alloc_end(struct mz_vector *v, size_t n);
#if 0
extern void *mz_vector_alloc_at(struct mz_vector *v, size_t n, int index);
#endif
extern int mz_vector_remove(struct mz_vector *v, int index);
extern int mz_vector_remove_ptr(struct mz_vector *v, const void *ptr);
extern void *mz_vector_get(const struct mz_vector *v, int index);

#define mz_vector_for_each(v, it)			\
	for (it = (v).data; it != (v).end; ++it)

#define mz_vector_for_each_n(v, it, i)			\
	for (it = (v).data, i = 0; it != (v).end; ++it, ++i)

#define mz_vector_for_each_p(v, it)			\
	for (it = (v)->data; it != (v)->end; ++it)

#define mz_vector_for_each_pn(v, it, i)			\
	for (it = (v)->data, i = 0; it != (v)->end; ++it, ++i)


/* -- string -- */

#define MZ_USE_SNPRINTF 0

struct mz_string {
	char *str;
	size_t len; /* That's the number of bytes, not Unicode characters */
	size_t n;
	/* ToDo: mz_bool const; for static const string data optimisation */
};

extern int mz_string_init(struct mz_string *s, size_t size);
extern void mz_string_free(struct mz_string *s);
extern void mz_string_clear(struct mz_string *s);
extern int mz_string_set(struct mz_string *s, const char *str, size_t len);
#define mz_string_set_nolen(s, str) mz_string_set(s, str, strlen(str))
extern int mz_string_cat(struct mz_string *s, const char *str, size_t len);
#define mz_string_cat_nolen(s, str) mz_string_cat(s, str, strlen(str))
extern int mz_string_cat_str(struct mz_string *s, const struct mz_string *z);
extern int mz_string_cat_html(struct mz_string *s, const char *str);
extern int mz_string_cat_file(struct mz_string *s, const char *file_name);
#if MZ_USE_SNPRINTF
extern int mz_string_printf(struct mz_string *s, const char *fmt, ...);
#endif
extern int mz_string_cmp(const struct mz_string *s, const char *z, size_t len);
#define mz_string_cmp_nolen(s, z) mz_string_cmp(s, z, strlen(z))
extern int mz_string_cmp_str(const struct mz_string *s,
			     const struct mz_string *z);
extern mz_bool mz_string_is_whitespace(const char *str, size_t len);
extern void mz_string_get_trimmed(
	const char *str, size_t str_len, const char **tmd, size_t *tmd_len);
extern void mz_string_get_trimmed_str(
	const struct mz_string *s, const char **tmd, size_t *tmd_len);
extern int mz_string_split(const struct mz_string *s, struct mz_vector *v,
			   char sep);
extern mz_bool mz_string_to_bool(const struct mz_string *s);


/* -- attributes -- */

struct mz_attribute {
	struct mz_string name;
	struct mz_string value;
};

extern const struct mz_string *mz_attribute_get(
	const struct mz_vector *atts, const char *name, size_t nlen);
extern int mz_attribute_set(struct mz_vector *atts, const char *name,
			    size_t nlen, const char *value, size_t vlen);
extern int mz_attribute_set_default(
	struct mz_vector *atts, const char *name, size_t nlen,
	const char *value, size_t vlen);


/* -- xnode -- */

enum mz_xnode_type {
	MZ_XNODE_TEXT = 1,
	MZ_XNODE_HTML = 2,
};

struct mz_xnode {
	enum mz_xnode_type node_type;
	mz_bool do_format;
	mz_bool init;
};

extern int mz_xnode_init(struct mz_xnode *x, enum mz_xnode_type node_type,
			 mz_bool do_format);
extern void mz_xnode_free(struct mz_xnode *x);


/* -- xtext -- */

struct mz_xtext {
	struct mz_xnode xnode;
	struct mz_string text;
	struct mz_vector split;
};

extern int mz_xtext_init(struct mz_xtext *x, const char *text, size_t len,
			 mz_bool do_format);
extern void mz_xtext_free(struct mz_xtext *x);
extern int mz_xtext_set(struct mz_xtext *x, const char *text, size_t len);
extern int mz_xtext_set_html(struct mz_xtext *x, const char *text);
extern int mz_xtext_set_from_file(struct mz_xtext *x, const char *file_name);
extern mz_bool mz_xtext_equals(struct mz_xtext *x, struct mz_xtext *y);
extern int mz_xtext_do_produce(struct mz_xtext *x, void *printer,
			       unsigned indent);
extern int mz_xtext_do_produce_single(struct mz_xtext *x, void *printer,
				      unsigned indent);


/* -- xhtml -- */

struct mz_xhtml {
	struct mz_xnode xnode;
	struct mz_string tag;
	struct mz_vector atts;
	struct mz_vector nodes;
	struct mz_string out;
	mz_bool do_short;
};

extern int mz_xhtml_init(struct mz_xhtml *x, const char *tag, size_t tag_len,
			 mz_bool do_format, mz_bool do_short);
extern void mz_xhtml_free(struct mz_xhtml *x);
extern struct mz_xtext *mz_xhtml_add_text(
	struct mz_xhtml *x, const char *str, size_t len, mz_bool do_format);
extern int mz_xhtml_set_text(struct mz_xhtml *x, const char *str, size_t len);
extern struct mz_xtext *mz_xhtml_get_text_node(struct mz_xhtml *x, unsigned i);
extern struct mz_xhtml *mz_xhtml_add_leaf(
	struct mz_xhtml *x, const char *tag, size_t tag_len, const char *text,
	size_t text_len, mz_bool do_format, mz_bool do_short);
extern struct mz_xhtml *mz_xhtml_get_indexed_leaf(
	struct mz_xhtml *x, const char *tag, size_t tag_len, size_t index);
extern struct mz_xhtml *mz_xhtml_get_leaf(
	struct mz_xhtml *x, const char *tag, size_t tag_len,
	const char *att_name, size_t att_name_len,
	const char *att_value, size_t att_value_len);
extern int mz_xhtml_import_node(struct mz_xhtml *x, struct mz_xnode *node);
extern int mz_xhtml_remove_node(struct mz_xhtml *x, struct mz_xnode *node);
extern unsigned long mz_xhtml_count_nodes(
	struct mz_xhtml *x, enum mz_xnode_type node_type);
extern mz_bool mz_xhtml_equals(struct mz_xhtml *x, struct mz_xhtml *y);
extern int mz_xhtml_do_produce(struct mz_xhtml *x, void *printer,
			       unsigned indent, mz_bool root_format);


/* -- xdoc -- */

enum mz_dtd {
	MZ_DTD_XHTML11 = 1,
	MZ_DTD_STRICT,
	MZ_DTD_TRANSITIONAL,
	MZ_DTD_FRAMESET,
	MZ_DTD_HTML,
};

struct mz_doctype {
	enum mz_dtd dtd;
	char *header;
	size_t len;
};

struct mz_xdoc {
	struct mz_xhtml *root;
	struct mz_vector atts;
	struct mz_string header;
	const struct mz_doctype *doctype;
	mz_bool init;
};

extern int mz_xdoc_init(struct mz_xdoc *x, struct mz_xhtml *root);
extern void mz_xdoc_free(struct mz_xdoc *x);
extern int mz_xdoc_set_std_doctype(struct mz_xdoc *x, enum mz_dtd dtd);
extern const struct mz_doctype *mz_xdoc_get_std_doctype(enum mz_dtd dtd);
extern int mz_xdoc_set_header(struct mz_xdoc *x, const char *hdr, size_t len);
extern int mz_xdoc_do_produce(struct mz_xdoc *x, void *printer, int indent);


/* -- xparser -- */

typedef mz_bool (*mz_handle_format_t)(void *, struct mz_xhtml *, mz_bool);

struct mz_xparser {
	mz_handle_format_t format;
	void *ctx;
	mz_bool init;
};

extern int mz_xparser_init(struct mz_xparser *x, mz_handle_format_t format,
			   void *ctx);
extern void mz_xparser_free(struct mz_xparser *x);
extern mz_bool mz_std_handle_format(void *ctx, struct mz_xhtml *x, mz_bool f);
extern mz_bool mz_tree_handle_format(void *ctx, struct mz_xhtml *x, mz_bool f);
extern mz_bool mz_tb_handle_format(void *ctx, struct mz_xhtml *x, mz_bool f);
extern int mz_xparser_parse_file(
	struct mz_xparser *xparser, struct mz_xdoc *xdoc,
	const char *file_name, mz_bool do_format);
extern int mz_xparser_parse_string(
	struct mz_xparser *xparser, struct mz_xdoc *xdoc,
	const char *str, size_t len, mz_bool do_format);


/* -- Request / response protocol -- */

#define MZ_QUERY_LANG "mz_lang"
#define MZ_QUERY_TARGET "mz_target"
#define MZ_QUERY_TEMPLATE "mz_template"
#define MZ_QUERY_TITLE "mz_title"
#define MZ_QUERY_ADD "mz_add"
#define MZ_QUERY_MOVE "mz_move"
#define MZ_QUERY_INDEX "mz_index"
#define MZ_QUERY_BACK "mz_back"


/* -- file monitor -- */

enum mz_monitor_msg {
	MZ_MON_RUNNING,
	MZ_MON_STOPPED,
	MZ_MON_ERROR,
	MZ_MON_FILE,
};

struct mz_monitor_event {
	enum mz_monitor_msg msg;
	void *ctx;
	const struct mz_string *file_name;
	void *file_ctx;
};

typedef int (*mz_monitor_callback_t)(const struct mz_monitor_event *ev);

struct mz_monitor;

extern int mz_monitor_init(struct mz_monitor **p, mz_monitor_callback_t cb,
			   void *ctx);
extern void mz_monitor_free(struct mz_monitor *m);
extern int mz_monitor_get_errno(const struct mz_monitor *m);
extern int mz_monitor_add_file(struct mz_monitor *m, const char *file_name,
			       size_t len, void *file_ctx);
extern int mz_monitor_remove_file(struct mz_monitor *m, const char *file_name,
				  size_t len);
extern int mz_monitor_run(struct mz_monitor *m);
extern int mz_monitor_cancel(struct mz_monitor *m);


/* -- configuration -- */

struct mz_core_config {
	mz_bool valid;
	struct mz_config *config;
	const struct mz_string *dtd;
	enum mz_dtd dtd_id;
	const struct mz_string *defaultLanguage;
	const struct mz_string *staticDir;
	const struct mz_string *baseURL;
	const struct mz_string *pages;
	const struct mz_string *textbox;
	const struct mz_string *useSession;
	mz_bool do_use_session;
};

struct mz_page_config {
	mz_bool valid;
	struct mz_config *config;
	const struct mz_string *defaultLanguage;
	const struct mz_string *charset;
	const struct mz_string *defaultIcon;
	const struct mz_string *baseURL;
	const struct mz_string *publicURL;
	const struct mz_string *defaultCSS;
	const struct mz_string *contentType;
	struct mz_vector css_list;     /* mz_string */
};

struct mz_config {
	struct mz_vector opt;          /* mz_attribute */
	struct mz_core_config core;
	struct mz_page_config page;
	mz_bool init;
};

extern int mz_config_init(struct mz_config *c);
extern void mz_config_free(struct mz_config *c);
#define mz_config_invalidate(c) do {		\
		(c)->core.valid = false;	\
		(c)->page.valid = false;	\
	} while (0)
#define mz_config_set(c, name, value)					\
	 mz_attribute_set(&(c)->opt, MZ_STRLEN(name), MZ_STRLEN(value))
#define mz_config_get(c, name)						\
	mz_attribute_get(&(c)->opt, MZ_STRLEN(name))
extern const struct mz_core_config *mz_config_get_core(struct mz_config *c);
extern const struct mz_page_config *mz_config_get_page(struct mz_config *c);


/* - Callbacks to be populated by client code */

struct mz_callbacks {
	int (*printer_produce)(void *printer, const char *str, size_t len);
	struct mz_xtext *(*create_xtext)(const char *text, size_t len,
					 mz_bool do_format);
	struct mz_xhtml *(*create_xhtml)(const char *tag, size_t len,
					 mz_bool do_format, mz_bool do_short);
};

extern const struct mz_callbacks mz_callbacks;
extern int mz_callback_error;

#endif /* INCLUDE_LIBMZ_H */

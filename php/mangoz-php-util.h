/* Mangoz - php/mangoz-php-util.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MANGOZ_PHP_UTIL
#define INCLUDE_MANGOZ_PHP_UTIL 1

#include <php.h>

#define MZ_RETURN_STRING_CONST(s) do {				\
	size_t len = strlen(s);					\
	(return_value)->value.str.len = len;			\
	(return_value)->value.str.val = estrndup(s, len);	\
	(return_value)->type = IS_STRING;			\
	return;							\
} while (0)

#define MZ_RETURN_STRING_CONST_L(s, l) do {			\
	(return_value)->value.str.len = (l);			\
	(return_value)->value.str.val = estrndup((s), (l));	\
	(return_value)->type = IS_STRING;			\
	return;							\
} while (0)

#define MZ_GET_OBJECT(o, z) do {				\
	o = zend_object_store_get_object((z) TSRMLS_CC);	\
	if (o == NULL)						\
		RETURN_FALSE;					\
} while (0)

#define MZ_GET_THIS(o) MZ_GET_OBJECT(o, getThis())

#define MZ_PARSE(s, ...) do {					\
	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,	\
				  s, ##__VA_ARGS__) == FAILURE)	\
		RETURN_FALSE;					\
} while (0)

#if PHP_API_VERSION > 20041225
# define MZ_ZVAL_ADDREF(zvalp) zval_addref_p(zvalp)
# define MZ_ZVAL_INITREF(zvalp) do {            \
		z->is_ref__gc = 1;		\
		z->refcount__gc = 1;		\
	} while (0)
#else
# define MZ_ZVAL_ADDREF(zvalp) ZVAL_ADDREF(zvalp)
# define MZ_ZVAL_INITREF(zvalp) do {		\
		z->is_ref = 1;			\
		z->refcount = 1;		\
	} while (0)
#endif

#define MZ_NEITHER ((zend_bool)(-1)) /* neither true nor false */

extern void mz_init_zend_object(zend_object *std, zend_class_entry *ce);
extern void mz_free_zend_object(zend_object *std);

#endif /* INCLUDE_MANGOZ_PHP_UTIL */

/* Mangoz - php/mangoz-php-util.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-php-util.h"

void mz_init_zend_object(zend_object *std, zend_class_entry *ce)
{
	void *tmp;

	std->ce = ce;
	ALLOC_HASHTABLE(std->properties);
	zend_hash_init(std->properties, 0, NULL, ZVAL_PTR_DTOR, 0);
	zend_hash_copy(std->properties, &ce->default_properties,
		       (copy_ctor_func_t) zval_add_ref, &tmp, sizeof (void *));
}

void mz_free_zend_object(zend_object *std)
{
	zend_hash_destroy(std->properties);
	FREE_HASHTABLE(std->properties);
}

/* Mangoz - php/MzPrinter.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-php.h"
#include "mangoz-php-util.h"
#include <mangoz/libmz/libmz.h>

/* ----------------------------------------------------------------------------
 * MzPrinter
 * Interface for all the Printer implementations
 */

static zend_class_entry *MzPrinter_ce;
static zend_object_handlers MzPrinter_oh;

static function_entry MzPrinter_methods[] = {
	PHP_ABSTRACT_ME(MzPrinter, clear,   NULL)
	PHP_ABSTRACT_ME(MzPrinter, produce, NULL)
	PHP_ABSTRACT_ME(MzPrinter, length,  NULL)
	{ NULL, NULL, NULL }
};

void mangoz_MzPrinter_init(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "MzPrinter", MzPrinter_methods);
	MzPrinter_ce = zend_register_internal_class(&ce TSRMLS_CC);
	memcpy(&MzPrinter_oh, zend_get_std_object_handlers(),
	       sizeof(zend_object_handlers));
	MzPrinter_oh.clone_obj = NULL;
}

/* ----------------------------------------------------------------------------
 * MzStdPrinter
 * Printer to stdout
 */

struct MzStdPrinter_object {
	zend_object std;
	size_t len;
};
typedef struct MzStdPrinter_object MzStdPrinter;

static zend_class_entry *MzStdPrinter_ce;
static zend_object_handlers MzStdPrinter_oh;

PHP_METHOD(MzStdPrinter, __construct)
{
	MzStdPrinter *o;

	MZ_GET_THIS(o);
	o->len = 0;
}

PHP_METHOD(MzStdPrinter, clear)
{
	MzStdPrinter *o;

	MZ_GET_THIS(o);
	o->len = 0;
}

PHP_METHOD(MzStdPrinter, produce)
{
	MzStdPrinter *o;
	const char *data;
	int data_len;

	MZ_GET_THIS(o);
	MZ_PARSE("s", &data, &data_len);
	o->len += data_len;
	php_printf(data);
}

PHP_METHOD(MzStdPrinter, length)
{
	MzStdPrinter *o;

	MZ_GET_THIS(o);
	RETURN_LONG(o->len);
}

static function_entry MzStdPrinter_methods[] = {
	PHP_ME(MzStdPrinter, __construct, NULL, ZEND_ACC_PUBLIC |ZEND_ACC_CTOR)
	PHP_ME(MzStdPrinter, clear,       NULL, ZEND_ACC_PUBLIC)
	PHP_ME(MzStdPrinter, produce,     NULL, ZEND_ACC_PUBLIC)
	PHP_ME(MzStdPrinter, length ,     NULL, ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

static void MzStdPrinter_free(void *p TSRMLS_DC)
{
	MzStdPrinter *o = p;

	mz_free_zend_object(&o->std);
	efree(o);
}

static zend_object_value MzStdPrinter_create(zend_class_entry *ce TSRMLS_DC)
{
	MzStdPrinter *o;
	zend_object_value ret;

	o = emalloc(sizeof(MzStdPrinter));
	mz_init_zend_object(&o->std, ce);
	ret.handle = zend_objects_store_put(o, NULL, MzStdPrinter_free,
					    NULL TSRMLS_CC);
	ret.handlers = &MzStdPrinter_oh;

	return ret;
}

void mangoz_MzStdPrinter_init(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "MzStdPrinter", MzStdPrinter_methods);
	MzStdPrinter_ce = zend_register_internal_class(&ce TSRMLS_CC);
	MzStdPrinter_ce->create_object = MzStdPrinter_create;
	MzStdPrinter_ce->parent = MzPrinter_ce;
	memcpy(&MzStdPrinter_oh, zend_get_std_object_handlers(),
	       sizeof(zend_object_handlers));
	MzStdPrinter_oh.clone_obj = NULL;
}

/* ----------------------------------------------------------------------------
 * MzStringPrinter
 * Printer to a string
 */

struct MzStringPrinter_object {
	zend_object std;
	struct mz_string string;
};
typedef struct MzStringPrinter_object MzStringPrinter;

static zend_class_entry *MzStringPrinter_ce;
static zend_object_handlers MzStringPrinter_oh;

PHP_METHOD(MzStringPrinter, __construct)
{
	MzStringPrinter *o;

	MZ_GET_THIS(o);

	if (mz_string_init(&o->string, 0) < 0)
		RETURN_NULL();
}

PHP_METHOD(MzStringPrinter, clear)
{
	MzStringPrinter *o;

	MZ_GET_THIS(o);
	mz_string_clear(&o->string);
}

PHP_METHOD(MzStringPrinter, produce)
{
	MzStringPrinter *o;
	const char *data;
	int data_len;

	MZ_GET_THIS(o);
	MZ_PARSE("s", &data, &data_len);

	if (mz_string_cat(&o->string, data, data_len) < 0)
		RETURN_FALSE; /* ToDo: throw exception */
}

PHP_METHOD(MzStringPrinter, length)
{
	MzStringPrinter *o;

	MZ_GET_THIS(o);
	RETURN_LONG(o->string.len);
}

PHP_METHOD(MzStringPrinter, getString)
{
	MzStringPrinter *o;

	MZ_GET_THIS(o);
	MZ_RETURN_STRING_CONST_L(o->string.str, o->string.len);
}

static function_entry MzStringPrinter_methods[] = {
	PHP_ME(MzStringPrinter, __construct, NULL, (ZEND_ACC_PUBLIC |
						    ZEND_ACC_CTOR))
	PHP_ME(MzStringPrinter, clear,       NULL, ZEND_ACC_PUBLIC)
	PHP_ME(MzStringPrinter, produce,     NULL, ZEND_ACC_PUBLIC)
	PHP_ME(MzStringPrinter, length ,     NULL, ZEND_ACC_PUBLIC)
	PHP_ME(MzStringPrinter, getString,   NULL, ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

static void MzStringPrinter_free(void *p TSRMLS_DC)
{
	MzStringPrinter *o = p;

	mz_string_free(&o->string);
	zend_hash_destroy(o->std.properties);
	FREE_HASHTABLE(o->std.properties);
	efree(o);
}

static zend_object_value MzStringPrinter_create(zend_class_entry *ce TSRMLS_DC)
{
	MzStringPrinter *o;
	zend_object_value ret;

	o = emalloc(sizeof(MzStringPrinter));
	mz_init_zend_object(&o->std, ce);
	ret.handle =
		zend_objects_store_put(o, NULL, MzStringPrinter_free,
				       NULL TSRMLS_CC);
	ret.handlers = &MzStringPrinter_oh;

	return ret;
}

void mangoz_MzStringPrinter_init(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "MzStringPrinter", MzStringPrinter_methods);
	MzStringPrinter_ce = zend_register_internal_class(&ce TSRMLS_CC);
	MzStringPrinter_ce->create_object = MzStringPrinter_create;
	MzStringPrinter_ce->parent = MzPrinter_ce;
	memcpy(&MzStringPrinter_oh, zend_get_std_object_handlers(),
	       sizeof(zend_object_handlers));
	MzStringPrinter_oh.clone_obj = NULL;
}

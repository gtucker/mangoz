/* Mangoz - php/Xnode.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-php.h"
#include "mangoz-php-util.h"
#include "callbacks.h"
#include <mangoz/libmz/libmz.h>

/* ToDo: convert to runtime flags */
#define XNODE_LOG_ALLOC 0

#define MZ_XNODE_LOG(msg, ...)						\
	fprintf(stderr, "[Xnode    %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

static struct mz_xnode *extract_xnode(zval *z);
static zend_object_handle get_xnode_handle(struct mz_xnode *xnode);
static void make_xnode_ref(zval *z, struct mz_xnode *xnode);

/* ----------------------------------------------------------------------------
 * Xtext
 */

#define MZ_XTEXT_LOG(msg, ...)						\
	fprintf(stderr, "[Xtext    %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

struct Xtext_object {
	zend_object std;
	struct mz_xtext xtext;
	zend_object_handle handle;
};
typedef struct Xtext_object Xtext;

static zend_class_entry *Xtext_ce;
static zend_object_handlers Xtext_oh;
static unsigned long Xtext_counter = 0;

static Xtext *Xtext_new(zend_class_entry *ce);
static zend_object_value Xtext_register(Xtext *o);
static void Xtext_make_ref(zval *z, struct mz_xtext *x);

PHP_METHOD(Xtext, __construct)
{
	Xtext *o;
	char *text;
	int text_len;
	zend_bool do_format = true;

	MZ_GET_THIS(o);
	MZ_PARSE("s|b", &text, &text_len, &do_format);
#if XNODE_LOG_ALLOC
	MZ_XTEXT_LOG("%p construct (%s)", &o->xtext, text);
#endif
	if (mz_xtext_init(&o->xtext, text, text_len, do_format) < 0)
		RETURN_NULL();
}

PHP_METHOD(Xtext, setText)
{
	Xtext *o;
	const char *text;
	int text_len;

	MZ_GET_THIS(o);
	MZ_PARSE("s", &text, &text_len);

	if (mz_xtext_set(&o->xtext, text, text_len) < 0)
		RETURN_FALSE; /* ToDo: throw exception */
}

PHP_METHOD(Xtext, getText)
{
	Xtext *o;

	MZ_GET_THIS(o);
	MZ_RETURN_STRING_CONST_L(o->xtext.text.str, o->xtext.text.len);
}

PHP_METHOD(Xtext, getSplitText)
{
	Xtext *o;
	struct mz_string *line;

	MZ_GET_THIS(o);
	array_init(return_value);

	mz_vector_for_each(o->xtext.split, line) {
		zval *zline;

		/* ToDo: add_next_index_stringl ? */
		MAKE_STD_ZVAL(zline);
		zline->value.str.len = line->len;
		zline->value.str.val = estrndup(line->str, line->len);
		zline->type = IS_STRING;
		add_next_index_zval(return_value, zline);
	}
}

PHP_METHOD(Xtext, equals)
{
	Xtext *o;
	Xtext *other;
	zval *z;

	MZ_GET_THIS(o);
	MZ_PARSE("o", &z);

	if (z->value.obj.handlers != &Xtext_oh)
		RETURN_FALSE;

	other = zend_object_store_get_object(z);

	if (!mz_xtext_equals(&o->xtext, &other->xtext))
		RETURN_FALSE;

	RETURN_TRUE;
}

PHP_METHOD(Xtext, getNodeType)
{
	RETURN_LONG(MZ_XNODE_TEXT);
}

PHP_METHOD(Xtext, produce)
{
	Xtext *o;
	zval *printer;

	MZ_GET_THIS(o);
	MZ_PARSE("o", &printer);

	if (mz_xtext_do_produce(&o->xtext, printer, 0) < 0)
		RETURN_NULL();
}

static function_entry Xtext_methods[] = {
	PHP_ME(Xtext, __construct,       NULL, ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(Xtext, setText,           NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xtext, getText,           NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xtext, getSplitText,      NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xtext, equals,            NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xtext, getNodeType,       NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xtext, produce,           NULL, ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

static void Xtext_free(void *p TSRMLS_DC)
{
	Xtext *o = p;

	--Xtext_counter;
#if XNODE_LOG_ALLOC
	MZ_XTEXT_LOG("%p free [%lu]", &o->xtext, Xtext_counter);
#endif
	mz_free_zend_object(&o->std);
	mz_xtext_free(&o->xtext);
	efree(o);
}

static zend_object_value Xtext_create(zend_class_entry *ce TSRMLS_DC)
{
	return Xtext_register(Xtext_new(ce));
}

unsigned long mangoz_Xtext_count(void)
{
	return Xtext_counter;
}

void mangoz_Xtext_init(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "Xtext", Xtext_methods);
	Xtext_ce = zend_register_internal_class(&ce TSRMLS_CC);
	Xtext_ce->create_object = Xtext_create;
	memcpy(&Xtext_oh, zend_get_std_object_handlers(),
	       sizeof(zend_object_handlers));
	Xtext_oh.clone_obj = NULL;
}

/* - Xtext static functions - */

static Xtext *Xtext_new(zend_class_entry *ce)
{
	Xtext *o;

	o = emalloc(sizeof(Xtext));
	++Xtext_counter;
#if XNODE_LOG_ALLOC
	MZ_XTEXT_LOG("%p new [%lu]", &o->xtext, Xtext_counter);
#endif
	mz_init_zend_object(&o->std, ce);
	MZ_CLEAR(o->xtext.xnode);

	return o;
}

static zend_object_value Xtext_register(Xtext *o)
{
	zend_object_value ret;

	ret.handle = zend_objects_store_put(o, NULL, Xtext_free,
					    NULL TSRMLS_CC);
	ret.handlers = &Xtext_oh;
	o->handle = ret.handle;

#if XNODE_LOG_ALLOC
	MZ_XTEXT_LOG("%p register handle %d", &o->xtext, o->handle);
#endif

	return ret;
}

static void Xtext_make_ref(zval *z, struct mz_xtext *x)
{
	Xtext *o;

	o = MZ_GET_PARENT(x, Xtext_object, xtext);
	z->type = IS_OBJECT;
	MZ_ZVAL_INITREF(z);
	z->value.obj.handle = o->handle;
	z->value.obj.handlers = &Xtext_oh;
	zend_objects_store_add_ref_by_handle(o->handle);
}

/* ----------------------------------------------------------------------------
 * Xhtml
 */

#define MZ_GET_XHTML_INIT(o) do {		\
		MZ_GET_THIS(o);			\
		if (!o->xhtml.xnode.init)	\
			RETURN_FALSE;		\
	} while (0)

#define MZ_XHTML_LOG(msg, ...) do {					\
		if (mangoz_logging & MZ_LOG_XHTML_FLAG)			\
			fprintf(stderr, "[Xhtml    %4i] "msg"\n",	\
				__LINE__, ##__VA_ARGS__);		\
	} while (0)

struct Xhtml_object {
	zend_object std;
	struct mz_xhtml xhtml;
	zend_object_handle handle;
};
typedef struct Xhtml_object Xhtml;

static zend_class_entry *Xhtml_ce;
static zend_object_handlers Xhtml_oh;
static unsigned long Xhtml_counter = 0;

static Xhtml *Xhtml_new(zend_class_entry *ce);
static zend_object_value Xhtml_register(Xhtml *o);
static void Xhtml_make_ref(zval *z, struct mz_xhtml *x);

PHP_METHOD(Xhtml, __construct)
{
	const char *tag;
	int ntag;
	const char *text = NULL;
	int ntext = 0;
	zend_bool do_format = true;
	zend_bool do_short = true;
	Xhtml *o;

	MZ_GET_THIS(o);
	MZ_PARSE("s|sbb", &tag, &ntag, &text, &ntext, &do_format, &do_short);
#if XNODE_LOG_ALLOC
	MZ_XHTML_LOG("%p construct (%s)", &o->xhtml, tag);
#endif

	if (mz_xhtml_init(&o->xhtml, tag, ntag, do_format, do_short) < 0)
		RETURN_NULL();

	if (text != NULL)
		if (mz_xhtml_add_text(&o->xhtml, text, ntext, do_format) < 0)
			RETURN_NULL();
}

PHP_METHOD(Xhtml, getTag)
{
	Xhtml *o;

	MZ_GET_XHTML_INIT(o);
	MZ_RETURN_STRING_CONST_L(o->xhtml.tag.str, o->xhtml.tag.len);
}

PHP_METHOD(Xhtml, setAttribute)
{
	Xhtml *o;
	const char *name;
	int name_len;
	const char *value;
	int value_len;
	int res;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("ss", &name, &name_len, &value, &value_len);

	res = mz_attribute_set(&o->xhtml.atts, name, name_len,
			       value, value_len);

	if (res < 0) {
		fprintf(stderr, "ERROR: setAttribute (%i)\n", res);
		RETURN_NULL();
	}

	Xhtml_make_ref(return_value, &o->xhtml);
}

PHP_METHOD(Xhtml, setId)
{
	Xhtml *o;
	const char *id;
	int id_len;
	int res;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("s", &id, &id_len);

	res = mz_attribute_set(&o->xhtml.atts, MZ_STRLEN("id"), id, id_len);

	if (res < 0)
		RETURN_NULL();
}

PHP_METHOD(Xhtml, setClass)
{
	Xhtml *o;
	const char *class;
	int class_len;
	int res;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("s", &class, &class_len);

	res = mz_attribute_set(&o->xhtml.atts, MZ_STRLEN("class"),
			       class, class_len);

	if (res < 0)
		RETURN_NULL();
}

PHP_METHOD(Xhtml, getAttribute)
{
	Xhtml *o;
	const char *name;
	int name_len;
	const struct mz_string *value;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("s", &name, &name_len);

	value = mz_attribute_get(&o->xhtml.atts, name, name_len);

	if (value == NULL)
		RETURN_NULL();

	MZ_RETURN_STRING_CONST_L(value->str, value->len);
}

PHP_METHOD(Xhtml, getAttributes)
{
	Xhtml *o;
	struct mz_attribute *a;

	MZ_GET_XHTML_INIT(o);
	array_init(return_value);

	mz_vector_for_each(o->xhtml.atts, a) {
		zval *z;

		MAKE_STD_ZVAL(z);
		z->value.str.len = a->value.len;
		z->value.str.val = estrndup(a->value.str, a->value.len);
		z->type = IS_STRING;
		add_assoc_zval_ex(return_value, a->name.str, a->name.len+1, z);
	}
}

PHP_METHOD(Xhtml, getId)
{
	Xhtml *o;
	const struct mz_string *value;

	MZ_GET_XHTML_INIT(o);

	value = mz_attribute_get(&o->xhtml.atts, MZ_STRLEN("id"));

	if (value == NULL)
		RETURN_NULL();

	MZ_RETURN_STRING_CONST_L(value->str, value->len);
}

PHP_METHOD(Xhtml, getClass)
{
	Xhtml *o;
	const struct mz_string *value;

	MZ_GET_XHTML_INIT(o);

	value = mz_attribute_get(&o->xhtml.atts, MZ_STRLEN("class"));

	if (value == NULL)
		RETURN_NULL();

	MZ_RETURN_STRING_CONST_L(value->str, value->len);
}

PHP_METHOD(Xhtml, addText)
{
	Xhtml *o;
	const char *text;
	int text_len;
	zend_bool do_format = MZ_NEITHER;
	struct mz_xtext *xtext;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("s|b", &text, &text_len, &do_format);

	if (do_format == MZ_NEITHER)
		do_format = o->xhtml.xnode.do_format;

	xtext = mz_xhtml_add_text(&o->xhtml, text, text_len, do_format);

	if (xtext == NULL)
		RETURN_FALSE;

#if XNODE_LOG_ALLOC
	MZ_XHTML_LOG("%p addText (%s) %p", &o->xhtml, text, xtext);
#endif

	Xtext_make_ref(return_value, xtext);
}

PHP_METHOD(Xhtml, setText)
{
	Xhtml *o;
	const char *text;
	int text_len;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("s", &text, &text_len);

	if (mz_xhtml_set_text(&o->xhtml, text, text_len) < 0)
		RETURN_FALSE; /* ToDo: throw exception */
}

PHP_METHOD(Xhtml, getText)
{
	Xhtml *o;
	long index = 0;
	struct mz_xtext *xtext;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("|l", &index);

	xtext = mz_xhtml_get_text_node(&o->xhtml, index);

	if (xtext == NULL)
		RETURN_NULL();

	MZ_RETURN_STRING_CONST_L(xtext->text.str, xtext->text.len);
}

PHP_METHOD(Xhtml, getSplitText)
{
	Xhtml *o;
	long index = 0;
	struct mz_xtext *xtext;
	struct mz_string *line;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("|l", &index);

	xtext = mz_xhtml_get_text_node(&o->xhtml, index);

	if (xtext == NULL)
		RETURN_NULL();

	array_init(return_value);

	mz_vector_for_each(xtext->split, line) {
		zval *zline;

		/* ToDo: add_next_index_stringl ? */
		MAKE_STD_ZVAL(zline);
		zline->value.str.len = line->len;
		zline->value.str.val = estrndup(line->str, line->len);
		zline->type = IS_STRING;
		add_next_index_zval(return_value, zline);
	}
}

PHP_METHOD(Xhtml, getTextNode)
{
	Xhtml *o;
	long index = 0;
	struct mz_xtext *xtext;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("|l", &index);

	xtext = mz_xhtml_get_text_node(&o->xhtml, index);

	if (xtext == NULL)
		RETURN_NULL();

	Xtext_make_ref(return_value, xtext);
}

PHP_METHOD(Xhtml, countText)
{
	Xhtml *o;

	MZ_GET_XHTML_INIT(o);
	RETURN_LONG(mz_xhtml_count_nodes(&o->xhtml, MZ_XNODE_TEXT));
}

PHP_METHOD(Xhtml, addLeaf)
{
	Xhtml *o;
	const char *tag;
	int tag_len;
	const char *text = NULL;
	int text_len = 0;
	zend_bool do_format = MZ_NEITHER;
	zend_bool do_short = true;
	struct mz_xhtml *xhtml;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("s|sbb", &tag, &tag_len, &text, &text_len,
		 &do_format, &do_short);

	if (do_format == MZ_NEITHER)
		do_format = o->xhtml.xnode.do_format;

	xhtml = mz_xhtml_add_leaf(&o->xhtml, tag, tag_len, text, text_len,
				  do_format, do_short);

#if XNODE_LOG_ALLOC
	MZ_XHTML_LOG("%p addLeaf (%s, %s) %p", &o->xhtml, tag, text, xhtml);
#endif

	if (xhtml == NULL)
		RETURN_NULL();

	Xhtml_make_ref(return_value, xhtml);
}

PHP_METHOD(Xhtml, getIndexedLeaf)
{
	Xhtml *o;
	const char *tag;
	int tag_len;
	long index;
	struct mz_xhtml *found;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("sl", &tag, &tag_len, &index);

	if (index < 0)
		RETURN_NULL();

	found = mz_xhtml_get_indexed_leaf(&o->xhtml, tag, tag_len, index);

	if (found == NULL)
		RETURN_NULL();

	Xhtml_make_ref(return_value, found);
}

PHP_METHOD(Xhtml, getLeaf)
{
	Xhtml *o;
	struct mz_xhtml *found;
	const char *tag;
	int tag_len;
	const char *att_name = NULL;
	int att_name_len = 0;
	const char *att_value = NULL;
	int att_value_len = 0;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("s|ss", &tag, &tag_len, &att_name, &att_name_len,
		 &att_value, &att_value_len);

	if ((att_name != NULL) && (att_value == NULL))
		RETURN_NULL();

	found = mz_xhtml_get_leaf(&o->xhtml, tag, tag_len, att_name,
				  att_name_len, att_value, att_value_len);

	if (found == NULL)
		RETURN_NULL();

	Xhtml_make_ref(return_value, found);
}

PHP_METHOD(Xhtml, getLeaves)
{
	Xhtml *o;
	struct mz_xnode **node;
	const char *tag = NULL;
	int tag_len = 0;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("|s", &tag, &tag_len);
	array_init(return_value);

	mz_vector_for_each(o->xhtml.nodes, node) {
		struct mz_xhtml *x;
		zval *leaf;

		if ((*node)->node_type != MZ_XNODE_HTML)
			continue;

		x = MZ_GET_PARENT(*node, mz_xhtml, xnode);

		if ((tag != NULL) && (mz_string_cmp(&x->tag, tag, tag_len)))
			continue;

		MAKE_STD_ZVAL(leaf);
		Xhtml_make_ref(leaf, x);
		add_next_index_zval(return_value, leaf);
	}
}

PHP_METHOD(Xhtml, countLeaves)
{
	Xhtml *o;

	MZ_GET_XHTML_INIT(o);
	RETURN_LONG(mz_xhtml_count_nodes(&o->xhtml, MZ_XNODE_HTML));
}

PHP_METHOD(Xhtml, importNode)
{
	Xhtml *o;
	zval *z;
	struct mz_xnode *xnode;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("o", &z);

	xnode = extract_xnode(z);

	if (xnode == NULL) {
		MZ_XHTML_LOG("Oops");
		RETURN_NULL();
	}

#if XNODE_LOG_ALLOC
	MZ_XHTML_LOG("%p importNode", &o->xhtml);
#endif

	if (mz_xhtml_import_node(&o->xhtml, xnode) < 0)
		RETURN_FALSE; /* ToDo: throw exception */

	make_xnode_ref(return_value, xnode);
	MZ_ZVAL_ADDREF(return_value);
}

PHP_METHOD(Xhtml, insertNode)
{
	Xhtml *o;
	zval *z;
	long index = 0;
	struct mz_xnode *xnode;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("o|l", &z, &index);

	xnode = extract_xnode(z);

	if (xnode == NULL)
		RETURN_NULL();

	if (mz_vector_insert(&o->xhtml.nodes, &xnode, index) < 0)
		RETURN_NULL();

	make_xnode_ref(return_value, xnode);
}

PHP_METHOD(Xhtml, removeNode)
{
	Xhtml *o;
	zval *z;
	struct mz_xnode *xnode;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("o", &z);

	xnode = extract_xnode(z);

	if (xnode == NULL)
		RETURN_NULL();

	if (mz_xhtml_remove_node(&o->xhtml, xnode) < 0)
		RETURN_NULL();
}

PHP_METHOD(Xhtml, getNode)
{
	Xhtml *o;
	long index;
	struct mz_xnode **found;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("l", &index);

	found = mz_vector_get(&o->xhtml.nodes, index);

	if (found == NULL)
		RETURN_NULL();

	make_xnode_ref(return_value, *found);
}

PHP_METHOD(Xhtml, getNodes)
{
	Xhtml *o;
	struct mz_xnode **node;

	MZ_GET_XHTML_INIT(o);
	array_init(return_value);

	mz_vector_for_each(o->xhtml.nodes, node) {
		zval *z;

		MAKE_STD_ZVAL(z);
		make_xnode_ref(z, *node);
		add_next_index_zval(return_value, z);
	}
}

PHP_METHOD(Xhtml, countNodes)
{
	Xhtml *o;

	MZ_GET_XHTML_INIT(o);
	RETURN_LONG(o->xhtml.nodes.n);
}

PHP_METHOD(Xhtml, equals)
{
	Xhtml *o;
	Xhtml *other;
	zval *z;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("o", &z);

	if (z->value.obj.handlers != &Xhtml_oh)
		RETURN_FALSE;

	other = zend_object_store_get_object(z);

	if (!mz_xhtml_equals(&o->xhtml, &other->xhtml))
		RETURN_FALSE;

	RETURN_TRUE;
}

PHP_METHOD(Xhtml, getNodeType)
{
	RETURN_LONG(MZ_XNODE_HTML);
}

PHP_METHOD(Xhtml, produce)
{
	Xhtml *o;
	zval *printer;

	MZ_GET_XHTML_INIT(o);
	MZ_PARSE("o", &printer);

	if (mz_xhtml_do_produce(&o->xhtml, printer, 0, false) < 0)
		RETURN_NULL();
}

static function_entry Xhtml_methods[] = {
	PHP_ME(Xhtml, __construct,     NULL, ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(Xhtml, getTag,          NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, setAttribute,    NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, setId,           NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, setClass,        NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getAttribute,    NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getAttributes,   NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getId,           NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getClass,        NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, addText,         NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, setText,         NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getText,         NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getSplitText,    NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getTextNode,     NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, countText,       NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, addLeaf,         NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getIndexedLeaf,  NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getLeaf,         NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getLeaves,       NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, countLeaves,     NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, importNode,      NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, insertNode,      NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, removeNode,      NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getNode,         NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getNodes,        NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, countNodes,      NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, getNodeType,     NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, equals,          NULL, ZEND_ACC_PUBLIC)
	PHP_ME(Xhtml, produce,         NULL, ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

static void Xhtml_free(void *p TSRMLS_DC)
{
	Xhtml *o = p;

	--Xhtml_counter;
#if XNODE_LOG_ALLOC
	MZ_XHTML_LOG("%p free [%lu]", &o->xhtml, Xhtml_counter);
#endif

	if (o->xhtml.xnode.init) {
		struct mz_xnode **node;

		mz_vector_for_each(o->xhtml.nodes, node) {
			zend_object_handle handle = get_xnode_handle(*node);
			zend_objects_store_del_ref_by_handle(handle);
		}
	}

	zend_hash_destroy(o->std.properties);
	FREE_HASHTABLE(o->std.properties);

	if (o->xhtml.xnode.init) {
		mz_xhtml_free(&o->xhtml);
	}

	efree(o);
}

static zend_object_value Xhtml_create(zend_class_entry *ce TSRMLS_DC)
{
	return Xhtml_register(Xhtml_new(ce));
}

void mangoz_Xhtml_init(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "Xhtml", Xhtml_methods);
	Xhtml_ce = zend_register_internal_class(&ce TSRMLS_CC);
	Xhtml_ce->create_object = Xhtml_create;
	memcpy(&Xhtml_oh, zend_get_std_object_handlers(),
	       sizeof(zend_object_handlers));
	Xhtml_oh.clone_obj = NULL;
}

unsigned long mangoz_Xhtml_count(void)
{
	return Xhtml_counter;
}

static Xhtml *Xhtml_new(zend_class_entry *ce)
{
	Xhtml *o;

	o = emalloc(sizeof (Xhtml));
	++Xhtml_counter;
#if XNODE_LOG_ALLOC
	MZ_XHTML_LOG("%p new [%lu]", &o->xhtml, Xhtml_counter);
#endif
	mz_init_zend_object(&o->std, ce);
	MZ_CLEAR(o->xhtml.xnode);

	return o;
}

static zend_object_value Xhtml_register(Xhtml *o)
{
	zend_object_value ret;

	ret.handle = zend_objects_store_put(o, NULL, Xhtml_free,
					    NULL TSRMLS_CC);
	ret.handlers = &Xhtml_oh;
	o->handle = ret.handle;

	return ret;
}

static void Xhtml_make_ref(zval *z, struct mz_xhtml *x)
{
	Xhtml *o;

	o = MZ_GET_PARENT(x, Xhtml_object, xhtml);
	z->type = IS_OBJECT;
	MZ_ZVAL_INITREF(z);
	z->value.obj.handle = o->handle;
	z->value.obj.handlers = &Xhtml_oh;
	zend_objects_store_add_ref_by_handle(o->handle);
}

/* ----------------------------------------------------------------------------
 * callbacks
 */

int mangoz_php_printer_produce(void *vprinter, const char *str, size_t len)
{
	int res;
	zval *printer = vprinter;
	zval *fname;
	zval *zstr;
	zval **zargs[1];
	zval *retval;

	MAKE_STD_ZVAL(fname);
	fname->value.str.len = 7;
	fname->value.str.val = "produce";
	fname->type = IS_STRING;

	MAKE_STD_ZVAL(zstr);
	zstr->value.str.len = len;
	zstr->value.str.val = estrndup(str, len);
	zstr->type = IS_STRING;
	zargs[0] = &zstr;

	res = call_user_function_ex
		(NULL, &printer, fname, &retval, 1, zargs, 0, NULL TSRMLS_CC);

	if (res) /* ToDo: throw exception */
		MZ_XHTML_LOG("ERROR (%i)\n", res);

	efree(zstr->value.str.val);

	/* ToDo: check fname gets garbage-collected */

	return res;
}

struct mz_xtext *mangoz_php_create_xtext(const char *text, size_t len,
					 mz_bool do_format)
{
	Xtext *o;
	zval *z;

	o = Xtext_new(Xtext_ce);

#if XNODE_LOG_ALLOC
	MZ_XNODE_LOG("%p create_xtext (%s)", &o->xtext, text);
#endif

	if (o == NULL)
		return NULL;

	if (mz_xtext_init(&o->xtext, text, len, do_format) < 0)
		return NULL; /* ToDo: clean-up */

	MAKE_STD_ZVAL(z);
	z->type = IS_OBJECT;
	z->value.obj = Xtext_register(o);
	MZ_ZVAL_ADDREF(z);

	return &o->xtext;
}

struct mz_xhtml *mangoz_php_create_xhtml(const char *tag, size_t len,
					 mz_bool do_format, mz_bool do_short)
{
	Xhtml *o;
	zval *z;

	o = Xhtml_new(Xhtml_ce);

#if XNODE_LOG_ALLOC
	MZ_XNODE_LOG("%p create_xhtml (%s)", &o->xhtml, tag);
#endif

	if (o == NULL)
		return NULL;

	if (mz_xhtml_init(&o->xhtml, tag, len, do_format, do_short) < 0)
		return NULL; /* ToDo: clean-up */

	MAKE_STD_ZVAL(z);
	z->type = IS_OBJECT;
	z->value.obj = Xhtml_register(o);
	MZ_ZVAL_ADDREF(z);

	return &o->xhtml;
}

/* ----------------------------------------------------------------------------
 * tools
 */

static struct mz_xnode *extract_xnode(zval *z)
{
	void *vobj = zend_object_store_get_object(z);
	zend_object_handlers *oh = z->value.obj.handlers;
	struct mz_xnode *xnode;

	if (oh == &Xtext_oh) {
		Xtext *o = vobj;
		struct mz_xtext *xtext = &o->xtext;
		xnode = MZ_GET_PARENT(xtext, mz_xtext, xnode);
	} else if (oh == &Xhtml_oh) {
		Xhtml *o = vobj;
		struct mz_xhtml *xhtml = &o->xhtml;
		xnode = MZ_GET_PARENT(xhtml, mz_xhtml, xnode);
	} else {
		MZ_XNODE_LOG("Oops");
		xnode = NULL;
	}

	return xnode;
}

static zend_object_handle get_xnode_handle(struct mz_xnode *xnode)
{
	zend_object_handle handle;

	switch (xnode->node_type) {
	case MZ_XNODE_TEXT: {
		struct mz_xtext *xtext = MZ_GET_PARENT(xnode, mz_xtext, xnode);
		Xtext *o = MZ_GET_PARENT(xtext, Xtext_object, xtext);
		handle = o->handle;
		break;
	}
	case MZ_XNODE_HTML: {
		struct mz_xhtml *xhtml = MZ_GET_PARENT(xnode, mz_xhtml, xnode);
		Xhtml *o = MZ_GET_PARENT(xhtml, Xhtml_object, xhtml);
		handle = o->handle;
		break;
	}
	default:
		MZ_XNODE_LOG("Oops");
		handle = 0;
		break;
	}

	return handle;
}

static void make_xnode_ref(zval *z, struct mz_xnode *xnode)
{
	switch (xnode->node_type) {
	case MZ_XNODE_TEXT: {
		struct mz_xtext *x = MZ_GET_PARENT(xnode, mz_xtext, xnode);
		Xtext_make_ref(z, x);
		break;
	}
	case MZ_XNODE_HTML: {
		struct mz_xhtml *x = MZ_GET_PARENT(xnode, mz_xhtml, xnode);
		Xhtml_make_ref(z, x);
		break;
	}
	}
}

/* Mangoz - php/mangoz-php.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-php.h"
#include "mangoz-php-util.h"
#include "callbacks.h"
#include <mangoz/libmz/libmz.h>
#include <php.h>

#define PHP_MANGOZ_VERSION "0.7"
#define PHP_MANGOZ_EXTNAME "mangoz"

#define MZ_REGISTER_CONSTANT(name) \
	REGISTER_LONG_CONSTANT(#name, name, CONST_CS | CONST_PERSISTENT)

unsigned long mangoz_logging;

const struct mz_callbacks mz_callbacks = {
	.printer_produce = mangoz_php_printer_produce,
	.create_xtext = mangoz_php_create_xtext,
	.create_xhtml = mangoz_php_create_xhtml,
};

static void *mangoz_php_malloc(size_t size)
{
	return emalloc(size);
}

static void *mangoz_php_realloc(void *ptr, size_t size)
{
	return erealloc(ptr, size);
}

static void mangoz_php_free(void *ptr)
{
	efree(ptr);
}

PHP_MINIT_FUNCTION(mangoz)
{
	mz_malloc = mangoz_php_malloc;
	mz_realloc = mangoz_php_realloc;
	mz_free = mangoz_php_free;

	mangoz_logging = 0;

	MZ_REGISTER_CONSTANT(MZ_LOG_XHTML_FLAG);
	MZ_REGISTER_CONSTANT(MZ_XNODE_TEXT);
	MZ_REGISTER_CONSTANT(MZ_XNODE_HTML);

	mangoz_Xtext_init();
	mangoz_Xhtml_init();
	mangoz_MzPrinter_init();
	mangoz_MzStdPrinter_init();
	mangoz_MzStringPrinter_init();

	return SUCCESS;
}

PHP_MSHUTDOWN_FUNCTION(mangoz)
{
	return SUCCESS;
}

PHP_FUNCTION(mangoz_get_version)
{
	RETURN_LONG(0x00070000);
}

PHP_FUNCTION(mangoz_set_logging)
{
	long flag;
	zend_bool enabled = 1;

	MZ_PARSE("l|b", &flag, &enabled);

	if (enabled)
		mangoz_logging |= flag;
	else
		mangoz_logging &= ~flag;
}

PHP_FUNCTION(mangoz_xtext_count)
{
	RETURN_LONG(mangoz_Xtext_count());
}

PHP_FUNCTION(mangoz_xhtml_count)
{
	RETURN_LONG(mangoz_Xhtml_count());
}

static zend_function_entry mangoz_functions[] = {
	PHP_FE(mangoz_get_version, NULL)
	PHP_FE(mangoz_set_logging, NULL)
	PHP_FE(mangoz_xtext_count, NULL)
	PHP_FE(mangoz_xhtml_count, NULL)
	{ NULL, NULL, NULL }
};

static zend_module_entry mangoz_module_entry = {
	STANDARD_MODULE_HEADER,
	.name = PHP_MANGOZ_EXTNAME,
	.functions = mangoz_functions,
	.module_startup_func = PHP_MINIT(mangoz),
	.module_shutdown_func = PHP_MSHUTDOWN(mangoz),
	.request_startup_func = NULL,
	.request_shutdown_func = NULL,
	.info_func = NULL, /* PHP_MINFO(mangoz), */
	.version = PHP_MANGOZ_VERSION,
	STANDARD_MODULE_PROPERTIES
};

ZEND_GET_MODULE(mangoz);

/* Mangoz - php/mangoz-php.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MANGOZ_PHP_H
#define INCLUDE_MANGOZ_PHP_H 1

extern unsigned long mangoz_logging;

enum mangoz_log_flag {
	MZ_LOG_XHTML_FLAG = 1,
};

extern void mangoz_Xtext_init(void);
extern unsigned long mangoz_Xtext_count(void);
extern void mangoz_Xhtml_init(void);
extern unsigned long mangoz_Xhtml_count(void);
extern void mangoz_MzPrinter_init(void);
extern void mangoz_MzStdPrinter_init(void);
extern void mangoz_MzStringPrinter_init(void);

#endif /* INCLUDE_MANGOZ_PHP_H */

/* Mangoz - php/callbacks.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MANGOZ_PHP_CALLBACKS_H
#define INCLUDE_MANGOZ_PHP_CALLBACKS_H 1

#include <mangoz/libmz/libmz.h>

extern int mangoz_php_printer_produce(
	void *printer, const char *str, size_t len);
extern struct mz_xtext *mangoz_php_create_xtext(
	const char *text, size_t len, mz_bool do_format);
extern struct mz_xhtml *mangoz_php_create_xhtml(
	const char *tag, size_t tag_len, mz_bool do_format, mz_bool do_short);

#endif /* INCLUDE_MANGOZ_PHP_CALLBACKS_H */

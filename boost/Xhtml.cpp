/* Mangoz - boost/Xhtml.cpp
   Copyright (C) 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/boost/mangoz-boost.h>

using namespace std;
using namespace boost;

namespace mangoz {

  Xhtml::Xhtml(const string &tag)
  {
    if (mz_xhtml_init(&this->xhtml, tag.c_str(), tag.size(), false, false) < 0)
      cerr << "Oops: mz_xhtml_init failed" << endl; /* ToDo: throw exception */
  }

  Xhtml::~Xhtml()
  {
  }

  const mz_xnode_type Xhtml::getNodeType()
  {
    return MZ_XNODE_HTML;
  }

  const int Xhtml::produce(Printer &printer)
  {
    return 0;
  }

  shared_ptr<string> Xhtml::getTag()
  {
    return shared_ptr<string>(new string(this->xhtml.tag.str));
  }

} // namespace mangoz

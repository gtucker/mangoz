/* Mangoz - boost/Xtext.cpp
   Copyright (C) 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/boost/mangoz-boost.h>
#include "mangoz-boost-util.h"

using namespace std;
using namespace boost;

#include <stdio.h>

namespace mangoz {

  Xtext::Xtext(const string &text, bool do_format)
  {
    MZ_CLEAR(this->xtext.xnode);

    if (mz_xtext_init(&this->xtext, text.c_str(), text.length(), do_format)< 0)
      cerr << "Oops: mz_xtext_init failed" << endl; /* ToDo: throw exception */
  }

  Xtext::~Xtext()
  {
    mz_xtext_free(&this->xtext);
  }

  const mz_xnode_type Xtext::getNodeType()
  {
    return MZ_XNODE_TEXT;
  }

  const int Xtext::produce(Printer &printer)
  {
    return mz_xtext_do_produce(&this->xtext, &printer, 0);
  }

  bool Xtext::setText(const string &text)
  {
    if (mz_xtext_set(&this->xtext, text.c_str(), text.length()) < 0)
      return false;

    return true;
  }

  const string Xtext::getText()
  {
    return string(this->xtext.text.str);
  }

  const shared_string Xtext::getSharedText()
  {
    return shared_string(new string(this->xtext.text.str));
  }

  const shared_strvec Xtext::getSplitText()
  {
    shared_strvec split;

    split = shared_strvec(new strvec());
    split->reserve(this->xtext.split.n);
    mz_boost_vector_for_each(this->xtext.split, line, mz_string)
      split->push_back(line->str);

    return split;
  }

} // namespace mangoz

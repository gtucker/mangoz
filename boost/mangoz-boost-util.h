/* Mangoz - boost/mangoz-boost-util.h
   Copyright (C) 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MANGOZ_BOOST_INCLUDE_H
#define INCLUDE_MANGOZ_BOOST_INCLUDE_H 1

#define mz_boost_vector_for_each(v, it, type)        \
  for (type *it = static_cast<type *>(v.data);       \
       it != static_cast<const type *>(v.end);       \
       it++)

#endif /* INCLUDE_MANGOZ_BOOST_INCLUDE_H */

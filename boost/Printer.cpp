/* Mangoz - boost/Printer.cpp
   Copyright (C) 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/boost/mangoz-boost.h>

using namespace std;

namespace mangoz {

  // -- StreamPrinter --

  StreamPrinter::StreamPrinter(ostream &out): out(out), total_length(0)
  {
  }

  StreamPrinter::~StreamPrinter()
  {
  }

  void StreamPrinter::clear()
  {
    this->total_length = 0;
  }

  int StreamPrinter::produce(const char *str, size_t len)
  {
    this->out << str;
    this->total_length += len;
    return 0;
  }

  const size_t StreamPrinter::length()
  {
    return this->total_length;
  }

  // -- StringPrinter --

  StringPrinter::StringPrinter()
  {
  }

  StringPrinter::~StringPrinter()
  {
  }

  void StringPrinter::clear()
  {
    this->str.clear();
  }

  int StringPrinter::produce(const char *str, size_t len)
  {
    this->str += str;
    return 0;
  }

  const size_t StringPrinter::length()
  {
    return this->str.length();
  }

  const string &StringPrinter::getString()
  {
    return this->str;
  }

} // namespace mangoz

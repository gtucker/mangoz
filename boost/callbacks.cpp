/* Mangoz - boost/callbacks.cpp
   Copyright (C) 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/boost/mangoz-boost.h>

extern "C" {
#include <mangoz/libmz/libmz.h>
}

using namespace mangoz;
using namespace std;

extern "C" int mz_boost_printer_produce(void *vprinter, const char *str,
                                        size_t len)
{
  Printer *printer = static_cast<Printer *>(vprinter);
  return printer->produce(str, len);
}

extern "C" const struct mz_callbacks mz_callbacks = {
  mz_boost_printer_produce,
  NULL,
  NULL,
};


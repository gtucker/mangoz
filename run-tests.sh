#!/bin/sh

# Mangoz - run-test.sh
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

set -e

echo "--- libmz ---"
./out/$(uname -m)/test/mz/mz-test test/data

echo "--- Boost ---"
./out/$(uname -m)/test/boost/api/xhtml_api

echo "--- PHP ---"
php -c php/mangoz.ini test/php/garbage.php
php -c php/mangoz.ini test/php/xhtml_api.php

echo "--- Python ---"
python test/python/garbage.py
python test/python/configuration_api.py
python test/python/xparser_api.py
python test/python/xhtml_api.py
python test/python/xpage_api.py
python test/python/xdoc_api.py
python test/python/tree_api.py
python test/python/textbox_api.py
python test/python/path_test.py

echo "--- ALL TESTS PASSED ---"

exit 0

# Mangoz - build/lib.mk
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

target_out_lib = $(addprefix $(LIBDIR)/,$(out))
target_obj := $(addprefix $(OUTDIR)/,$(obj))
static_libs := $(addprefix $(LIBDIR)/,$(libs)) # ToDo: dependencies suck
CFLAGS += $(addprefix -I,$(INCLUDE))

all: output_dir depend.mk $(target_out_lib)
	@echo -n

$(target_out_lib): $(target_obj) $(static_libs)

ifneq ($(MAKECMDGOALS),clean)
-include depend.mk
endif

# Mangoz - build/app.mk
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

target_out = $(addprefix $(OUTDIR)/,$(out))
target_obj := $(addprefix $(OUTDIR)/,$(obj))
target_libs := $(addprefix $(LIBDIR)/,$(libs))
CFLAGS += $(addprefix -I,$(INCLUDE))

all: output_dir depend.mk $(target_out)
	@echo -n

$(target_out): $(target_obj) $(target_libs)
	@echo "  LINK    " $(call relobj,$@)
ifeq ($(LANG),C++)
	@$(CPLUSPLUS) $(CFLAGS) -o $@ $^
else
	@$(CC) $(CFLAGS) -o $@ $^
endif

ifneq ($(MAKECMDGOALS),clean)
-include depend.mk
endif

# Mangoz - build/builder.mk
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

ARCH ?= $(shell uname -m)

AS := $(CROSS_COMPILE)as
LD := $(CROSS_COMPILE)ld
CC := $(CROSS_COMPILE)gcc
CPLUSPLUS := $(CROSS_COMPILE)g++
CPP := $(CC) -E
AR := $(CROSS_COMPILE)ar
NM := $(CROSS_COMPILE)nm
STRIP := $(CROSS_COMPILE)strip
OBJCOPY := $(CROSS_COMPILE)objcopy
OBJDUMP := $(CROSS_COMPILE)objdump

OUTDIR_ROOT := out
RELDIR := $(subst $(TOP_DIR)/,,$(realpath $(CURDIR)))
LIBDIR := $(TOP_DIR)/$(OUTDIR_ROOT)/$(ARCH)/lib
OUTDIR := $(TOP_DIR)/$(OUTDIR_ROOT)/$(ARCH)/$(RELDIR)
INCLUDE ?= $(TOP_DIR)/include

.DEFAULT_GOAL := all

src := $(filter-out $(exclude),$(wildcard *.c) $(wildcard *.cpp))
headers := $(wildcard *.h)
obj := $(addsuffix .o,$(basename $(src)))

relobj = $(RELDIR)/$(subst $(OUTDIR)/,,$(1))
rellib = $(subst $(LIBDIR)/,,$(1))

$(OUTDIR)/%.o: %.c
	@echo "  CC      " $(call relobj,$@)
	@$(CC) $(CFLAGS) -c -fPIC -o $@ $<

$(OUTDIR)/%.o: %.cpp
	@echo "  C++     " $(call relobj,$@)
	@$(CPLUSPLUS) $(CFLAGS) -c -fPIC -o $@ $<

$(LIBDIR)/%.a:
	@echo "  AR      " $(call rellib,$@)
	@$(AR) rcs $@ $^

$(LIBDIR)/%.so:
	@echo "  LINK    " $(call rellib,$@)
ifeq ($(LANG),C++)
	@$(CPLUSPLUS) $(CFLAGS) -fpic -shared -Wl,-soname,$@ -o $@ $^
else
	@$(CC) $(CFLAGS) -fpic -shared -Wl,-soname,$@ -o $@ $^
endif

output_dir: $(LIBDIR) $(OUTDIR)
	@echo -n

$(LIBDIR):
	@mkdir -p $(LIBDIR)

$(OUTDIR):
	@mkdir -p $(OUTDIR)

depend.mk: $(src) $(headers)
	@-rm -f depend.mk
	@gcc -MM $(addprefix -I,$(INCLUDE)) $(src) > depend.mk
	@sed -i -e s/'^\(.*\.o: \)'/'$(subst /,\/,$(OUTDIR))\/\1'/g depend.mk

.PHONY: sub-make clean print_arch

sub-make:
	@for dir in $(subdirs); do \
	    $(MAKE) -C $$dir -s $(MAKECMDGOALS) || exit 1; \
	done

clean:
	@echo "  CLEAN   " $(RELDIR)
	-@rm -rf $(OUTDIR)
	-@rm -f $(target_out_lib)
	-@rm -f depend.mk

	@for dir in $(subdirs); do \
	    $(MAKE) -C $$dir -s clean || exit 1; \
	done

print-arch:
	@echo "  Architecture: $(ARCH)"

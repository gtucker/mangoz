# Mangoz - example/python/django_example/urls.py
# Copyright (C) 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    url(r'^(?P<path>.*)$', 'django_example.main.views.home', name='home'),
)

urlpatterns += staticfiles_urlpatterns()

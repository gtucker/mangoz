# Mangoz - example/python/django_example/main/composer.py
# Copyright (C) 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import mangoz
from mangoz import Composer, Response

class BaseComposer(Composer):
    def compose(self, page, req):
        self._add_headers(page)
        self._add_contents(page)
        self._add_nav_bar(page.body)
        self._add_stats(page)
        return Response(page)

    def _add_headers(self, page):
        page.setDescription(self._sel.description)
        page.setTitle(' :: '.join(['Mangoz Example', self._sel.title]))

    def _add_contents(self, page):
        self.importElements(page.body, self._sel.getElements())

    def _add_nav_bar(self, root):
        level = self._sel.navigator.getLevel(0, True)
        if not level:
            return

        nav_bar = root.addP()
        for it in level:
            nav_bar.addText('|')
            link = nav_bar.addLink(it['name'], it['title'],
                                   ': '.join(["Go to page", it['title']]))
            if it['current'] is True:
                link.setClass('current')
        nav_bar.addText('|')

    def _add_stats(self, page):
        img_root = ''.join([self._conf.get('publicURL'), 'image'])
        about = "Powered by Mangoz-{0:08X} | language: {1} ({2})".format(
            mangoz.get_version(), self._sel.fetchText('language'),
            page.lang_tag)
        page.body.addP(about).setId('stats')
        cert = page.body.addP().setClass('w3cert')
        cert.addLink('http://jigsaw.w3.org/css-validator/').\
            setAttribute('target', '_blank').\
            addImage('/'.join([img_root, 'vcss-blue.png']), 'Valid CSS!').\
            setClass('w3cert')
        cert.addLink('http://validator.w3.org/check?uri=referer').\
            setAttribute('target', '_blank').\
            addImage('/'.join([img_root, 'valid-xhtml11-blue.png']),
                     'Valid XHTML!').\
            setClass('w3cert')


class StackComposer(BaseComposer):
    def importElements(self, root, ele):
        layout = root.addTable().setClass('stack')
        for e in ele:
            cell = layout.addRow().addCell().setClass('stack')
            for n in e.getLeaves():
                cell.importNode(n)

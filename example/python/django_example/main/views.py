# Mangoz - example/python/django_example/main/views.py
# Copyright (C) 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

from django.conf import settings
from composer import BaseComposer, StackComposer
import mangoz as mz

sel = mz.Selector(settings.MZCONF,
                  {'simple': BaseComposer,
                   'stack': StackComposer})

def home(request, path):
    return mz.django_connector(sel, request, path)

# Mangoz - example/python/django_example/main/tests.py
# Copyright (C) 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase


class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)

#!/bin/bash

set -e

last_tag=`git tag -l | grep build- | sort | tail -n 1`
n=`echo $last_tag | awk '{print substr($0,7)}'`
n="10#$n"
let 'm=n+1'
build_n=`printf "%04d" $m`
new_tag="build-$build_n"
echo "tag: $new_tag"
today=`date +%Y-%m-%d`
msg="Mangoz build #$m $today"
echo "msg: $msg"
git tag -a "$new_tag" -m "$msg"

exit 0

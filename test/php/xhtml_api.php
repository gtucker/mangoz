<?php

/* Mangoz - test/php/xhtml_api.php
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

/* require_once 'mangoz/core/xparser.php'; */

$sp = new MzStringPrinter ();

function test ($id, $cond)
{
  if (!$cond)
    {
      printf ("[%-32s] FAILED\n", $id);
      exit (1);
    }

  printf ("[%-32s] OK\n", $id);
}

function same_array ($a, $b)
{
  if (count ($a) != count ($b))
    return false;

  foreach ($a as $k => $v)
    {
      if (!array_key_exists ($k, $b))
        return false;
      if ($v != $b[$k])
        return false;
    }

  return true;
}

/* Xtext */

$x = new Xtext ("Alright");
test ("Xtext.getNodeType", ($x->getNodeType () == MZ_XNODE_TEXT));

$txt1 = "Hello";
$x = new Xtext ($txt1);
$txt2 = $x->getText ();
test ("Xtext.getText", ($txt1 == $txt2));

$txt1 = "OK";
$x->setText ($txt1);
$txt2 = $x->getText ();
test ("Xtext.setText", ($txt1 == $txt2));

$array1 = array ("A", "B", "C");
$txt = "A\nB\nC";
$x->setText ($txt);
$array2 = $x->getSplitText ();
test ("Xtext.getSplitText", same_array ($array1, $array2));

$txt = "That is \nsome piece of text.";
$x = new Xtext ($txt);
$y = new Xtext ($txt);
test ("Xtext.equals", $x->equals ($y));

/* Xhtml */

$x = new Xhtml ("x");
test ("Xhtml.getNodeType", ($x->getNodeType () == MZ_XNODE_HTML));

$tag1 = "z";
$x = new Xhtml ($tag1);
$tag2 = $x->getTag ();
test ("Xhtml.getTag", ($tag1 == $tag2));

$key = "test-key";
$value1 = "test-value";
$x->setAttribute ($key, $value1);
$value2 = $x->getAttribute ($key);
test ("Xhtml.setAttribute", ($value1 == $value2));

$key = "another-key";
$value1 = "something-else";
$x->setAttribute ($key, $value1);
$value2 = $x->getAttribute ($key);
test ("Xhtml.getAttribute", ($value1 == $value2));

$id1 = "test-id";
$x->setId ($id1);
$id2 = $x->getId ();
test ("Xhtml.{get,set}Id", ($id1 == $id2));

$cls1 = "test-class";
$x->setClass ($cls1);
$cls2 = $x->getClass ();
test ("Xhtml.{get,set}Class", ($cls1 == $cls2));

$att1 = array ("key-a"=>"value-a", "key-b"=>"value-b", "key-c"=>"value-c");
$x = new Xhtml ("z");
foreach ($att1 as $k => $v)
    $x->setAttribute ($k, $v);
$att2 = $x->getAttributes ();
test ("Xhtml.getAttributes", same_array ($att1, $att2));

/* Xhtml: text */

$txt1 = "Some text.";
$x = new Xhtml ("z", $txt1);
$txt2 = $x->getText ();
test ("Xhtml.getText", ($txt1 == $txt2));

$txt1a = "First text";
$txt1b = "Second text";
$x = new Xhtml ("z");
$x->addText ($txt1a);
$x->addText ($txt1b);
$txt2a = $x->getText (0);
$txt2b = $x->getText (1);
test ("Xhtml.addText", (($txt1a == $txt2a) && ($txt1b == $txt2b)));

$txt1 = "The text.";
$x = new Xhtml ("z");
$x->setText ($txt1);
$txt2 = $x->getText ();
test ("Xhtml.setText", ($txt1 == $txt2));

$txt1a = array ("A", "B", "C");
$txt1b = "A\nB\nC";
$x = new Xhtml ("z");
$x->setText ($txt1b);
$txt2 = $x->getSplitText ();
test ("Xhtml.getSplitText", same_array ($txt1a, $txt2));

$txt1 = "Some piece of text...";
$x = new Xhtml ("z");
$node1 = $x->addText ($txt1);
$node2 = $x->getTextNode ();
test ("Xhtml.getTextNode", ($node1 == $node2));

$txt = array ("Hello", "Bye-bye", "Good night", "So long");
$n = count ($txt);
$x = new Xhtml ("z");
foreach ($txt as $t)
  $x->addText ($t);
$m = $x->countText ();
test ("Xhtml.countText", ($n == $m));

/* Xhtml: leaves */

$tag1 = "y";
$x = new Xhtml ("x");
$x->addLeaf ($tag1);
$y = $x->getLeaf ($tag1);
$tag2 = $y ? $y->getTag () : null;
test ("Xhtml.addLeaf", ($tag1 == $tag2));

$tag1a = "y";
$tag1b = "z";
$id1a = "id-1";
$id1b = "id-2";
$id1c = "id-3";
$id1 = array ($id1a, $id1b, null, $id1c);
$x = new Xhtml ("x");
$x->addLeaf ($tag1a)->setId ($id1a);
$x->addLeaf ($tag1b)->setId ($id1b);
$x->addLeaf ($tag1a)->setId ($id1c);
$x2a = $x->getIndexedLeaf ($tag1a, 0);
$x2b = $x->getIndexedLeaf ($tag1b, 0);
$x2c = $x->getIndexedLeaf ($tag1b, 1);
$x2d = $x->getIndexedLeaf ($tag1a, 1);
$id2 = array ($x2a->getId (), $x2b->getId (), $x2c, $x2d->getId ());
test ("Xhtml.getIndexedLeaf", same_array ($id1, $id2));

$tag1a = "y";
$tag1b = "z";
$x = new Xhtml ("x");
$x->addLeaf ($tag1a);
$x->addLeaf ($tag1b);
$y = $x->getLeaf ($tag1a);
$z = $x->getLeaf ($tag1b);
$tag2a = $y ? $y->getTag () : null;
$tag2b = $z ? $z->getTag () : null;
test ("Xhtml.getLeaf", (($tag1a == $tag2a) && ($tag1b == $tag2b)));

$tag_leaves = array ("a"=>"a", "b"=>"b", "c"=>"c", "d"=>"d");
$x = new Xhtml ("x");
foreach ($tag_leaves as $tag)
  $x->addLeaf ($tag);
$leaves = $x->getLeaves ();
$pass = true;
foreach ($leaves as $leaf)
  {
    $tag = $leaf->getTag ();
    if (!array_key_exists ($tag, $tag_leaves)) {
      $pass = false;
      break;
    }
    unset ($tag_leaves[$tag]);
  }
test ("Xhtml.getLeaves", $pass);

$tags = array ("a", "b", "c", "d");
$n = count ($tags);
$x = new Xhtml ("x");
foreach ($tags as $t)
  $x->addLeaf ($t);
$m = $x->countLeaves ();
test ("Xhtml.countLeaves", ($n == $m));

/* Xhtml: nodes */

$node_tags = array ("a", "b", "c", "d");
$n = count ($node_tags);
$txt1 = "That's a text leaf.";
$x = new Xhtml ("x");
foreach ($node_tags as $t)
  $x->addLeaf ($t);
$x->addText ($txt1);
$pass = true;
for ($i = 0; $i < $n; $i++)
  {
    $node = $x->getNode ($i);
    if (($node === NULL) || ($node->getTag () != $node_tags[$i]))
      {
        $pass = false;
        break;
      }
  }
$txt_node = $x->getNode ($n);
if ($txt_node->getText () != $txt1)
  $pass = false;
if ($x->getNode ($n + 1) !== NULL)
  $pass = false;
if ($x->getNode (-1) != $txt_node)
  $pass = false;
test ("Xhtml.getNode", $pass);

$n1 = new Xhtml ("a");
$n2 = new Xtext ("Hey");
$n3 = new Xhtml ("b");
$n4 = new Xtext ("Alright");
$nodes1 = array ($n1, $n2, $n3, $n4);
$x = new Xhtml ("x");
foreach ($nodes1 as $n)
  $x->importNode ($n);
$nodes2 = $x->getNodes ();
test ("Xhtml.getNodes", same_array ($nodes1, $nodes2));

$y1 = new Xhtml ("y");
$txt1 = "Alright there.";
$t1 = new Xtext ($txt1);
$x = new Xhtml ("x");
$x->importNode ($y1);
$x->importNode ($t1);
$y2 = $x->getNode (0);
$t2 = $x->getNode (1);
$txt2 = $t2 ? $t2->getText () : null;
test ("Xhtml.importNode #001",
      (($y1 == $y2) && ($t1 == $t2) && ($txt1 == $txt2)));
/*
Xhtml::$DETECT_RECURSIVE_IMPORT = true;

$x = new Xhtml ("x");
$res = $x->importNode ($x);
test ("Xhtml.importNode #002", ($res === null));

$x = new Xhtml ("x");
$y = $x->addLeaf ("y");
$res = $y->importNode ($x);
test ("Xhtml.importNode #003", ($res === null));

Xhtml::$DETECT_RECURSIVE_IMPORT = false;

$x = new Xhtml ("x");
$res = $x->importNode ($x);
test ("Xhtml.importNode #004", ($res === $x));
*/

$tag1a = "y";
$tag1b = "z";
$txt1a = "A first text";
$txt1b = "And another one...";
$x = new Xhtml ("x");
$x->addLeaf ($tag1a);
$x->addText ($txt1a);
$y1 = new Xhtml ($tag1b);
$t1 = new Xtext ($txt1b);
$x->insertNode ($y1);
$x->insertNode ($t1, 2);
$y2b = $x->getNode (0);
$y2a = $x->getNode (1);
$t2b = $x->getNode (2);
$t2a = $x->getNode (3);
$tag2a = $y2a->getTag ();
$tag2b = $y2b->getTag ();
$txt2a = $t2a->getText ();
$txt2b = $t2b->getText ();
test ("Xhtml.insertNode", (($tag2a == $tag1a) && ($tag2b == $tag1b)
                           && ($txt2a == $txt1a) && ($txt2b == $txt1b)
                           && ($y1 == $y2b) && ($t1 == $t2b)));

$n1 = new Xhtml ("a");
$n2 = new Xtext ("Hey");
$n3 = new Xhtml ("b");
$n4 = new Xtext ("Alright");
$nodes1 = array ($n1, $n2, $n3, $n4);
$x = new Xhtml ("x");
foreach ($nodes1 as $n)
  $x->importNode ($n);
$pass = true;
foreach (array (3, 0, 1, 0) as $i)
  {
    $n = $nodes1[$i];
    $x->removeNode ($n);
    unset ($nodes1[$i]);
    $tmp = array ();
    foreach ($nodes1 as &$n)
      $tmp[] = $n;
    $nodes1 = $tmp;
    $nodes2 = $x->getNodes ();
    if (!same_array ($nodes1, $nodes2))
      {
        $pass = false;
        break;
      }
  }
test ("Xhtml.removeNode", $pass);

$n1 = new Xhtml ("a");
$n2 = new Xtext ("Hey");
$n3 = new Xhtml ("b");
$n4 = new Xtext ("Alright");
$nodes1 = array ($n1, $n2, $n3, $n4);
$n = count ($nodes1);
$x = new Xhtml ("x");
foreach ($nodes1 as $node)
  $x->importNode ($node);
$m = $x->countNodes ();
test ("Xhtml.countNodes", ($n == $m));

/* Xhtml: equals */

function makeNode1 ()
{
  return new Xhtml ("a");
}

function makeNode2 ()
{
  $x = new Xhtml ("x", "That's the top-level text value");

  $x->setAttribute ("a", "b");
  $x->addLeaf ("y", "OK")->setAttribute ("c", "d");
  $z = $x->addLeaf ("z", "Alright.")->setAttribute ("hello", "Bye-bye");
  $z->addLeaf ("w")->setId ("something");

  return $x;
}

function makeNode3 ()
{
  $x = new Xhtml ("x");

  for ($i = 0; $i < 50; $i++)
    {
      $y = $x->addLeaf ("tag-$i");

      for ($j = 0; $j < 50; $j++)
        $y->setAttribute ("att-$j", "$i-$j");
    }

  return $x;
}

test ("Xhtml.equals #001", (makeNode1 ()->equals (makeNode1 ())));
test ("Xhtml.equals #002", (makeNode2 ()->equals (makeNode2 ())));
test ("Xhtml.equals #003", (makeNode3 ()->equals (makeNode3 ())));

$x1 = makeNode2 ();
$x2 = makeNode2 ();
$x2->setText ("And now for something completely different");
test ("Xhtml.equals #100", (!$x1->equals ($x2)));

$x1 = makeNode2 ();
$x2 = makeNode2 ();
$x1->setId ("Arthur");
$x2->setId ("Dent");
test ("Xhtml.equals #101", (!$x1->equals ($x2)));

/* Xhtml: helpers */
/*
$parser = new Xparser ();
$doc = $parser->parseFile ("../data/xhtml_api.xml");
$tests = $doc->getRoot ();

$x = new Xhtml ("test");
$x->setId ("helpers");
$x->addP ("A paragraph.");
$x->addDiv ("That's a div.");
$x->addSpan ()->setId ("empty-span");
$x->addFile ("../data/xhtml_api.txt");
$x->addImage ("image.png", "This is not an image.");
$x->addLink ("http://here.com", "That's a link to here", "No title yet");
$x->addLinkParams ("http://there.com", array ("one" => "1", "two" => "2"),
                   "there", "That's the title!");
$helpers_data = $tests->getLeaf ("test", "id", "helpers");
test ("Xhtml.add* helpers", $x->equals ($helpers_data));
*/

/* Xlist */

/*
$x = new Xhtml ("test");
$x->setId ("list");

$l = new Xlist ();
$x->importNode ($l);
$x->addList ()->setId ("list-2");
$l = $x->addList ();
$l->addPoint ("item");
$l = $x->addList ();
for ($i = 0; $i < 10; $i++)
  $l->addPoint ("Item #$i")->setId ("list-id-$i");

$list = $tests->getLeaf ("test", "id", "list");
test ("Xlist", $x->equals ($list));
*/

/* Xtable */

/*
$x = new Xhtml ("test");
$x->setId ("table");
$t = new Xtable ();
$t->setId ("table-1");
$t->addRow ()->addCell ("A single cell");
$x->importNode ($t);
$t = $x->addTable ();
$r = $t->addRow ()->setId ("row-1");
$r->addCell ("First");
$r->addCell ("Second")->addP ("Hello");
$r = $t->addRow ()->setId ("row-2");
$r->addHeader ("Header");
for ($i = 0; $i < 10; $i++)
  $r->addCell ("Cell #$i")->setAttribute ("loop", $i);
$table = $tests->getLeaf ("test", "id", "table");
test ("Xtable", $x->equals ($table));
*/

/* Xform */

/*
$x = new Xhtml ("test");
$x->setId ("form");
$f = new Xform ("http://nowhere.com", "GET");
$x->importNode ($f);
$f = $x->addForm ("http://localhost/", "POST");
$f->addLeaf ("h1", "Form test");
$f->addEdit ("edit-test", 10, "edit-value");
$f->addText ("text-test", 12, 12, "Hello\nBye-bye.");
$f->addPassword ("password-test", 15);
$f->addSelect ("select-test", array ("a" => "apricot", "b" => "banana"), "b");
$f->addCheckbox ("checkbox-test-a", "no-check");
$f->addCheckbox ("checkbox-test-b", "check", true);
$f->addHidden ("hidden-test", "here it is");
$f->addSubmit ("submit-test-a");
$f->addSubmit ("submit-test-b", "submit-name");
$f->addSubmitImage ("http://localhost/image.png");
$f->addUpload ("upload-test");
$f = $x->addForm ("http://elsewhere.com", "POST");
$f->setInputId (1);
for ($i = 0; $i < 10; $i++)
  {
    $f->addEdit ("set-test");
    $f->addCheckbox ("check-test", "test-value-$i", ($i % 2) ? true : false);
    $f->addLeaf ("br");
    $f->incInputId ();
  }
$f->setInputId (0);
$f->addSubmit ("GO");

$form = $tests->getLeaf ("test", "id", "form");
test ("Xform", $x->equals ($form));
*/

exit (0);

?>

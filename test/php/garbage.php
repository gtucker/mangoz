<?php

/* Mangoz - test/php/garbage.php
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

/*mangoz_set_logging(MZ_LOG_XHTML_FLAG);*/

function check_xtext_count($count)
{
  $x = mangoz_xtext_count();

  if ($x != $count)
    {
      printf("MEMORY LEAK! %d instead of %d Xtext counts\n", $x, $count);
      exit(1);
    }
}

function check_xhtml_count($count)
{
  $x = mangoz_xhtml_count();

  if ($x != $count)
    {
      printf("MEMORY LEAK! %d instead of %d Xhtml counts\n", $x, $count);
      exit(1);
    }
}

check_xtext_count(0);
check_xhtml_count(0);

print("# 001 --------------------------------------------------\n");

/* Simple case: create an object and then free it */
{
  $x = new Xhtml("x");
  check_xhtml_count(1);
  unset($x);
  check_xhtml_count(0);
}

print("# 002 --------------------------------------------------\n");

/* Check that all leaves get freed when the root is freed
   (the leaves are not referenced anywhere else) */
{
  $x = new Xhtml("x");
  check_xhtml_count(1);
  $x->addLeaf("y");
  check_xhtml_count(2);
  unset($x);
  check_xhtml_count(0);
}

print("# 003 --------------------------------------------------\n");

/* Check that getLeaf creates a proper reference */
{
  $x = new Xhtml("x");
  check_xhtml_count(1);
  $x->addLeaf("y");
  check_xhtml_count(2);
  $y = $x->getLeaf("y");
  check_xhtml_count(2);
  unset($x);
  check_xhtml_count(1);

  if ($y->getTag() != "y")
    die("leaf reference error\n");

  check_xhtml_count(1);
  unset($y);
  check_xhtml_count(0);
}

print("# 004 --------------------------------------------------\n");

/* Check that an imported leaf gets a proper reference */
{
  $x = new Xhtml("x");
  check_xhtml_count(1);
  $y = new Xhtml("y");
  check_xhtml_count(2);
  $x->importNode($y);
  check_xhtml_count(2);
  unset($y);
  check_xhtml_count(2);
  unset($x);
  check_xhtml_count(0);
}

print("# 005 --------------------------------------------------\n");

/* Check that references to objects of classes derived from Xhtml are also
   valid (when returned by getLeaf) */
{
  class Xnoisy extends Xhtml
  {
    function getTag()
    {
      $tag = parent::getTag();
      return "NOISY $tag";
    }
  }

  $x = new Xhtml("x");
  $x->importNode(new Xnoisy("y"));
  check_xhtml_count(2);
  $y = $x->getLeaf("y");
  unset($x);
  check_xhtml_count(1);
  $tag = $y->getTag();
  unset($y);
  check_xhtml_count(0);

  if ($tag != "NOISY y")
    {
      printf("Error: not the expected noisy tag value (%s)\n", $tag);
      exit(1);
    }
}

print("# 006 --------------------------------------------------\n");

/* Check that an Xtext node gets collected with the Xhtml parent */
{
  check_xtext_count(0);
  $x = new Xhtml("x", "Hello");
  check_xhtml_count(1);
  check_xtext_count(1);
  unset($x);
  check_xhtml_count(0);
  check_xtext_count(0);
}

print("Alright.\n");

exit(0);

?>

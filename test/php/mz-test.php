<?php

/* Mangoz - test/php/mz-test.php
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

$use_c_lib = true;

if ($use_c_lib === false) {
  require_once 'mangoz/core/xhtml.php';
}

$impl = $use_c_lib ? "C" : "PHP";
printf("mangoz version: 0x%08X-%s\n", mangoz_get_version(), $impl);

mangoz_set_logging(MZ_LOG_XHTML_FLAG);

/* ------------------------------------------------------------------------- */

function produce($x)
{
  $sp = new MzStringPrinter();
  $x->produce($sp);
  print("--------------------------------------------------\n");
  print($sp->getString());
  print("--------------------------------------------------\n");
}

$x = new Xhtml("div");
$y = new Xhtml("p");
$x->setText("Hello");
$x->importNode($y);
$y->setText("Alright.");
$x->addLeaf("h1", "Title")->setId("main-title");
$x->addLeaf("br");
$x->addText("Here");
$x->setAttribute("id", "main");
produce($x);

/* ------------------------------------------------------------------------- */

$start_time = microtime(1);

$x = new Xhtml("div", "Alright");

if ($x == null)
  die("Xhtml failed");

$x->setAttribute("id", "root");

$y = $x->addLeaf("p", "Nice");

if ($y == null)
  die("addLeaf failed");

$y->setAttribute("id", "foo-bar");

$z = $y->addText("more text");

class Hello extends Xhtml {

  function getTag()
  {
    return parent::getTag() . " (overloaded)";
  }

  function doProduce($out, $indent, $rootFormat)
  {
    $out->produce("<!-- Hello.doProduce -->");
    parent::doProduce($out, $indent, $rootFormat);
  }
}

class MzTestPrinter extends MzPrinter
{
  function clear()
  {
  }

  function produce($str)
  {
    print("<!-- ( -->$str<!-- ) -->");
  }

  function length()
  {
    return 0;
  }
}

$h = new Hello("span");
$h->setAttribute("class", "youpla");
$h->setText("boom");
$x->importNode($h);

if (true)
  {
    printf("test printer\n");
    $p = new MzTestPrinter();
  }
else
  {
    printf("standard printer\n");
    $p = new MzStdPrinter();
  }

$elapsed = microtime(1) - $start_time;
printf("elements built in %f ms\n", $elapsed * 1000.0);

printf("x has %d nodes\n", $x->countNodes());
printf("x id: %s\n", $x->getAttribute("id"));
printf("y text: %s\n", $y->getText());

$found = $x->getLeaf("p", "id", "foo-bar");

if ($found == null)
  print("leaf not found\n");

$start_time = microtime(1);
$x->produce($p);
$elapsed = microtime(1) - $start_time;
printf("produced in %f ms\n", $elapsed * 1000.0);
printf("length: %d\n", $p->length());

printf("x tag: %s\n", $x->getTag());
printf("h tag: %s\n", $h->getTag());
printf("found leaf:\n");
$found->produce($p);

print("\n");

exit(0);

?>

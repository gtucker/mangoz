/* Mangoz - test/boost/api/xhtml_api.cpp
   Copyright (C) 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/boost/mangoz-boost.h>
#include <iostream>
#include <stdio.h>

using namespace mangoz;
using namespace boost;
using namespace std;

static void test(const char *test_id, bool cond)
{
  if (!cond)
    {
      fprintf(stderr, "[%-32s] FAILED\n", test_id);
      exit(EXIT_FAILURE);
    }
  else
    {
      fprintf(stderr, "[%-32s] OK\n", test_id);
    }
}

int main()
{
  StringPrinter sp = StringPrinter();

  // Xtext

  {
    Xtext x = Xtext("Alright");
    test("Xtext.getNodeType", (x.getNodeType() == MZ_XNODE_TEXT));
  }

  {
    string txt1a = "Hello";
    Xtext x = Xtext(txt1a);
    string txt2a = x.getText();
    test("Xtext.getText", (txt1a == txt2a));

    string txt1b = "OK";
    x.setText(txt1b);
    string txt2b = x.getText();
    test("Xtext.setText", (txt1b == txt2b));
  }

  // Xhtml

  return 0;
}

#if 0
class Something
{
public:
  Something(const char *msg_str): msg(msg_str)
  {
    cerr << "Something" << endl;
  }

  virtual ~Something()
  {
    cerr << "~Something" << endl;
  }

  virtual const string &getMsg()
  {
    return this->msg;
  }

private:
  string msg;
};

static shared_ptr<Something> make_something(const char *msg_str)
{
  shared_ptr<Something>ret(new Something(msg_str));

  return ret;
}

  shared_ptr<Something> alright;

  cout << "tag: " << x.getTag() << endl;

  {
    shared_ptr<Something> x = make_something("Hello");
    cerr << "Alright." << endl;
    /*
    shared_ptr<string> tmp_str = make_some_string("Hello");
    cout << "Message: " << *tmp_str << endl;
    str_a = tmp_str;
    */
    cout << "Message: " << x->getMsg() << endl;
    alright = x;
    cout << "Still here: " << x->getMsg() << endl;
  }

  cout << "And now: " << alright->getMsg() << endl;
#endif

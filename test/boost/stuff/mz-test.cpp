/* Mangoz - test/boost/stuff/mz-test.cpp
   Copyright (C) 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/boost/mangoz-boost.h>
#include <iostream>

using namespace mangoz;
using namespace boost;
using namespace std;

int main()
{
  Xtext t = Xtext("First line,\nSecond\n...and third.");

  cout << "full text: " << t.getText() << endl;
  cout << "full text (shared): " << *t.getSharedText() << endl;

  shared_strvec split = t.getSplitText();

  for (strvec::iterator it = split->begin(); it != split->end(); ++it)
    cout << "[" << *it << "]" << endl;

  cout << "text node:" << endl;
  StreamPrinter p = StreamPrinter(cout);
  t.produce(p);

  return 0;
}

#!/bin/sh

set -e

# libmz native tests
out="$TOP_DIR/out/$(uname -m)/tests"
$out/mz/mz-test

# PHP tests
for f in $TOP_DIR/tests/php/*.php
do
    php $f
done

# Python tests
for f in $TOP_DIR/tests/python/*.py
do
    python $f
done

echo "*** ALL TESTS OK ***"

exit 0

/* Mangoz - test/mz/mz-test-monitor.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"
#include <pthread.h>
#include <unistd.h>
#include <assert.h>

#define LOG_TAG "t-mon"
#include <mangoz/tests/log.h>

#define FILE_NAME "mz-test-monitered-file"

struct cb_data {
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	mz_bool running;
	mz_bool notified;
};

static int monitor_cb(const struct mz_monitor_event *ev);

int mz_test_monitor(void)
{
	struct mz_monitor *m;
	struct cb_data cb_data;
	mz_bool notified = false;
	FILE *f;

	f = fopen(FILE_NAME, "w");

	if (f == NULL) {
		LOG("Failed to open file: %s", FILE_NAME);
		return -1;
	}

	cb_data.running = false;
	cb_data.notified = false;
	MZE(pthread_mutex_init(&cb_data.mutex, NULL));
	MZE(pthread_cond_init(&cb_data.cond, NULL));
	MZE(mz_monitor_init(&m, monitor_cb, &cb_data));
	MZE(mz_monitor_add_file(m, MZ_STRLEN(FILE_NAME), &notified));
	MZE(mz_monitor_run(m));

	pthread_mutex_lock(&cb_data.mutex);
	while (!cb_data.running)
		pthread_cond_wait(&cb_data.cond, &cb_data.mutex);
	pthread_mutex_unlock(&cb_data.mutex);

	fprintf(f, "Hello!\n");
	fclose(f);

	pthread_mutex_lock(&cb_data.mutex);
	while (!cb_data.notified)
		pthread_cond_wait(&cb_data.cond, &cb_data.mutex);
	pthread_mutex_unlock(&cb_data.mutex);

	MZE(mz_monitor_cancel(m));
	mz_monitor_free(m);
	pthread_mutex_destroy(&cb_data.mutex);
	pthread_cond_destroy(&cb_data.cond);
	unlink(FILE_NAME);

	return 0;
}

static int monitor_cb(const struct mz_monitor_event *ev)
{
	struct cb_data *data = ev->ctx;
	int stat = 0;

	pthread_mutex_lock(&data->mutex);

	switch (ev->msg) {
	case MZ_MON_RUNNING:
		LOG("running");
		data->running = true;
		break;
	case MZ_MON_STOPPED:
		LOG("stopped");
		data->running = false;
		break;
	case MZ_MON_ERROR:
		assert(!"ERROR!");
		break;
	case MZ_MON_FILE:
		LOG("File changed: %s", ev->file_name->str);
		data->notified = true;
		break;
	default:
		assert(!"invalid message");
		stat = -1;
		break;
	}

	pthread_cond_signal(&data->cond);
	pthread_mutex_unlock(&data->mutex);

	return stat;
}

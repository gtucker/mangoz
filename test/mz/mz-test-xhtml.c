/* Mangoz - test/mz/mz-test-xhtml.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"
#include <mangoz/libmz/util.h>

#define LOG_TAG "t-xhtml"
#include <mangoz/tests/log.h>

int mz_test_xhtml(void)
{
	struct mz_xhtml x;
	struct mz_xhtml leaf;
	struct mz_xhtml leaf2;
	struct mz_xhtml *created;
	struct mz_xhtml *found;
	struct mz_xtext *text;
	struct mz_test_printer printer;
	enum mz_error err;

	MZ_CLEAR(x.xnode);
	MZ_CLEAR(leaf.xnode);
	MZ_CLEAR(leaf2.xnode);

	err = mz_xhtml_init(&x, MZ_STRLEN("xhtml"), true, false);

	if (err < 0) {
		LOG("failed to initialise mz_xhtml (%i)", err);
		return err;
	}

	err = mz_xhtml_set_text(&x, MZ_STRLEN("hello"));

	if (err < 0) {
		LOG("failed to set text (%i)", err);
		return err;
	}

	printer.type = MZ_TEST_FILE_PRINTER;
	printer.data.file = stdout;

	LOG("tag: [%s]", x.tag.str);

	text = mz_xhtml_get_text_node(&x, 0);

	if (text == NULL) {
		LOG("failed to get text node");
		return -1;
	}

	LOG("text: [%s]", text->text.str);

	created = mz_xhtml_add_leaf(&x, MZ_STRLEN("p"), MZ_STRLEN("created"),
				    true, false);

	if (created == NULL) {
		LOG("failed to create leaf");
		return -1;
	}

	err = mz_attribute_set(&x.atts, MZ_STRLEN("id"), MZ_STRLEN("abc"));

	if (err < 0) {
		LOG("failed to set attribute (%i)", err);
		return -1;
	}

	err = mz_attribute_set(&x.atts, MZ_STRLEN("foo"), MZ_STRLEN("bar"));

	if (err < 0) {
		LOG("failed to set attribute (%i)", err);
		return err;
	}

	LOG("id: [%s]", mz_attribute_get(&x.atts, MZ_STRLEN("id"))->str);

	mz_xhtml_init(&leaf, MZ_STRLEN("p"), true, false);
	mz_xhtml_set_text(&leaf, MZ_STRLEN("One,\ntwo,\nthree."));
	mz_attribute_set(&leaf.atts, MZ_STRLEN("height"), MZ_STRLEN("12.54"));
	mz_xhtml_import_node(&x, &leaf.xnode);

	mz_xhtml_init(&leaf2, MZ_STRLEN("div"), true, false);
	mz_attribute_set(&leaf2.atts, MZ_STRLEN("width"), MZ_STRLEN("narrow"));
	mz_attribute_set(&leaf2.atts, MZ_STRLEN("id"), MZ_STRLEN("bingo"));
	mz_xhtml_import_node(&x, &leaf2.xnode);

	err = mz_xhtml_do_produce(&x, &printer, 0, true);

	if (err < 0) {
		LOG("failed to produce Xhtml (%i)", err);
		return err;
	}

	fprintf(stdout, "\n");
	fflush(stdout);

	found = mz_xhtml_get_leaf(&x, MZ_STRLEN("div"), MZ_STRLEN("id"),
				  MZ_STRLEN("bingo"));

	if (found == NULL) {
		LOG("could not find the leaf");
		return -1;
	} else {
		LOG("leaf found:");
		mz_xhtml_do_produce(found, &printer, 0, true);
		fprintf(stdout, "\n");
		fflush(stdout);
	}

	mz_xhtml_free(&x);

	return 0;
}

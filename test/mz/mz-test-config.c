/* Mangoz - test/mz/mz-test-config.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"

#define LOG_TAG "config"
#include <mangoz/tests/log.h>

int mz_test_config(void)
{
	struct mz_config c;
	const struct mz_string *lang;
	const struct mz_string *css;
	const struct mz_core_config *core_config;
	const struct mz_page_config *page_config;
	int i;
	int ret = 0;

	MZ_CLEAR(c);

	MZE(mz_config_init(&c));
	MZE(mz_config_set(&c, "defaultLanguage", "en"));
	MZE(mz_config_set(&c, "baseURL", "http://localhost/mz-test/"));
	MZE(mz_config_set(&c, "staticDir", "static/"));
	MZE(mz_config_set(&c, "publicURL", "/mz-test-public/"));
	MZE(mz_config_set(&c, "defaultCSS", "mangoz.css fancy.css"));
	MZE(mz_config_set(&c, "textbox", "textbox.xml"));

	lang = mz_config_get(&c, "defaultLanguage");

	LOG("lang: %s", lang->str);

	core_config = mz_config_get_core(&c);

	if (core_config == NULL) {
		LOG("failed to get core config");
		ret = -1;
	}

	LOG("core lang: %s", core_config->defaultLanguage->str);

	page_config = mz_config_get_page(&c);

	if (page_config == NULL) {
		LOG("failed to get page config");
		ret = -1;
	}

	LOG("contentType: %s", page_config->contentType->str);

	mz_vector_for_each_n(page_config->css_list, css, i)
		LOG("css[%d]: (%02zu) %s", i, css->len, css->str);

	mz_config_free(&c);

	return ret;
}

/* Mangoz - test/mz/mz-test-xtext.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"
#include <mangoz/libmz/util.h>

#define LOG_TAG "t-xtext"
#include <mangoz/tests/log.h>

static void output_string(struct mz_string *string);

int mz_test_xtext(void)
{
	struct mz_xtext x;
	struct mz_test_printer printer;
	int res;

	MZ_CLEAR(x.xnode);

	if (mz_xtext_init(&x, MZ_STRLEN("That's the text"), false) < 0)
		return -1;

	printer.type = MZ_TEST_STRING_PRINTER;

	if (mz_string_init(&printer.data.string, 0) < 0)
		return -1;

	LOG("text: [%s]", x.text.str);

	res = mz_xtext_do_produce(&x, &printer, 0);
	if (res < 0)
		LOG("Oops");
	else
		LOG("produce OK");

	output_string(&printer.data.string);

	res = mz_xtext_set(&x, MZ_STRLEN("First line\nSecond\n...and third."));
	if (res < 0)
		LOG("Oops");

	res = mz_xtext_do_produce(&x, &printer, 0);
	if (res < 0)
		LOG("Oops");

	output_string(&printer.data.string);

	x.xnode.do_format = true;
	res = mz_xtext_set(&x, MZ_STRLEN("A\n  B  \nC."));
	if (res < 0)
		LOG("Oops");

	res = mz_xtext_do_produce(&x, &printer, 0);
	if (res < 0)
		LOG("Oops");

	output_string(&printer.data.string);

	mz_xtext_free(&x);
	mz_string_free(&printer.data.string);

	return res;
}

static void output_string(struct mz_string *string)
{
	fprintf(stdout, string->str);
	fflush(stdout);
	fprintf(stdout, "\n---\n");
	mz_string_clear(string);

}

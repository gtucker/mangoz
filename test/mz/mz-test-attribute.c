/* Mangoz - test/mz/mz-test-attribute.c
   Copyright (C) 2013 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"

#define LOG_TAG "attr"
#include <mangoz/tests/log.h>

static int test_get_set_attribute(void);
static int test_default_attribute(void);

int mz_test_attribute(void)
{
	int ret = 0;

	if (test_get_set_attribute() < 0)
		ret = -1;

	if (test_default_attribute() < 0)
		ret = -1;

	return ret;
}

static int test_get_set_attribute(void)
{
	struct att_data {
		const char *name;
		size_t nlen;
		const char *value;
		size_t vlen;
	};
	static const struct att_data data[] = {
		{ MZ_STRLEN("apple"), MZ_STRLEN("green") },
		{ MZ_STRLEN("apricot"), MZ_STRLEN("orange") },
		{ MZ_STRLEN("plum"), MZ_STRLEN("blue") },
		{ NULL, 0, NULL, 0 }
	};
	const struct att_data *it;
	struct mz_vector atts;
	int ret = 0;

	LOG("mz_attribute_{get|set}");

	MZE(mz_vector_init(&atts, sizeof(struct mz_attribute), 4));

	for (it = data; it->name != NULL; ++it) {
		if (mz_attribute_set(&atts, it->name, it->nlen,
				     it->value, it->vlen) < 0) {
			ret = -1;
			LOG("Failed to set attribute: %s=%s",
			    it->name, it->value);
			break;
		}
	}

	for (it = data; it->name != NULL; ++it) {
		const struct mz_string * const found =
			mz_attribute_get(&atts, it->name, it->nlen);

		if (found == NULL) {
			LOG("Failed to find attribute: %s", it->name);
			ret = -1;
		} else if (mz_string_cmp(found, it->value, it->vlen)) {
			LOG("Attribute mismatch: %s = %s instead of %s",
			    it->name, found->str, it->value);
		} else {
			LOG("OK %s=%s", it->name, found->str);
		}
	}

	mz_vector_free(&atts);

	return ret;
}

static int test_default_attribute(void)
{
	static const char att_name[] = "att_name";
	static const char att_default[] = "att_default";
	static const char att_value[] = "att_value";
	const struct mz_string *found;
	struct mz_vector atts;
	int ret = 0;

	LOG("mz_attribute_set_default");

	MZE(mz_vector_init(&atts, sizeof(struct mz_attribute), 2));

	if (mz_attribute_get(&atts, MZ_STRLEN(att_name)) != NULL) {
		LOG("Attribute should not be found yet: %s", att_name);
		ret = -1;
	}

	if (mz_attribute_set_default(&atts, MZ_STRLEN(att_name),
				     MZ_STRLEN(att_default)) < 0) {
		LOG("Failed to set default: %s=%s", att_name, att_default);
		ret = -1;
	}

	found = mz_attribute_get(&atts, MZ_STRLEN(att_name));

	if (found == NULL) {
		LOG("Failed to find default attribute: %s", att_name);
		ret = -1;
	} else if (mz_string_cmp(found, MZ_STRLEN(att_default))) {
		LOG("Found wrong default value: %s = %s instead of %s",
		    att_name, found->str, att_default);
		ret = -1;
	} else {
		LOG("OK default: %s=%s (%s)",
		    att_name, found->str, att_default);
	}

	if (mz_attribute_set(&atts, MZ_STRLEN(att_name),
			     MZ_STRLEN(att_value)) < 0) {
		LOG("Failed to set attribute: %s", att_name);
		ret = -1;
	}

	found = mz_attribute_get(&atts, MZ_STRLEN(att_name));

	if (found == NULL) {
		LOG("Failed to find attribute: %s", att_name);
		ret = -1;
	} else if (mz_string_cmp(found, MZ_STRLEN(att_value))) {
		LOG("Found wrong value: %s = %s instead of %s",
		    att_name, found->str, att_value);
		ret = -1;
	} else {
		LOG("OK %s=%s (%s)", att_name, found->str, att_value);
	}

	if (mz_attribute_set_default(&atts, MZ_STRLEN(att_name),
				     MZ_STRLEN(att_default)) < 0) {
		LOG("Failed to set default: %s=%s", att_name, att_default);
		ret = -1;
	}

	found = mz_attribute_get(&atts, MZ_STRLEN(att_name));

	if (found == NULL) {
		LOG("Failed to find default attribute: %s", att_name);
		ret = -1;
	} else if (mz_string_cmp(found, MZ_STRLEN(att_value))) {
		LOG("Found wrong default value: %s = %s instead of %s",
		    att_name, found->str, att_value);
		ret = -1;
	} else {
		LOG("OK default: %s=%s (%s)",
		    att_name, found->str, att_value);
	}

	mz_vector_free(&atts);

	return 0;
}

/* Mangoz - test/mz/mz-test-errors.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"

#define LOG_TAG "errors"
#include <mangoz/tests/log.h>

int mz_test_errors(void)
{
	struct error_code_test {
		int error;
		const char *msg;
	};
	static const struct error_code_test tests[] = {
		{ -1, "Invalid error code" },
		{ 0, "Invalid error code" },
		{ MZ_EALLOC, "Memory allocation failed" },
		{ MZ_ENOTFOUND, "Object not found" },
		{ MZ_EINDEX, "Index out of bounds" },
		{ MZ_EIO, "Input/output error" },
		{ MZ_ESTD, "Standard system error" },
		{ MZ_ECALLBACK, "Callback error" },
		{ MZ_EXML, "XML handling error" },
		{ MZ_EXML+1, "Invalid error code" },
	};
	int i;

	for (i = 0; i < MZ_ARRAY_SZ(tests); ++i) {
		const struct error_code_test *it = &tests[i];
		const char *errmsg = mz_strerror(it->error);

		LOG("error %d: %s", it->error, it->msg);

		if (strcmp(it->msg, errmsg)) {
			LOG("Wrong error message: [%s]", errmsg);
			return -1;
		}
	}

	return 0;
}

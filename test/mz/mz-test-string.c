/* Mangoz - test/mz/mz-test-string.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"
#include <stdio.h>
#include <string.h>

#define LOG_TAG "string"
#include <mangoz/tests/log.h>

#define ESTR(e) (e ? "ERROR" : "OK")

#if MZ_USE_SNPRINTF
static int test_snprintf(void);
#endif

int mz_test_string(void)
{
	static const char *str_a = "hello";
	static const char *str_b = "bye-bye";
	struct mz_string sa;
	struct mz_string sb;
	struct mz_string s;
	const size_t str_a_len = strlen(str_a);
	const size_t str_b_len = strlen(str_b);
	const char *trimmed;
	size_t trimmed_len;
	int res = 0; /* ToDo: deal with individual test results */
	int i;

	if (mz_string_init(&sa, 0) < 0)
		return -1;

	if (mz_string_init(&sb, 0) < 0) {
		mz_string_free(&sa);
		return -1;
	}

	if (mz_string_init(&s, 0) < 0) {
		mz_string_free(&sa);
		mz_string_free(&sb);
		return -1;
	}

	if (mz_string_set_nolen(&sa, str_a) < 0)
		return -1;

	if (sa.len != strlen(str_a))
		return -1;

	LOG("set (%zu, %zu, %s) OK", sa.len, sa.n, sa.str);

	if (mz_string_set(&sb, str_b, str_b_len) < 0)
		return -1;

	if (sb.len != strlen(str_b))
		return -1;

	LOG("set (%zu, %zu, %s) OK", sb.len, sb.n, sb.str);

	if (mz_string_cat_str(&s, &sa) < 0)
		res = -1;

	LOG("cat_str (%zu, %zu, %s) %s", s.len, s.n, s.str,
	    res ? "ERROR" : "OK");

	if (mz_string_cmp_str(&s, &sa))
		res = -1;

	LOG("cmp_str (%s) %s", s.str, res ? "ERROR" : "OK");

	if (mz_string_cat_nolen(&s, " ") < 0)
		res = -1;

	LOG("cat (%zu, %zu, %s) %s", s.len, s.n, s.str,
	    res ? "ERROR" : "OK");

	if (mz_string_cat_str(&s, &sb) < 0)
		res = -1;

	LOG("cat_str (%zu, %zu, %s) %s", s.len, s.n, s.str,
	    res ? "ERROR" : "OK");

	if (mz_string_cmp_nolen(&s, "hello bye-bye"))
		res = -1;

	LOG("cmp (%s) %s", s.str, res ? "ERROR" : "OK");

	if (mz_string_set(&sa, MZ_STRLEN("bingo")) < 0)
		res = -1;

	LOG("set (%s) %s", s.str, res ? "ERROR" : "OK");

	i = mz_string_cmp(&sa, MZ_STRLEN("bingo"));

	if (i != 0) {
		res = -1;
		LOG("cmp bingo %d ERROR", i);
	}

	i = mz_string_cmp(&sa, MZ_STRLEN("bing"));

	if (i != 1) {
		res = -1;
		LOG("cmp bing %d ERROR", i);
	}

	i = mz_string_cmp(&sa, MZ_STRLEN("binga"));

	if (i != 1) {
		res = -1;
		LOG("cmp binga %d ERROR", i);
	}

	i = mz_string_cmp(&sa, MZ_STRLEN("bingoz"));

	if (i != -1) {
		res = -1;
		LOG("cmp bingoz %d ERROR", i);
	}

	if (mz_string_set(&s, "", 0) < 0)
		res = -1;

	LOG("set (%s) %s", s.str, res ? "ERROR" : "OK");

	for (i = 0; i < 200; ++i)
		if (mz_string_cat(&s, str_a, str_a_len) < 0)
			res = -1;

	if (strncmp(s.str, str_a, str_a_len)
	    || (strncmp(&s.str[str_a_len * 199], str_a, str_a_len)))
		res = -1;

	LOG("cat (%zu, %zu) %s", s.len, s.n, res ? "ERROR" : "OK");

	mz_string_get_trimmed(MZ_STRLEN("   Hello\n   "), &trimmed,
			      &trimmed_len);
	mz_string_set(&s, trimmed, trimmed_len);

	if (strcmp(s.str, "Hello"))
		res = -1;

	LOG("trim (%zu) [%s] %s", s.len, s.str, ESTR(res));

	mz_string_free(&sa);
	mz_string_free(&sb);
	mz_string_free(&s);

#if MZ_USE_SNPRINTF
	if (test_snprintf() < 0)
	  res = -1;
#endif

	return res;
}

#if MZ_USE_SNPRINTF
static int test_snprintf(void)
{
	static const char *FRUIT[8] = {
		"apple",
		"orange",
		"banana",
		"kiwi",
		"lemon",
		"pear",
		"mango",
		"avocado",
	};
	struct mz_string s;
	struct mz_string really_long;
	int i;
	int res;

	res = mz_string_init(&s, 0);

	if (res < 0)
		return res;

	res = mz_string_init(&really_long, 1024);

	if (res < 0) {
		mz_string_free(&s);
		return res;
	}

	srand(1);

	for (i = 0; (i < 150) && !res; ++i) {
		const int index = rand() & 0x7;

		res = mz_string_cat_nolen(&really_long, FRUIT[index]);

		if (!res && (i != 149))
			res = mz_string_cat(&really_long, MZ_STRLEN(" "));
	}

	if (res < 0)
		goto cleanup;

	LOG("n: %zu", s.n);

	/* short */
	res = mz_string_printf(&s, "Yes.");

	if (res < 0)
		goto cleanup;

	LOG("%zu [%s]", s.n, s.str);

	/* longer */
	res = mz_string_printf(&s, "%f / %d = %f", 1.0, 7, 1.0 / 7);

	if (res < 0)
		goto cleanup;

	LOG("%zu [%s]", s.n, s.str);

	/* really long */
	res = mz_string_printf(&s, "Huge fruit basket (%zu): %s",
			       really_long.len, really_long.str);

	if (res < 0)
		goto cleanup;

	LOG("%zu [%s]", s.n, s.str);

	/* short again */
	res = mz_string_printf(&s, "Alright.");

	if (res < 0)
		goto cleanup;

	LOG("%zu [%s]", s.n, s.str);

	/* empty */
	res = mz_string_printf(&s, "", really_long.str);

	if (res < 0)
		goto cleanup;

	LOG("%zu [%s]", s.n, s.str);

cleanup:
	mz_string_free(&s);
	mz_string_free(&really_long);

	return res;
}
#endif

/* Mangoz - test/mz/mz-test-xdoc.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"
#include <mangoz/libmz/util.h>
#include <string.h>

#define LOG_TAG "xdoc"
#include <mangoz/tests/log.h>

int mz_test_xdoc(void)
{
	struct mz_test_printer printer;
	struct mz_xdoc x;
	struct mz_xhtml root;
	struct mz_xhtml *leaf;

	MZ_CLEAR(x);
	MZ_CLEAR(root.xnode);

	MZE(mz_xhtml_init(&root, MZ_STRLEN("xhtml"), true, true));
	MZE(mz_xdoc_init(&x, &root));

	MZE(mz_attribute_set(&root.atts, MZ_STRLEN("id"), MZ_STRLEN("main")));

	leaf = mz_xhtml_add_leaf(&root, MZ_STRLEN("div"),
				 MZ_STRLEN("Hi there!"), true, true);
	MZE(leaf != NULL);
	MZE(mz_attribute_set(&leaf->atts, MZ_STRLEN("class"),
			     MZ_STRLEN("test")));

	printer.type = MZ_TEST_FILE_PRINTER;
	printer.data.file = stdout;
	MZE(mz_xdoc_do_produce(&x, &printer, 0));

	MZE(mz_xdoc_set_std_doctype(&x, MZ_DTD_XHTML11));
	MZE(mz_xdoc_do_produce(&x, &printer, 0));

	LOG("Doctype id: %d", x.doctype->dtd);
	if (x.doctype->dtd != MZ_DTD_XHTML11) {
		LOG("Doctype error!");
		return -1;
	}

	{
		static const char *test_header = "<!DOCTYPE Alright>";
		const size_t header_len = strlen(test_header);

		MZE(mz_xdoc_set_header(&x, test_header, header_len));
		LOG("Doctype header: %s", x.header.str);
		if (strcmp(x.header.str, test_header)) {
			LOG("Doctype header error!");
			return -1;
		}
	}

	{
		static const char *std_header = \
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" "
			"\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">";
		const struct mz_doctype *std_doctype;

		std_doctype = mz_xdoc_get_std_doctype(MZ_DTD_XHTML11);

		LOG("XHTML11 header:\n%s", std_doctype->header);
		if (strcmp(std_header, std_doctype->header)) {
			LOG("XHTML11 doctype header error!");
			return -1;
		}

		LOG("XHTML11 DTD id: %d", std_doctype->dtd);
		if (std_doctype->dtd != MZ_DTD_XHTML11) {
			LOG("XHTML11 DTD id error!");
			return -1;
		}
	}

	mz_xdoc_free(&x);
	mz_xhtml_free(&root);

	return 0;
}

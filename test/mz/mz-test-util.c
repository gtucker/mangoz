/* Mangoz - test/mz/mz-test-util.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"
#include <string.h>

#define LOG_TAG "util"
#include <mangoz/tests/log.h>

static int test_except_fail(void);
static int test_except_pass(void);

static int failure(void);
static int success(void);
static int test_const_str_len(const char *str, size_t len);

int mz_test_util(void)
{
	if (test_except_fail() != -1) {
		LOG("MZ_EXCEPT(-1) failed");
		return -1;
	}

	if (test_except_pass() != 1) {
		LOG("MZ_EXCEPT(0) failed");
		return -1;
	}

	if ((test_const_str_len(MZ_STRLEN("How about that?")) < 0)
	    || (test_const_str_len(MZ_STRLEN("")) < 0)) {
		LOG("MZ_STRLEN failed");
		return -1;
	}

	return 0;
}

static int test_except_fail(void)
{
	MZE(failure());
	return 0;
}

static int test_except_pass(void)
{
	MZE(success());
	return 1;
}

static int failure(void)
{
	return -1;
}

static int success(void)
{
	return 0;
}

static int test_const_str_len(const char *str, size_t len)
{
	const size_t test_len = strlen(str);

	if (test_len != len) {
		LOG("wrong length: [%s] %zu instead of %zu",
		    str, test_len, len);
		return -1;
	}

	return 0;
}

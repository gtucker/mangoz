/* Mangoz - test/mz/mz-test.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"
#include <assert.h>

#define LOG_TAG "mz-test"
#include <mangoz/tests/log.h>

static int test_produce(void *printer, const char *str, size_t len);
static struct mz_xtext *test_create_xtext(const char *text, size_t len,
					  mz_bool do_format);
static struct mz_xhtml *test_create_xhtml(const char *tag, size_t tag_len,
					  mz_bool do_format, mz_bool do_short);
static int add_pseudo_garbage(struct mz_xnode *xnode);
static void collect_pseudo_garbage(void);

const char *mz_test_data_path;

const struct mz_callbacks mz_callbacks = {
	.printer_produce = test_produce,
	.create_xtext = test_create_xtext,
	.create_xhtml = test_create_xhtml,
};

struct pseudo_garbage {
	struct mz_xnode *xnode;
	struct pseudo_garbage *next;
};

static struct pseudo_garbage *g_stuff = NULL;
static size_t g_stuff_counter = 0;

typedef int (*test_func_t)(void);

int main(int argc, char **argv)
{
	struct test_case {
		char *name;
		test_func_t func;
	};
	static const struct test_case cases[] = {
		{ "util", mz_test_util },
		{ "errors", mz_test_errors },
		{ "string", mz_test_string },
		{ "attribute", mz_test_attribute },
		{ "xtext", mz_test_xtext },
		{ "xhtml", mz_test_xhtml },
		{ "xdoc", mz_test_xdoc },
		{ "xparser", mz_test_xparser },
		{ "monitor", mz_test_monitor },
		{ "config", mz_test_config },
		{ NULL, NULL }
	};
	const struct test_case *it;
	const char *test_name;
	int res = 0;

	if (argc < 2)
		mz_test_data_path = ".";
	else
		mz_test_data_path = argv[1];

	if (argc < 3)
		test_name = NULL;
	else
		test_name = argv[2];

	LOG("data path: %s", mz_test_data_path);

	for (it = cases; it->func != NULL; ++it) {
		if (test_name && strcmp(test_name, it->name))
			continue;

		LOG("- libmz: %s -", it->name);

		if (it->func() < 0) {
			LOG("%s failed", it->name);
			res = -1;
		}

		collect_pseudo_garbage();
	}

	if (!res)
		LOG("PASSED");
	else
		LOG("FAILED");

	exit((res < 0) ? EXIT_FAILURE : EXIT_SUCCESS);
}

static int test_produce(void *vprinter, const char *str, size_t len)
{
	struct mz_test_printer *printer = vprinter;
	int res = 0;

	switch (printer->type) {
	case MZ_TEST_FILE_PRINTER:
		fprintf(printer->data.file, "%s", str);
		break;
	case MZ_TEST_STRING_PRINTER:
		res = mz_string_cat(&printer->data.string, str, len);
		break;
	}

	return 0;
}

static struct mz_xtext *test_create_xtext(const char *text, size_t len,
					  mz_bool do_format)
{
	struct mz_xtext *xtext = malloc(sizeof(struct mz_xtext));

	if (xtext == NULL)
		return NULL;

	MZ_CLEAR(xtext->xnode);

	if (mz_xtext_init(xtext, text, len, do_format) < 0) {
		free(xtext);
		return NULL;
	}

	if (add_pseudo_garbage(&xtext->xnode) < 0) {
		mz_xtext_free(xtext);
		return NULL;
	}

	return xtext;
}

static struct mz_xhtml *test_create_xhtml(const char *tag, size_t tag_len,
					  mz_bool do_format, mz_bool do_short)
{
	struct mz_xhtml *xhtml = malloc(sizeof(struct mz_xhtml));

	if (xhtml == NULL)
		return NULL;

	MZ_CLEAR(xhtml->xnode);

	if (mz_xhtml_init(xhtml, tag, tag_len, do_format, do_short) < 0) {
		free(xhtml);
		return NULL;
	}

	if (add_pseudo_garbage(&xhtml->xnode) < 0) {
		mz_xhtml_free(xhtml);
		return NULL;
	}

	return xhtml;
}

static int add_pseudo_garbage(struct mz_xnode *xnode)
{
	struct pseudo_garbage *g = malloc(sizeof(struct pseudo_garbage));

	if (g == NULL)
		return -MZ_EALLOC;

	++g_stuff_counter;
	g->xnode = xnode;
	g->next = g_stuff;
	g_stuff = g;

	return 0;
}

static void collect_pseudo_garbage(void)
{
	struct pseudo_garbage *g = g_stuff;

	LOG("=== collecting garbage (%zu) ===", g_stuff_counter);

	while (g != NULL) {
		struct pseudo_garbage *next;

		switch (g->xnode->node_type) {
		case MZ_XNODE_TEXT: {
			struct mz_xtext *xtext;
			xtext = MZ_GET_PARENT(g->xnode, mz_xtext, xnode);
			mz_xtext_free(xtext);
			break;
		}
		case MZ_XNODE_HTML: {
			struct mz_xhtml *xhtml;
			xhtml = MZ_GET_PARENT(g->xnode, mz_xhtml, xnode);
			mz_xhtml_free(xhtml);
			break;
		}
		default:
			assert(!"That's not right!");
			break;
		}

		next = g->next;
		free(g);
		g = next;
	}

	g_stuff = NULL;
	g_stuff_counter = 0;
}

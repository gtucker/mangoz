/* Mangoz - test/mz/mz-tests.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_TESTS_H
#define INCLUDE_TESTS_H 1

#include <stdio.h>
#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>

enum mz_test_printer_type {
	MZ_TEST_FILE_PRINTER = 0,
	MZ_TEST_STRING_PRINTER,
};

struct mz_test_printer {
	enum mz_test_printer_type type;
	union {
		FILE *file;
		struct mz_string string;
	} data;
};

extern const char *mz_test_data_path;

extern int mz_test_util(void);
extern int mz_test_errors(void);
/*extern int mz_test_vector(void);*/
extern int mz_test_string(void);
extern int mz_test_attribute(void);
extern int mz_test_xtext(void);
extern int mz_test_xhtml(void);
extern int mz_test_xdoc(void);
extern int mz_test_xparser(void);
extern int mz_test_monitor(void);
extern int mz_test_config(void);

#endif /* INCLUDE_TESTS_H */

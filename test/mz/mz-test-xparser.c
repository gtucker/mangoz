/* Mangoz - test/mz/mz-test-xparser.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mz-tests.h"
#include <mangoz/libmz/util.h>
#include <string.h>

#define LOG_TAG "t-parser"
#include <mangoz/tests/log.h>

static int test_file_parser(void);
static int test_string_parser(void);
static int filecmp(const char *fname1, const char *fname2);

int mz_test_xparser(void)
{
	int ret = 0;

	if (test_file_parser() < 0) {
		LOG("file parser failed!");
		ret = -1;
	}

	if (test_string_parser() < 0) {
		LOG("string parser failed!");
		ret = -1;
	}

	return ret;
}

static int test_file_parser(void)
{
	static const char *out_fname = "output.xml";
	struct mz_xdoc xdoc;
	struct mz_test_printer printer;
	struct mz_string doc_path;
	struct mz_xparser xparser;
	int ret = 0;

	MZ_CLEAR(xdoc);
	MZ_CLEAR(xparser);

	MZE(mz_xparser_init(&xparser, mz_std_handle_format, NULL));
	MZE(mz_string_init(&doc_path, 0));
	MZE(mz_string_set_nolen(&doc_path, mz_test_data_path));
	MZE(mz_string_cat(&doc_path, MZ_STRLEN("/simple.xml")));
	printer.type = MZ_TEST_FILE_PRINTER;
	LOG("XML doc: %s", doc_path.str);

	MZE(mz_xparser_parse_file(&xparser, &xdoc, doc_path.str, true));
	printer.data.file = fopen(out_fname, "w");
	MZE(mz_xdoc_do_produce(&xdoc, &printer, 0));
	fclose(printer.data.file);
	mz_xdoc_free(&xdoc);
	if (filecmp(doc_path.str, out_fname)) {
		LOG("parse_file with format failed");
		ret = -1;
	}

	MZE(mz_xparser_parse_file(&xparser, &xdoc, doc_path.str, false));
	printer.data.file = fopen(out_fname, "w");
	MZE(mz_xdoc_do_produce(&xdoc, &printer, 0));
	fclose(printer.data.file);
	mz_xdoc_free(&xdoc);
	if (filecmp(doc_path.str, out_fname)) {
		LOG("parse_file without format failed");
		ret = -1;
	}

	mz_string_free(&doc_path);
	mz_xparser_free(&xparser);

	return ret;
}

static int test_string_parser(void)
{
	struct mz_string ref;
	struct mz_string doc_path;
	struct mz_test_printer printer;
	struct mz_xdoc xdoc;
	struct mz_xparser xparser;
	int ret = 0;

	MZ_CLEAR(xdoc);
	MZ_CLEAR(xparser);

	MZE(mz_xparser_init(&xparser, mz_std_handle_format, NULL));
	MZE(mz_string_init(&doc_path, 0));
	MZE(mz_string_set_nolen(&doc_path, mz_test_data_path));
	MZE(mz_string_cat(&doc_path, MZ_STRLEN("/string-test.xml")));
	MZE(mz_string_init(&ref, 0));
	MZE(mz_string_cat_file(&ref, doc_path.str));
	MZE(mz_string_init(&printer.data.string, 0));
	printer.type = MZ_TEST_STRING_PRINTER;
	LOG("XML doc: %s", doc_path.str);

	MZE(mz_xparser_parse_string(&xparser, &xdoc, ref.str, ref.len, true));
	MZE(mz_xdoc_do_produce(&xdoc, &printer, 0));
	if (mz_string_cmp_str(&ref, &printer.data.string)) {
		LOG("parse_string with format failed");
		ret = -1;
		printf("ref:\n%s\nparsed:\n%s\n",
		       ref.str, printer.data.string.str);
	}
	mz_xdoc_free(&xdoc);
	mz_string_clear(&printer.data.string);

	MZE(mz_xparser_parse_string(&xparser, &xdoc, ref.str, ref.len, false));
	MZE(mz_xdoc_do_produce(&xdoc, &printer, 0));
	if (mz_string_cmp_str(&ref, &printer.data.string)) {
		LOG("parse_string without format failed");
		ret = -1;
		printf("ref:\n%s\nparsed:\n%s\n",
		       ref.str, printer.data.string.str);
	}
	mz_xdoc_free(&xdoc);
	mz_string_clear(&printer.data.string);

	mz_string_free(&doc_path);
	mz_string_free(&ref);
	mz_xparser_free(&xparser);

	return ret;
}

static int filecmp(const char *fname1, const char *fname2)
{
	struct mz_string str1, str2;
	int ret = -1;

	MZE(mz_string_init(&str1, 0));
	MZE(mz_string_cat_file(&str1, fname1));
	MZE(mz_string_init(&str2, 0));
	MZE(mz_string_cat_file(&str2, fname2));
	ret = mz_string_cmp_str(&str1, &str2);
	mz_string_free(&str1);
	mz_string_free(&str2);

	return ret;
}

# Mangoz - test/python/xparser_api.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import os
import sys
import os
top_dir = os.environ['TOP_DIR']
sys.path.append(os.path.join(top_dir, 'python'))
import mangoz as mz

class DummyParser(mz.Xparser):
    def __init__(self, *args, **kw):
        super(DummyParser, self).__init__(*args, **kw)
        self.n = 0

    def handleFormat(self, xhtml, doFormat):
        self.n += 1
        return super(DummyParser, self).handleFormat(xhtml, doFormat)

def test(test_id, cond):
    if not cond:
        print("[%-32s] FAILED" % test_id)
        sys.exit(1)
    else:
        print("[%-32s] OK" % test_id)

if len(sys.argv) < 2:
    data_path = os.path.join(top_dir, 'test', 'data')
else:
    data_path = sys.argv[1]

sp = mz.StringPrinter()

try:
    def filecmp(fname1, fname2):
        f1 = open(fname1, "r")
        f2 = open(fname2, "r")
        data1 = f1.read()
        data2 = f2.read()
        return data1 == data2

    input_file_name = os.path.join(data_path, "simple.xml")
    output_file_name = "output-python.xml"
    dp = DummyParser()
    xdoc = dp.parseFile(input_file_name)
    test("Xparser sub-class", (dp.n == 8))
    sp.clear()
    xdoc.produce(sp)
    out = open(output_file_name, "w")
    out.write(sp.getString())
    out.close()
    test("Xparser.parseFile", filecmp(input_file_name, output_file_name))
except IOError, e:
    print(e)
    sys.exit(1)

try:
    f = open(os.path.join(data_path, "string-test.xml"))
    input_data = f.read()
    f.close()
    xdoc = mz.std_parser.parseString(input_data)
    sp.clear()
    xdoc.produce(sp)
    output_data = sp.getString()
    test("Xparser.parseString", (output_data == input_data))
except IOError, e:
    print(e)
    sys.exit(1)

sys.exit(0)

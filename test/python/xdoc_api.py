# Mangoz - test/python/xdoc_api.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import sys
import os
sys.path.append(os.path.join(os.environ['TOP_DIR'], 'python'))
import mangoz as mz

def test(test_id, cond):
    if not cond:
        print("[%-32s] FAILED" % test_id)
        sys.exit(1)
    else:
        print("[%-32s] OK" % test_id)

root = mz.Xhtml("top")
root.addLeaf("inner").setId("main")

doc = mz.Xdocument(root)
root_b = doc.getRoot()
test("Xdocument.getRoot", (root_b == root))

dtd1 = mz.DTD_TRANSITIONAL
doc.setDoctype(dtd1)
dtd2 = doc.getDoctype()
test("Xdocument.{get,set}Doctype", (dtd1 == dtd2))

header1 = "<!DOCTYPE html>"
doc.setDoctypeHeader(header1)
header2 = doc.getDoctypeHeader()
test("Xdocument.{get,set}DoctypeHeader", (header1 == header2))

# getStdDoctype

name1a = "test"
value1a = "something"
doc.setAttribute(name1a, value1a)
value2a = doc.getAttribute(name1a)
test("Xdocument.getAttribute", (value1a == value2a))

name1b = "blah"
value1b = "nothing"
doc.setAttribute(name1b, value1b)
value2b = doc.getAttribute(name1b)
test("Xdocument.setAttribute", ((value1a == value2a) and (value1b == value2b)))

out1 = "<!DOCTYPE html>\n<top>\n  <inner id=\"main\" />\n</top>\n"""
sp = mz.StringPrinter()
doc.produce(sp)
out2 = sp.getString()
test("Xdocument.produce", (out1 == out2))

std_header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \
\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">"
xhtml11_header = mz.Xdocument.getStdDoctypeHeader(mz.DTD_XHTML11)
test("Xdocument.getStdDoctypeHeader", (std_header == xhtml11_header))

sys.exit(0)

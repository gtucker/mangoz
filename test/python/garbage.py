# Mangoz - test/python/garbage.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import sys
import os
top_dir = os.environ['TOP_DIR']
sys.path.append(os.path.join(top_dir, 'python'))
import mangoz as mz

def check_count(n, m, node):
    if n != m:
        print("MEMORY LEAK! {0} instead of {1} {2} counts".format(m, n, node))
        sys.exit(1)

def check_xtext_count(n):
    check_count(n, mz.xtext_count(), 'Xtext')

def check_xhtml_count(n):
    check_count(n, mz.xhtml_count(), 'Xhtml')

check_xtext_count(0)
check_xhtml_count(0)

print("# 001 --------------------------------------------------");

# Simple case: create an object and then free it
x = mz.Xhtml('x')
check_xhtml_count(1)
del(x)
check_xhtml_count(0)

print("# 002 --------------------------------------------------");

# Check that all the leaves get freed when the root is freed
x = mz.Xhtml('x')
check_xhtml_count(1)
x.addLeaf('y')
check_xhtml_count(2)
del(x)
check_xhtml_count(0)

print("# 003 --------------------------------------------------");

# Check that getLeaf creates a proper reference
x = mz.Xhtml('x')
check_xhtml_count(1)
x.addLeaf('y')
check_xhtml_count(2)
y = x.getLeaf('y')
check_xhtml_count(2)
del(x)
check_xhtml_count(1)
if y.getTag() != 'y':
    print("leaf reference error")
    sys.exit(1)
del(y)
check_xhtml_count(0)

print("# 004 --------------------------------------------------");

# Check that an imported leaf gets a proper reference
x = mz.Xhtml('x')
check_xhtml_count(1)
y = mz.Xhtml('y')
check_xhtml_count(2)
x.importNode(y)
check_xhtml_count(2)
del(y)
check_xhtml_count(2)
del(x)
check_xhtml_count(0)

print("# 005 --------------------------------------------------");

# Check that references to objects of classes derived from Xhtml are also valid
# (when returned by getLeaf)
class Xnoisy(mz.Xhtml):
    def getTag(self):
        return "NOISY {0}".format(super(Xnoisy, self).getTag())

x = mz.Xhtml('x')
check_xhtml_count(1)
x.importNode(Xnoisy('y'))
check_xhtml_count(2)
y = x.getLeaf('y')
check_xhtml_count(2)
del(x)
check_xhtml_count(1)
tag = y.getTag()
del(y)
check_xhtml_count(0)
if tag != "NOISY y":
    print("Error: not the expected noisy tag value: {0}".format(tag))
    sys.exit(1)

print("# 006 --------------------------------------------------");

# Check that an Xtext node gets collected with the Xhtml parent
check_xtext_count(0)
x = mz.Xhtml('x', "Hello")
check_xhtml_count(1)
check_xtext_count(1)
del(x)
check_xhtml_count(0)
check_xtext_count(0)

print("# 007 --------------------------------------------------");

# Check that an Xdocument has its root element collected
check_xhtml_count(0)
xml = "<top><a>Hello</a><b id=\"hey\"/><c><d a=\"b\"/>Alright</c></top>"
xdoc = mz.std_parser.parseString(xml)
check_xtext_count(2)
check_xhtml_count(5)
del(xdoc)
check_xtext_count(0)
check_xtext_count(0)

print("Garbage collected all right.")

sys.exit(0)

# Mangoz - test/python/xhtml_api.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import sys
import os
top_dir = os.environ['TOP_DIR']
sys.path.append(os.path.join(top_dir, 'python'))
import mangoz as mz

sp = mz.StringPrinter()

def test(test_id, cond):
    if not cond:
        print("[%-32s] FAILED" % test_id)
        sys.exit(1)
    else:
        print("[%-32s] OK" % test_id)

unistr1 = u"Unicode: " + u'\xe9'

# Xtext

x = mz.Xtext("Alright");
test("Xtext.getNodeType", (x.getNodeType() == mz.XNODE_TEXT));

txt1 = "Hello"
x = mz.Xtext(txt1)
txt2 = x.getText()
test("Xtext.getText", (txt1 == txt2))

txt1a = "OK"
x.setText(txt1a)
txt2 = x.getText()
test("Xtext.setText", (txt1a == txt2))

uni1 = unistr1
x.setText(uni1)
uni2 = x.getText().decode('utf-8')
test("Xtext.setText (Unicode)", (uni1 == uni2))

int_text1 = str(123)
x.setText(123)
int_text2 = x.getText()
test("Xtext.setText (Integer)", (int_text1 == int_text2))

list1 = ["A", "B", "C"]
txt = "A\nB\nC"
x.setText(txt)
list2 = x.getSplitText()
test("Xtext.getSplitText", (list1 == list2))

txt = "That is \nsome piece of text."
x = mz.Xtext(txt)
y = mz.Xtext(txt)
test("Xtext.equals", x.equals(y))

# Xhtml

x = mz.Xhtml("x")
test("Xhtml.getNodeType", (x.getNodeType() == mz.XNODE_HTML))

tag1 = "z"
x = mz.Xhtml(tag1)
tag2 = x.getTag()
test("Xhtml.getTag", (tag1 == tag2))

key = "test-key"
value1 = "test-value"
x.setAttribute(key, value1)
value2 = x.getAttribute(key)
test("Xhtml.setAttribute", (value1 == value2))

key = "uni"
value1 = u"unicode"
x.setAttribute(key, value1)
value2 = x.getAttribute(key)
test("Xhtml.setAttribute (Unicode)", (value1 == value2))

int1 = str(456)
key = "int-value"
x.setAttribute(key, 456)
int2 = x.getAttribute(key)
test("Xhtml.setAttribute (Integer)", (int1 == int2))

key = "another-key"
value1 = "something-else"
x.setAttribute(key, value1)
value2 = x.getAttribute(key)
test("Xhtml.getAttribute", (value1 == value2))

id1 = "test-id"
x.setId(id1)
id2 = x.getId()
test("Xhtml.{get,set}Id", (id1 == id2))

cls1 = "test-class"
x.setClass(cls1)
cls2 = x.getClass()
test("Xhtml.{get,set}Class", (cls1 == cls2))

att1 = { "key-a": "value-a", "key-b": "value-b", "key-c": "value-c" }
x = mz.Xhtml("z")
for k, v in att1.items():
    x.setAttribute(k, v)
att2 = x.getAttributes()
test("Xhtml.getAttributes", (att1 == att2))

# Xhtml: text

txt1 = "Some text."
x = mz.Xhtml("z", txt1)
txt2 = x.getText()
test("Xhtml.getText", (txt1 == txt2))

txt1a = "First text"
txt1b = "Second text"
x = mz.Xhtml("z")
x.addText(txt1a)
x.addText(txt1b)
txt2a = x.getText(0)
txt2b = x.getText(1)
test("Xhtml.addText", ((txt1a, txt1b) == (txt2a, txt2b)))

uni1 = unistr1
x = mz.Xhtml("uni")
x.addText(uni1)
uni2 = x.getText().decode('utf-8')
test("Xhtml.addText (Unicode)", (uni1 == uni2))

txt1 = "The text."
x = mz.Xhtml("z")
x.setText(txt1)
txt2 = x.getText()
test("Xhtml.setText", (txt1 == txt2))

uni1 = unistr1
x = mz.Xhtml("z")
x.setText(uni1)
uni2 = x.getText().decode('utf-8')
test("Xhtml.setText (Unicode)", (uni1 == uni2))

list1 = ["A", "B", "C"]
txt = "A\nB\nC"
x = mz.Xhtml("z")
x.setText(txt)
list2 = x.getSplitText()
test("Xhtml.getSplitText", (list1 == list2))

txt1 = "Some piece of text..."
x = mz.Xhtml("z")
node1 = x.addText(txt1)
node2 = x.getTextNode()
test("Xhtml.getTextNode", (node1 == node2))

txt = ["Hello", "Bye-bye", "Good night", "So long"]
n = len(txt)
x = mz.Xhtml("z")
for t in txt:
    x.addText(t)
m = x.countText()
test("Xhtml.countText", (n == m))

# Xhtml: leaves

tag1 = "y"
x = mz.Xhtml("x")
x.addLeaf(tag1)
y = x.getLeaf(tag1)
if y is not None:
    tag2 = y.getTag()
else:
    tag2 = None
test("Xhtml.addLeaf", (tag1 == tag2))

tag1a = "y"
tag1b = "z"
id1a = "id-1"
id1b = "id-2"
id1c = "id-3"
id1 = [id1a, id1b, None, id1c]
x = mz.Xhtml("x")
x.addLeaf(tag1a).setId(id1a)
x.addLeaf(tag1b).setId(id1b)
x.addLeaf(tag1a).setId(id1c)
x2a = x.getIndexedLeaf(tag1a, 0)
x2b = x.getIndexedLeaf(tag1b, 0)
x2c = x.getIndexedLeaf(tag1b, 1)
x2d = x.getIndexedLeaf(tag1a, 1)
id2 = [x2a.getId(), x2b.getId(), x2c, x2d.getId()]
test("Xhtml.getIndexedLeaf", (id1 == id2))

tag1a = "y"
tag1b = "z"
x = mz.Xhtml("x")
x.addLeaf(tag1a)
x.addLeaf(tag1b)
y = x.getLeaf(tag1a)
z = x.getLeaf(tag1b)
if y is not None:
    tag2a = y.getTag()
else:
    tag2a = None
if z is not None:
    tag2b = z.getTag()
else:
    tag2b = None
test("Xhtml.getLeaf", ((tag1a, tag1b) == (tag2a, tag2b)))

tags = dict()
for tag in ["a", "b", "c", "d"]:
    tags[tag] = tag
x = mz.Xhtml("x")
for tag in tags.keys():
    x.addLeaf(tag)
leaves = x.getLeaves()
test_pass = True
for leaf in leaves:
    tag = leaf.getTag()
    if tag not in tags.keys():
        test_pass = False
        break
    del tags[tag]
test("Xhtml.getLeaves", test_pass)

tags = ["a", "b", "c", "d"]
n = len(tags)
x = mz.Xhtml("x")
for t in tags:
    x.addLeaf(t)
m = x.countLeaves()
test("Xhtml.countLeaves", (n == m))

# Xhtml: nodes

node_tags = ["a", "b", "c", "d"]
n = len(node_tags)
txt1 = "That's a text leaf."
x = mz.Xhtml("x")
for t in node_tags:
    x.addLeaf(t)
x.addText(txt1)
test_pass = True
for i in range(n):
    node = x.getNode(i)
    if (node is None) or (node.getTag() != node_tags[i]):
        test_pass = False
        break
txt_node = x.getNode(n)
if txt_node.getText() != txt1:
    test_pass = False
if x.getNode(n + 1) is not None:
    test_pass = False
if x.getNode(-1) != txt_node:
    test_pass = False
test("Xhtml.getNode", test_pass)

n1 = mz.Xhtml("a")
n2 = mz.Xtext("Hey")
n3 = mz.Xhtml("b")
n4 = mz.Xtext("Alright")
nodes1 = [n1, n2, n3, n4]
x = mz.Xhtml("x")
for n in nodes1:
    x.importNode(n)
nodes2 = x.getNodes()
test("Xhtml.getNodes", (nodes1 == nodes2))

y1 = mz.Xhtml("y")
txt1 = "Alright there."
t1 = mz.Xtext(txt1)
x = mz.Xhtml("x")
x.importNode(y1)
x.importNode(t1)
y2 = x.getNode(0)
t2 = x.getNode(1)
if t2 is not None:
    txt2 = t2.getText()
else:
    txt2 = None
test("Xhtml.importNode #001", ((y1, t1, txt1) == (y2, t2, txt2)))

# ToDo: recursive import

tag1a = "y"
tag1b = "z"
txt1a = "A first text"
txt1b = "And another one..."
x = mz.Xhtml("x")
x.addLeaf(tag1a)
x.addText(txt1a)
y1 = mz.Xhtml(tag1b)
t1 = mz.Xtext(txt1b)
x.insertNode(y1)
x.insertNode(t1, 2)
y2b = x.getNode(0)
y2a = x.getNode(1)
t2b = x.getNode(2)
t2a = x.getNode(3)
tag2a = y2a.getTag()
tag2b = y2b.getTag()
txt2a = t2a.getText()
txt2b = t2b.getText()
test("Xhtml.insertNode", ((tag1a, tag1b, txt1a, txt1b, y1, t1) ==
                          (tag2a, tag2b, txt2a, txt2b, y2b, t2b)))

n1 = mz.Xhtml("a")
n2 = mz.Xtext("Hey")
n3 = mz.Xhtml("b")
n4 = mz.Xtext("Alright")
nodes1 = [n1, n2, n3, n4]
x = mz.Xhtml("x")
for n in nodes1:
    x.importNode(n)
test_pass = True
for i in (3, 0, 1, 0):
    n = nodes1[i]
    x.removeNode(n)
    del nodes1[i]
    tmp = list()
    for n in nodes1:
        tmp.append(n) # ToDo: copy?
    nodes1 = tmp
    nodes2 = x.getNodes()
    if nodes1 != nodes2:
        test_pass = False
        break
test("Xhtml.removeNode", test_pass)

n1 = mz.Xhtml("a")
n2 = mz.Xtext("Hey")
n3 = mz.Xhtml("b")
n4 = mz.Xtext("Alright")
nodes1 = [n1, n2, n3, n4]
n = len(nodes1)
x = mz.Xhtml("x")
for node in nodes1:
    x.importNode(node)
m = x.countNodes()
test("Xhtml.countNodes", (n == m))

# Xhtml: equals

def make_node1():
    return mz.Xhtml('a')

def make_node2():
    x = mz.Xhtml('x', "That's the top-level text value")
    x.setAttribute('a', 'b')
    x.addLeaf('y', "OK").setAttribute('c', "d")
    z = x.addLeaf('z', "Alright.").setAttribute('hello', "Bye-bye")
    z.addLeaf('w').setId("something")
    return x

def make_node3():
    x = mz.Xhtml('x')
    for i in range(0, 50):
        y = x.addLeaf('tag-%d' % i)
        for j in range(0, 50):
            y.setAttribute('att-%d' % j, "%d-%d" % (i, j))
    return x

test("Xhtml.equals #001", (make_node1().equals(make_node1())))
test("Xhtml.equals #002", (make_node2().equals(make_node2())))
test("Xhtml.equals #003", (make_node3().equals(make_node3())))

x1 = make_node2()
x2 = make_node2()
x2.setText("And now for something completely different")
test("Xhtml.equals #100", (not x1.equals(x2)))

x1 = make_node2()
x2 = make_node2()
x1.setId("Arthur")
x2.setId("Dent")
test("Xhtml.equals #101", (not x1.equals(x2)))

txt = "Hello"
x1 = mz.Xhtml('x', txt)
x2 = mz.Xhtml('y', txt)
test("Xhtml.equals #102", (not x1.equals(x2)))

# Xhtml: helpers

data_dir = os.path.join(top_dir, 'test', 'data')
xml_path = os.path.join(data_dir, 'xhtml_api.xml')
doc = mz.std_parser.parseFile(xml_path)
tests = doc.getRoot()

x = mz.Xhtml('test').setId("helpers")
x.addP("A paragraph.")
x.addDiv("That's a div.")
x.addSpan().setId("empty-span")
x.addFile(os.path.join(data_dir, 'xhtml_api.txt'))
x.addImage("image.png", "This is not an image.")
x.addLink("http://here.com", "That's a link to here", "No title yet")
x.addLinkParams("http://there.com", [("one", "1"), ("two", "2")], "there",
                "That's the title!")
x.addJscript()
helpers_data = tests.getLeaf('test', 'id', "helpers")
test("Xhtml.add* helpers", x.equals(helpers_data))

# Xlist

x = mz.Xhtml('test').setId('list')
l = mz.Xlist()
x.importNode(l)
x.addList().setId('list-2')
l = x.addList()
l.addPoint('item')
l = x.addList()
for i in range(0, 10):
    l.addPoint("Item #{0}".format(i)).setId("list-id-{0}".format(i))
list_data = tests.getLeaf('test', 'id', 'list')
test("Xlist", x.equals(list_data))

# Xtable

x = mz.Xhtml('test').setId('table')
t = mz.Xtable()
t.setId('table-1')
t.addRow().addCell("A single cell")
x.importNode(t)
t = x.addTable()
r = t.addRow().setId('row-1')
r.addCell("First")
r.addCell("Second").addP("Hello")
r = t.addRow().setId('row-2')
r.addHeader("Header")
for i in range(0, 10):
    r.addCell("Cell #{0}".format(i)).setAttribute('loop', str(i))
table = tests.getLeaf('test', 'id', 'table')
test("Xtable", x.equals(table))

# Xform

x = mz.Xhtml('test').setId('form')
f = mz.Xform("http://nowhere.com", "GET")
x.importNode(f)
f = x.addForm("http://localhost/", "POST")
f.addLeaf('h1', "Form test")
f.addEdit('edit-test', 10, "edit-value")
f.addText('text-test', 12, 12, "Hello\nBye-bye.")
f.addPassword('password-test', 15)
f.addSelect('select-test', (('a', "apricot"), ('b', "banana")), 'b')
f.addCheckBox('checkbox-test-a', "no-check")
f.addCheckBox('checkbox-test-b', "check", True)
f.addHidden('hidden-test', "here it is")
f.addSubmit('submit-test-a')
f.addSubmit('submit-test-b', "submit-name")
f.addSubmitImage("http://localhost/image.png")
f.addUpload('upload-test')
f = x.addForm("http://elsewhere.com", "POST")
f.setInputId(1)
for i in range(0, 10):
    f.addEdit('set-test')
    f.addCheckBox('check-test', 'test-value-{0}'.format(i), bool(i%2))
    f.addLeaf('br')
    f.incInputId()
f.setInputId(0)
f.addSubmit('GO')

form = tests.getLeaf('test', 'id', 'form')
test("Xform", x.equals(form))

sys.exit(0)

# Mangoz - test/python/tree_api.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import sys
import os
top_dir = os.environ['TOP_DIR']
sys.path.append(os.path.join(top_dir, 'python'))
import mangoz as mz

def test(test_id, cond):
    if not cond:
        print("[%-32s] FAILED" % test_id)
        sys.exit(1)
    else:
        print("[%-32s] OK" % test_id)

if len(sys.argv) < 2:
    data_path = os.path.join(top_dir, 'test', 'data')
else:
    data_path = sys.argv[1]

c = mz.Configuration()
c.set('defaultLanguage', 'en')
c.set('baseURL', 'http://mz-python-test/')
c.set('staticDir', data_path)
c.set('pages', 'test-tree.xml')
core = mz.CoreConfig(c)

path = 'b/b2/b2a/'
split = ('b/b2', 'b2a')
origin = 2
doc = mz.tree_parser.parseFile(os.path.join(core.staticDir, core.pages))
level = mz.Xhtml('pages').setAttribute('auto', 'b2b')
node = level.addLeaf('page').setId('b2a')
node.addLeaf('xhtml').addLeaf('p', "This is the test page.")
level.addLeaf('page').setId('b2b')

# Monitor debug
if False:
    import time
    print("Monitor test")
    tree = mz.Tree(c)
    print("Tree OK")
    time.sleep(3)
    print("bailing out now")
    del(tree)
    sys.exit(0)

tree = mz.Tree(c)
test("Tree", tree is not None)
tree.setPath(path)
test("Tree.{get,set}Path", (tree.getPath() == path))
test("Tree.getOrigin", (tree.getOrigin() == origin))
test("Tree.getLevel", level.equals(tree.getLevel()))
test("Tree.getNode", node.equals(tree.getNode()))
tree.setPath('b/b2/', False)
test("Tree.getNodeSubLevel", level.equals(tree.getNodeSubLevel()))
test("Tree.getDocument", doc.getRoot().equals(tree.getDocument().getRoot()))
test("Tree.getXmlFile", (tree.getXmlFile() == core.pages))
test("Tree.splitPath", (split == mz.Tree.splitPath(path)))

sys.exit(0)

# Mangoz - test/python/configuration_api.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import sys
import os
sys.path.append(os.path.join(os.environ['TOP_DIR'], 'python'))
import mangoz as mz

def test(test_id, cond):
    if not cond:
        print("[%-32s] FAILED" % test_id)
        sys.exit(1)
    else:
        print("[%-32s] OK" % test_id)

# Configuration

c = mz.Configuration()
nope = c.get('alien')
test("Configuration", (nope is None))

key = 'unused'
value1 = 'bingo'
c.set(key, value1)
value2 = c.get(key)
test("Configuration.set", (value1 == value2))

key = 'anotherOne'
value1 = 'yes'
c.set(key, value1)
value2 = c.get(key)
test("Configuration.get", (value1 == value2))

# CoreConfig

c = mz.Configuration()

defaultLanguage = 'de'
c.set('defaultLanguage', defaultLanguage)
baseURL = 'http://localhost/mz-test-python/'
c.set('baseURL', baseURL)
staticDir = 'static/'
c.set('staticDir', staticDir)

core = mz.CoreConfig(c)
test("CoreConfig", (core is not None))
test("CoreConfig.dtd (default)", (core.dtd == mz.DTD_XHTML11))
test("CoreConfig.defaultLanguage",(core.defaultLanguage == defaultLanguage))
test("CoreConfig.baseURL", (core.baseURL == baseURL))
test("CoreConfig.staticDir", (core.staticDir == staticDir))
test("CoreConfig.pages (default)", (core.pages == "pages.xml"))
test("CoreConfig.textbox (None)", (core.textbox is None))
test("CoreConfig.useSession (default)", (core.useSession is True))

dtd = 'strict'
dtd_id = mz.DTD_STRICT;
c.set('dtd', dtd)
pages = 'test-pages.xml'
c.set('pages', pages)
useSession = 'false'
do_use_session = False
c.set('useSession', useSession)
textbox = 'textbox.xml'
c.set('textbox', textbox)
core = mz.CoreConfig(c)
test("CoreConfig.dtd", (core.dtd == dtd_id))
test("CoreConfig.pages", (core.pages == pages))
test("CoreConfig.useSesion", (core.useSession == do_use_session))
test("CoreConfig.textbox", (core.textbox == textbox))

# PageConfig

c.set('baseURL')

page = mz.PageConfig(c)
test("PageConfig (default)", (page is not None))
test("PageConfig.defaultLanguage", (page.defaultLanguage == defaultLanguage))
test("PageConfig.charset (default)", (page.charset == 'UTF-8'))
test("PageConfig.defaultIcon (None)", (page.defaultIcon is None))
test("PageConfig.baseURL (None)", (page.baseURL is None))
test("PageConfig.publicURL (None)", (page.publicURL is None))
test("PageConfig.contentType (default)", (page.contentType == 'text/html'))
test("PageConfig.defaultCSS (empty)", (not page.defaultCSS))

charset = 'ISO-8859-1'
c.set('charset', charset)
defaultIcon = 'test.ico'
c.set('defaultIcon', defaultIcon)
baseURL = 'http://localhost/mz-test-python-again/'
c.set('baseURL', baseURL)
publicURL = '/mz-test-python-public/'
c.set('publicURL', publicURL)
defaultCSS = 'mangoz.css something.css page.css'
css_list = defaultCSS.split(' ')
c.set('defaultCSS', defaultCSS)

page = mz.PageConfig(c)

test("PageConfig", (page is not None))
test("PageConfig.charset", (page.charset == charset))
test("PageConfig.defaultIcon", (page.defaultIcon == defaultIcon))
test("PageConfig.baseURL", (page.baseURL == baseURL))
test("PageConfig.publicURL", (page.publicURL == publicURL))
test("PageConfig.defaultCSS", (page.defaultCSS == css_list))

sys.exit(0)

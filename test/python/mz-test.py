# Mangoz - test/python/mz-test.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import sys
import os
sys.path.append(os.path.join(os.environ['TOP_DIR'], 'python'))
import mangoz as mz

print("--- Python test ---")
print("Mangoz version: 0x%08X" % mz.get_version())

def do_print(x):
    x.produce(mz.StdPrinter())
    print("\n---")

t = mz.Xtext("Hello!")
do_print(t)

t.setText("Alright.")
do_print(t)

print("text: [%s]" % t.getText())

t.setText("First line,\nsecond line\n... and third line.")
do_print(t)

for i, line in enumerate(t.getSplitText()):
    print("line {%d}: {%s}" % (i, line))

x = mz.Xhtml('a')
print("That's an Xhtml: {%s}" % x)
do_print(x)

sp = mz.StringPrinter()
t.produce(sp)
print("string output: [%s]" % sp.getString())

sys.exit(0)

# ToDo: clean-up

print("Tag: {0} or {1}".format(x.tag, x.getTag()))

try:
    x.tag = 'b'
    print("BUG: tag can be written to!")
    sys.exit(1)
except AttributeError, e:
    print("Alright, tag is read-only.")

y = mz.Xhtml('p', "Blablablablah")
z = y.addLeaf('div')
y.addLeaf('h2', "small title")

z.importLeaf(mz.Xhtml('h1', "Alright!"))
z.addText("Cool.")
y.produce(TestPrinter())

sys.exit(0)

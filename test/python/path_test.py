# Mangoz - test/python/path_test.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

from __future__ import print_function
import sys
import os
top_dir = os.environ['TOP_DIR']
sys.path.append(os.path.join(top_dir, 'python'))
import mangoz as mz

if len(sys.argv) < 2:
    data_path = os.path.join(top_dir, 'test', 'data')
else:
    data_path = sys.argv[1]

c = mz.Configuration()
c.set('defaultLanguage', "en")
c.set('baseURL', "/")
c.set('staticDir', data_path)
c.set('publicURL', "/")
c.set('pages', "test-tree.xml")

tree = mz.Tree(c)

tests = [("/", False, None),
         ("/", True, "a"),
         ("/a", False, "a"),
         ("/a", True, "a"),
         ("/a/x", False, None),
         ("/a/x", True, None),
         ("/z", False, None),
         ("/z", True, None),
         ("/b", False, "b"),
         ("/b/z", False, None),
         ("/b/z", True, None),
         ("/b/b1", False, "b1"),
         ("/b/b1", True, "b1"),
         ("/b/b2", False, "b2"),
         ("/b/b2", True, "b2b"),
         ("/b/b2/b2a", False, "b2a"),
         ("/b/b2/b2a", True, "b2a"),
         ("/b/b2/b2b", False, "b2b"),
         ("/b/b2/b2b", True, "b2b"),
         ("/c", False, "c"),
         ("/c", True, "c1"),
         ("/c/c1", False, "c1"),
         ("/c/c1", True, "c1"),
         ("/d", False, "d"),
         ("/d", True, "d1a")]

test_pass = True

for test in tests:
    path = test[0]
    auto = test[1]
    page = test[2]

    if auto:
        auto_str = "yes"
    else:
        auto_str = "no"

    if page is None:
        page_id = "-"
    else:
        page_id = page

    print("path: {0:12s} auto: {1:4s} page: {2:3s} ".format(
            path, auto_str, page_id), end='')

    try:
        tree.setPath(path, auto)
        print(" {0:12s} ".format(tree.getPath()), end='')
        node = tree.getNode()
        if not node:
            if not page:
                print("OK page not found")
            else:
                print("ERROR! node is None")
                test_pass = False
        elif not page:
            print("ERROR! unexpected page returned ({0})".format(node_id))
            test_pass = False
        else:
            node_id = node.getId()

            if not node_id:
                print("ERROR! node id is None")
                test_pass = False
            elif node_id != page:
                print("ERROR! wrong node id ({0})".format(node_id))
                test_pass = False
            else:
                print("OK same page id ({0})".format(node_id))
    except Exception, e:
        print(" {0:12s} ".format(tree.getPath()), end='')

        if page is None:
            print("OK {0}".format(e))
        else:
            print("ERROR! exception: {0}".format(e))
            test_pass = False

if test_pass:
    sys.exit(0)
else:
    sys.exit(1)

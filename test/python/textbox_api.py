# Mangoz - test/python/textbox_api.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import sys
import os
top_dir = os.environ['TOP_DIR']
sys.path.append(os.path.join(top_dir, 'python'))
import mangoz as mz

def test(test_id, cond):
    if not cond:
        print("[%-32s] FAILED" % test_id)
        sys.exit(1)
    else:
        print("[%-32s] OK" % test_id)

if len(sys.argv) < 2:
    data_path = os.path.join(top_dir, 'test', 'data')
else:
    data_path = sys.argv[1]

xml_file = 'textbox_api.xml'

c = mz.Configuration()
c.set('defaultLanguage', 'en')
c.set('baseURL', 'http://mz-python-test/')
c.set('staticDir', data_path)
c.set('textbox', xml_file)

tb = mz.TextBox(c)
test("TextBox", tb is not None)

first_en = tb.get('a', 'en')
test("TextBox.get (en)", (first_en == "In English"))

first_de = tb.get('a', 'de')
test("TextBox.get (de)", (first_de == "Auf Deutsch"))

not_found = tb.get('b', 'en')
test("TextBox.get (not found)", (not_found == 'b'))

xml_file2 = tb.getXmlFile()
test("TextBox.getXmlFile", (xml_file2 == xml_file))

xdoc = mz.tb_parser.parseFile(os.path.join(data_path, xml_file))
tb_xdoc = tb.getDocument()
test("TextBox.getDocument", (xdoc.getRoot().equals(tb_xdoc.getRoot())))

box = mz.Xhtml('box').setId('a').setAttribute('description', "First test item")
box.addLeaf('text', first_en, False).setAttribute('lang', 'en')
box.addLeaf('text', first_de, False).setAttribute('lang', 'de')
box2 = tb.getBox('a')
test("TextBox.getBox", box.equals(box2))

boxes2 = tb.getBoxes()
test("TextBox.getBoxes", ((len(boxes2) == 1) and box.equals(boxes2[0])))

desc = "Second item"
box = mz.Xhtml('box').setId('b').setAttribute('description', desc)
tb.setDescription('b', desc)
box2 = tb.getBox('b')
test("TextBox.setDescription", box2 is not None and box.equals(box2))

scd_en1 = "Still in English"
scd_fr1 = "En Francais"
tb.set('b', 'en', scd_en1)
tb.set('b', 'fr', scd_fr1)
scd_en2 = tb.get('b', 'en')
scd_fr2 = tb.get('b', 'fr')
test("TextBox.set", ((scd_en1 == scd_en2) and (scd_fr1 == scd_fr2)))

tb.clear('b')
not_found = tb.get('b', 'en')
box = tb.getBox('b')
test("TextBox.clear", (not_found == 'b') and box is None)

sys.exit(0)

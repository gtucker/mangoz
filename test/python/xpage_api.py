# Mangoz - test/python/xpage_api.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import sys
import os
top_dir = os.environ['TOP_DIR']
sys.path.append(os.path.join(top_dir, 'python'))
import mangoz as mz

if len(sys.argv) < 2:
    data_dir = os.path.join(top_dir, 'test', 'data')
else:
    data_dir = sys.argv[1]

xdoc = mz.std_parser.parseFile(os.path.join(data_dir, 'xpage_api.xml'))
tests = xdoc.getRoot()

def test(test_id, cond):
    if not cond:
        print("[%-32s] FAILED" % test_id)
        return False
    else:
        print("[%-32s] OK" % test_id)
        return True

def test_xml(test_id, p):
    t = tests.getLeaf('test', 'id', test_id)
    x = mz.Xhtml('test').setId(test_id)
    x.importNode(p)
    if not test(' '.join(['Xpage', test_id]), t.equals(x)):
        t.produce(mz.StdPrinter())
        x.produce(mz.StdPrinter())
        sys.exit(1)

c = mz.Configuration()
c.set('defaultLanguage', 'en')
p = mz.Xpage(c)
test_xml('minimal', p)

c = mz.Configuration()
c.set('defaultLanguage', "de")
c.set('charset', "ISO-8859-1")
c.set('defaultIcon', "something.ico")
c.set('baseURL', "http://localhost/root/")
c.set('publicURL', "http://localhost/public/")
c.set('defaultCSS', "first.css second.css")
c.set('contentType', "application/xml+html")
p = mz.Xpage(c)
test_xml('full-config', p)

c = mz.Configuration()
c.set('defaultLanguage', 'en')
p = mz.Xpage(c)
meta = mz.Xhtml('meta').setAttribute('purpose', 'test-only')
p.importHeadNode(meta)
p.addCss('extra.css')
p.addJscript().setId('simple-test')
p.addExternalJscript("http://localhost/js/whatever.js")
p.addFileJscript(os.path.join(data_dir, 'xpage_api.js'))
p.setTitle('Xpage API')
p.setDescription("Test for the Xpage API")
sp = mz.StringPrinter()
p.produce(sp)
try:
    f = open(os.path.join(data_dir, 'xpage_api.xhtml'))
    ref = f.read()
    f.close()
    if not test('Xpage options', (sp.getString() == ref)):
        print(ref)
        print(sp.getString())
        sys.exit(1)
except IOError, e:
    print(e)
    sys.exit(1)

lang1 = 'fr'
c = mz.Configuration()
c.set('defaultLanguage', lang1)
p = mz.Xpage(c)
lang2 = p.getLangTag()
if not test("Xpage.getLangTag", (lang1 == lang2)):
    print("lang1: {0}, lang2: {1}".format(lang1, lang2))
    sys.exit(1)

c = mz.Configuration()
c.set('defaultLanguage', 'jp')
p = mz.Xpage(c)
body = p.getBody()
body.setId('test-body')
main = body.addDiv("Main part").setId('main')
test_xml('body', p)

sys.exit(0)

/* Mangoz - python/callbacks.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <Python.h>
#include "callbacks.h"

int mangoz_py_printer_produce(void *printer, const char *str, size_t len)
{
	PyObject *arg;
	PyObject *x;
	PyObject *produce_str; /* ToDo: make it persistent in mangoz module */

	arg = PyString_FromStringAndSize(str, len);

	if (arg == NULL)
		return -1;

	produce_str = PyString_FromString("produce");

	if (produce_str == NULL)
		return -1;

	x = PyObject_CallMethodObjArgs(printer, produce_str, arg, NULL);
	Py_DECREF(arg);

	if (x == NULL)
		return -1;

	Py_DECREF(x);

	return 0;
}

const struct mz_callbacks mz_callbacks = {
	.printer_produce = mangoz_py_printer_produce,
	.create_xtext = mangoz_py_create_xtext,
	.create_xhtml = mangoz_py_create_xhtml,
};


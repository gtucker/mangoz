#!/bin/sh

set -e

echo $PYTHON_INCLUDE
[ -n "$PYTHON_INCLUDE" ] && echo "OK"

ver="$1"
[ -z "$ver" ] && {
    echo "no version given"
    exit 1
}

[ -z $TOP_DIR ] && {
    echo "TOP_DIR not set"
    exit 1
}

dir="$TOP_DIR/package/mangoz-python-$ver"
[ -d "$dir" ] && {
    echo "output directory already exists: $dir"
    exit 1
}

echo "output directoy: $dir"
[ -d $TOP_DIR/package ] || mkdir $TOP_DIR/package
mkdir $dir
mkdir $dir/include
mkdir $dir/include/mangoz

echo "adding libmz..."
mkdir $dir/libmz
cp $TOP_DIR/libmz/*.c $dir/libmz/
cp -R $TOP_DIR/include/mangoz/libmz $dir/include/mangoz/

echo "adding _mangoz..."
cp $TOP_DIR/python/*.c $dir/
cp $TOP_DIR/python/*.h $dir/

echo "adding mangoz..."
cp -R $TOP_DIR/python/mangoz $dir/

echo "creating setup.py..."

files=""

cd $dir
for f in `find . -name "*.c"`
do
    if [ -n "$files" ]
    then
        files="$files,'$f'"
    else
        files="'$f'"
    fi
done
cd - 2>&1 > /dev/null

setup=$dir/setup.py

echo "from distutils.core import setup, Extension" > $setup
echo "import os" >> $setup
echo "os.environ['CFLAGS'] += ' -ansi -fno-strict-aliasing'" >> $setup
echo "setup(name='mangoz', version='$ver'," >> $setup
echo "      ext_modules=[Extension('_mangoz', [" >> $setup
echo "$files]," >> $setup
echo "          include_dirs=['./include'," >> $setup
[ -n "$PYTHON_INCLUDE" ] &&
echo "                        '$PYTHON_INCLUDE'," >> $setup
echo "                        '/usr/include/libxml2']," >> $setup
echo "          libraries=['xml2'])]," >> $setup
echo "      packages=['mangoz'])" >> $setup

exit 0

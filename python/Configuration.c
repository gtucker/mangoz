/* Mangoz - python/Configuration.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-python.h"
#include "mangoz-python-util.h"
#include "mangoz-python-private.h"
#include <structmember.h>

#define LOG(msg, ...)				\
	fprintf(stderr, "[Config   %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

#if 0 /* set to 1 to enable memory allocation logging */
# define LOG_ALLOC(msg, ...) LOG(msg, ##__VA_ARGS__)
#else
# define LOG_ALLOC(msg, ...)
#endif

static void Configuration_dealloc(Config *self)
{
	LOG_ALLOC("%p dealloc", self);

	if (self->config.init)
		mz_config_free(&self->config);

	self->ob_type->tp_free(self);
}

static int Configuration_init(Config *self, PyObject *args, PyObject *kw)
{
	if (mz_config_init(&self->config) < 0)
		return -1;

	return 0;
}

static PyObject *Configuration_new(PyTypeObject *type, PyObject *args,
				   PyObject *kw)
{
	Config *self;

	self = (Config *)type->tp_alloc(type, 0);

	if (self == NULL)
		return NULL;

	MZ_CLEAR(self->config);

	LOG_ALLOC("%p new", self);

	return (PyObject *)self;
}

/* - methods - */

static PyObject *ConfigObj_set(Config *self, PyObject *args)
{
	mz_config_invalidate(&self->config);

	if (mz_py_set_attribute(&self->config.opt, args) < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject *ConfigObj_get(Config *self, PyObject *args)
{
	return mz_py_get_attribute(&self->config.opt, args);
}

static PyMethodDef Configuration_methods[] = {
	{ "set", (PyCFunction)ConfigObj_set, METH_VARARGS,
	  "Set a configuration value." },
	{ "get", (PyCFunction)ConfigObj_get, METH_VARARGS,
	  "Get a configuration value." },
	{ NULL }
};

PyTypeObject mangoz_ConfigurationType = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /* ob_size */
	"_mangoz.Configuration",           /* tp_name */
	sizeof(Config),                    /* tp_basicsize */
	0,                                 /* tp_itemsize */
	(destructor)Configuration_dealloc, /* tp_dealloc */
	0,                                 /* tp_print */
	0,                                 /* tp_getattr */
	0,                                 /* tp_setattr */
	0,                                 /* tp_compare */
	0,                                 /* tp_repr */
	0,                                 /* tp_as_number */
	0,                                 /* tp_as_sequence */
	0,                                 /* tp_as_mapping */
	0,                                 /* tp_hash  */
	0,                                 /* tp_call */
	0,                                 /* tp_str */
	0,                                 /* tp_getattro */
	0,                                 /* tp_setattro */
	0,                                 /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT,                /* tp_flags */
	"Mangoz Configuration",            /* tp_doc */
	0,                                 /* tp_traverse */
	0,                                 /* tp_clear */
	0,                                 /* tp_richcompare */
	0,                                 /* tp_weaklistoffset */
	0,                                 /* tp_iter */
	0,                                 /* tp_iternext */
	Configuration_methods,             /* tp_methods */
	0,                                 /* tp_members */
	0,                                 /* tp_getset */
	0,                                 /* tp_base */
	0,                                 /* tp_dict */
	0,                                 /* tp_descr_get */
	0,                                 /* tp_descr_set */
	0,                                 /* tp_dictoffset */
	(initproc)Configuration_init,      /* tp_init */
	0,                                 /* tp_alloc */
	Configuration_new,                 /* tp_new */
};

void mangoz_Configuration_init(PyObject *m)
{
	Py_INCREF((PyObject *)(&mangoz_ConfigurationType));
	PyModule_AddObject(m, "Configuration",
			   (PyObject *)&mangoz_ConfigurationType);
}

/* Mangoz - python/mangoz-python.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-python.h"
#include "mangoz-python-util.h"
#include <mangoz/libmz/libmz.h>

#define MZ_ADD_MODULE_CONST(m, name, type) do {			\
		mangoz_##name = Py_BuildValue(type, MZ_##name);	\
		PyModule_AddObject(m, #name, mangoz_##name);	\
	} while (0)

PyObject *mangoz_Error;

PyObject *mangoz_XNODE_TEXT;
PyObject *mangoz_XNODE_HTML;
PyObject *mangoz_DTD_XHTML11;
PyObject *mangoz_DTD_STRICT;
PyObject *mangoz_DTD_TRANSITIONAL;
PyObject *mangoz_DTD_FRAMESET;
PyObject *mangoz_DTD_HTML;
PyObject *mangoz_QUERY_LANG;
PyObject *mangoz_QUERY_TARGET;
PyObject *mangoz_QUERY_TEMPLATE;
PyObject *mangoz_QUERY_TITLE;
PyObject *mangoz_QUERY_ADD;
PyObject *mangoz_QUERY_MOVE;
PyObject *mangoz_QUERY_INDEX;
PyObject *mangoz_QUERY_BACK;

static void mangoz_set_constants(PyObject *m);

static PyObject *mangoz_get_version(PyObject *self, PyObject *_)
{
	return Py_BuildValue("i", 0x00070000);
}

static PyObject *mangoz_xtext_count(PyObject *self, PyObject *_)
{
	return Py_BuildValue("i", mangoz_Xtext_count);
}

static PyObject *mangoz_xhtml_count(PyObject *self, PyObject *_)
{
	return Py_BuildValue("i", mangoz_Xhtml_count);
}

static PyMethodDef mangoz_methods[] = {
	{ "get_version", mangoz_get_version, METH_NOARGS,
	  "Get the Mangoz version" },
	{ "xtext_count", mangoz_xtext_count, METH_NOARGS,
	  "Get the number of Xtext instances" },
	{ "xhtml_count", mangoz_xhtml_count, METH_NOARGS,
	  "Get the number of Xhtml instances" },
	{ NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC init_mangoz(void)
{
	struct mangoz_class {
		PyTypeObject *type;
		void (*init_func)(PyObject *m);
	};
	static const struct mangoz_class mangoz_classes[] = {
		{ &mangoz_XtextType, mangoz_Xtext_init },
		{ &mangoz_XhtmlType, mangoz_Xhtml_init },
		{ &mangoz_XdocumentType, mangoz_Xdocument_init },
		{ &mangoz_StdPrinterType, mangoz_StdPrinter_init },
		{ &mangoz_StringPrinterType, mangoz_StringPrinter_init },
		{ &mangoz_ConfigurationType, mangoz_Configuration_init },
		{ &mangoz_CoreConfigType, mangoz_CoreConfig_init },
		{ &mangoz_PageConfigType, mangoz_PageConfig_init },
		{ &mangoz_XparserType, mangoz_Xparser_init },
		{ &mangoz_MonitorType, mangoz_Monitor_init },
		{ NULL, NULL }
	};
	const struct mangoz_class *cls;
	PyObject *m;

	for (cls = mangoz_classes; cls->type != NULL; ++cls) {
		if (!cls->type->tp_new)
			cls->type->tp_new = PyType_GenericNew;

		if (PyType_Ready(cls->type) < 0)
			return;
	}

	m = Py_InitModule("_mangoz", mangoz_methods);

	mangoz_Error = PyErr_NewException("_mangoz.error", NULL, NULL);
	Py_INCREF(mangoz_Error);
	PyModule_AddObject(m, "error", mangoz_Error);

	mangoz_set_constants(m);

	for (cls = mangoz_classes; cls->init_func != NULL; ++cls)
		cls->init_func(m);
}

static void mangoz_set_constants(PyObject *m)
{
	MZ_ADD_MODULE_CONST(m, XNODE_TEXT, "i");
	MZ_ADD_MODULE_CONST(m, XNODE_HTML, "i");
	MZ_ADD_MODULE_CONST(m, DTD_XHTML11, "i");
	MZ_ADD_MODULE_CONST(m, DTD_STRICT, "i");
	MZ_ADD_MODULE_CONST(m, DTD_TRANSITIONAL, "i");
	MZ_ADD_MODULE_CONST(m, DTD_FRAMESET, "i");
	MZ_ADD_MODULE_CONST(m, DTD_HTML, "i");
	MZ_ADD_MODULE_CONST(m, QUERY_LANG, "s");
	MZ_ADD_MODULE_CONST(m, QUERY_TARGET, "s");
	MZ_ADD_MODULE_CONST(m, QUERY_TEMPLATE, "s");
	MZ_ADD_MODULE_CONST(m, QUERY_TITLE, "s");
	MZ_ADD_MODULE_CONST(m, QUERY_ADD, "s");
	MZ_ADD_MODULE_CONST(m, QUERY_MOVE, "s");
	MZ_ADD_MODULE_CONST(m, QUERY_INDEX, "s");
	MZ_ADD_MODULE_CONST(m, QUERY_BACK, "s");
}

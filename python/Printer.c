/* Mangoz - python/Printer.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-python.h"
#include "mangoz-python-util.h"
#include <structmember.h>

/* ----------------------------------------------------------------------------
 * StdPrinter
 */

struct StdPrinter_object {
	PyObject_HEAD;
	size_t len;
};
typedef struct StdPrinter_object StdPrinter;

static int StdPrinter_init(PyObject *self, PyObject *args, PyObject *kw)
{
	((StdPrinter *)self)->len = 0;

	return 0;
}

/* - methods - */

static PyObject *StdPrinterObj_clear(StdPrinter *self, PyObject *args)
{
	self->len = 0;
	Py_RETURN_NONE;
}

static PyObject *StdPrinterObj_produce(StdPrinter *self, PyObject *args)
{
	char *text;
	int text_len;

	if (!PyArg_ParseTuple(args, "s#", &text, &text_len))
		return NULL;

	self->len += text_len;
	printf(text);
	Py_RETURN_NONE;
}

static PyMethodDef StdPrinter_methods[] = {
	{ "clear", (PyCFunction)StdPrinterObj_clear, METH_VARARGS,
	  "Clear the output." },
	{ "produce", (PyCFunction)StdPrinterObj_produce, METH_VARARGS,
	  "Produce." },
	{ NULL }
};

PyTypeObject mangoz_StdPrinterType = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /* ob_size */
	"_mangoz.StdPrinter",              /* tp_name */
	sizeof(StdPrinter),                /* tp_basicsize */
	0,                                 /* tp_itemsize */
	0,                                 /* tp_dealloc */
	0,                                 /* tp_print */
	0,                                 /* tp_getattr */
	0,                                 /* tp_setattr */
	0,                                 /* tp_compare */
	0,                                 /* tp_repr */
	0,                                 /* tp_as_number */
	0,                                 /* tp_as_sequence */
	0,                                 /* tp_as_mapping */
	0,                                 /* tp_hash  */
	0,                                 /* tp_call */
	0,                                 /* tp_str */
	0,                                 /* tp_getattro */
	0,                                 /* tp_setattro */
	0,                                 /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT,                /* tp_flags */
	"Mangoz StdPrinter",               /* tp_doc */
	0,                                 /* tp_traverse */
	0,                                 /* tp_clear */
	0,                                 /* tp_richcompare */
	0,                                 /* tp_weaklistoffset */
	0,                                 /* tp_iter */
	0,                                 /* tp_iternext */
	StdPrinter_methods,                /* tp_methods */
	0,                                 /* tp_members */
	0,                                 /* tp_getset */
	0,                                 /* tp_base */
	0,                                 /* tp_dict */
	0,                                 /* tp_descr_get */
	0,                                 /* tp_descr_set */
	0,                                 /* tp_dictoffset */
	StdPrinter_init,                   /* tp_init */
	0,                                 /* tp_alloc */
	0,                                 /* tp_new */
};

void mangoz_StdPrinter_init(PyObject *m)
{
	Py_INCREF((PyObject *)(&mangoz_StdPrinterType));
	PyModule_AddObject(m, "StdPrinter",
			   (PyObject *)&mangoz_StdPrinterType);
}

/* ----------------------------------------------------------------------------
 * SringPrinter
 */

struct StringPrinter_object {
	PyObject_HEAD;
	struct mz_string string;
	mz_bool init;
};
typedef struct StringPrinter_object StringPrinter;

static void StringPrinter_dealloc(StringPrinter *self)
{
	if (self->init) {
		mz_string_free(&self->string);
		self->init = false;
	}
}

static int StringPrinter_init(StringPrinter *self, PyObject *args, PyObject *_)
{
	if (mz_string_init(&self->string, 0) < 0)
		return -1;

	self->init = true;

	return 0;
}

static PyObject *StringPrinter_new(PyTypeObject *type, PyObject *args,
				   PyObject *kw)
{
	StringPrinter *self;

	self = (StringPrinter *)type->tp_alloc(type, 0);

	if (self == NULL)
		return NULL;

	self->init = false;

	return (PyObject *)self;
}

/* - methods - */

static PyObject *StringPrinterObj_clear(StringPrinter *self, PyObject *args)
{
	mz_string_clear(&self->string);
	Py_RETURN_NONE;
}

static PyObject *StringPrinterObj_produce(StringPrinter *self, PyObject *args)
{
	char *text;
	int text_len;

	if (!PyArg_ParseTuple(args, "s#", &text, &text_len))
		return NULL;

	if (mz_string_cat(&self->string, text, text_len) < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject *StringPrinterObj_getString(StringPrinter *self,PyObject *args)
{
	return mz2py_str(&self->string);
}

static PyMethodDef StringPrinter_methods[] = {
	{ "clear", (PyCFunction)StringPrinterObj_clear, METH_VARARGS,
	  "Clear the string." },
	{ "produce", (PyCFunction)StringPrinterObj_produce, METH_VARARGS,
	  "Produce." },
	{ "getString", (PyCFunction)StringPrinterObj_getString, METH_VARARGS,
	  "Get the resulting string." },
	{ NULL }
};

PyTypeObject mangoz_StringPrinterType = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /* ob_size */
	"_mangoz.StringPrinter",           /* tp_name */
	sizeof(StringPrinter),             /* tp_basicsize */
	0,                                 /* tp_itemsize */
	(destructor)StringPrinter_dealloc, /* tp_dealloc */
	0,                                 /* tp_print */
	0,                                 /* tp_getattr */
	0,                                 /* tp_setattr */
	0,                                 /* tp_compare */
	0,                                 /* tp_repr */
	0,                                 /* tp_as_number */
	0,                                 /* tp_as_sequence */
	0,                                 /* tp_as_mapping */
	0,                                 /* tp_hash  */
	0,                                 /* tp_call */
	0,                                 /* tp_str */
	0,                                 /* tp_getattro */
	0,                                 /* tp_setattro */
	0,                                 /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT,                /* tp_flags */
	"Mangoz StringPrinter",            /* tp_doc */
	0,                                 /* tp_traverse */
	0,                                 /* tp_clear */
	0,                                 /* tp_richcompare */
	0,                                 /* tp_weaklistoffset */
	0,                                 /* tp_iter */
	0,                                 /* tp_iternext */
	StringPrinter_methods,             /* tp_methods */
	0,                                 /* tp_members */
	0,                                 /* tp_getset */
	0,                                 /* tp_base */
	0,                                 /* tp_dict */
	0,                                 /* tp_descr_get */
	0,                                 /* tp_descr_set */
	0,                                 /* tp_dictoffset */
	(initproc)StringPrinter_init,      /* tp_init */
	0,                                 /* tp_alloc */
	StringPrinter_new,                 /* tp_new */
};

void mangoz_StringPrinter_init(PyObject *m)
{
	Py_INCREF((PyObject *)(&mangoz_StringPrinterType));
	PyModule_AddObject(m, "StringPrinter",
			   (PyObject *)&mangoz_StringPrinterType);
}

/* ToDo */
/* ----------------------------------------------------------------------------
 * FilePrinter
 */

/* Mangoz - python/mangoz-python-util.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-python-util.h"

PyObject *mz2py_strlist(const struct mz_vector *v)
{
	PyObject *list;
	const struct mz_string *it;
	size_t i;

	list = PyList_New(v->n);

	if (list == NULL)
		return NULL;

	mz_vector_for_each_pn(v, it, i) {
		if (PyList_SetItem(list, i, mz2py_str(it)) < 0)
			return NULL;
	}

	return list;
}

PyObject *mz_py_unistrobj(PyObject *obj, char **str, size_t *len)
{
	PyObject *ret_obj;
	PyObject *tmp_obj = NULL;

	if ((obj == NULL) || (obj == Py_None)) {
		ret_obj = NULL;
	} else if (PyUnicode_Check(obj)) {
		ret_obj = PyUnicode_AsEncodedString(obj, "utf-8", NULL);
	} else if (PyString_Check(obj)) {
		Py_INCREF(obj);
		ret_obj = obj;
#ifdef Py_USING_UNICODE
	} else if ((tmp_obj = PyObject_Unicode(obj)) != NULL) {
		ret_obj = PyUnicode_AsEncodedString(tmp_obj, "utf-8", NULL);
		Py_DECREF(tmp_obj);
#endif
	} else {
		ret_obj = PyObject_Str(obj);
	}

	if (ret_obj != NULL) {
		*str = PyString_AS_STRING(ret_obj);
		*len = PyString_GET_SIZE(ret_obj);
	} else {
		*str = NULL;
		*len = 0;
	}

	return ret_obj;
}

int mz_py_set_attribute(struct mz_vector *atts, PyObject *args)
{
	char *key;
	int key_n;
	PyObject *value_obj = NULL;
	PyObject *str_obj;
	char *value;
	size_t value_n;
	int ret;

	if (!PyArg_ParseTuple(args, "s#|O", &key, &key_n, &value_obj))
		return -1;

	str_obj = mz_py_unistrobj(value_obj, &value, &value_n);
	ret = mz_attribute_set(atts, key, key_n, value, value_n);
	Py_XDECREF(str_obj);

	return ret;
}

PyObject *mz_py_get_attribute(struct mz_vector *atts, PyObject *args)
{
	char *key;
	int key_len;
	const struct mz_string *value;

	if (!PyArg_ParseTuple(args, "s#", &key, &key_len))
		return NULL;

	value = mz_attribute_get(atts, key, key_len);

	if (value == NULL)
		Py_RETURN_NONE;

	return mz2py_str(value);
}

PyObject *mz_py_get_dtd_id(enum mz_dtd dtd)
{
	PyObject *pyid;

	switch (dtd) {
	case MZ_DTD_XHTML11:      pyid = mangoz_DTD_XHTML11;      break;
	case MZ_DTD_STRICT:       pyid = mangoz_DTD_STRICT;       break;
	case MZ_DTD_TRANSITIONAL: pyid = mangoz_DTD_TRANSITIONAL; break;
	case MZ_DTD_FRAMESET:     pyid = mangoz_DTD_FRAMESET;     break;
	case MZ_DTD_HTML:         pyid = mangoz_DTD_HTML;         break;
	default:                  pyid = NULL;                    break;
	}

	if (pyid == NULL)
		return NULL;

	Py_INCREF(pyid);

	return pyid;
}

void mz_py_check_init_threads(void)
{
	if (PyEval_ThreadsInitialized())
		return;

	PyEval_InitThreads();
	PyEval_ReleaseLock();
}

/* Mangoz - python/mangoz-python-util.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MANGOZ_PYTHON_UTIL_H
#define INCLUDE_MANGOZ_PYTHON_UTIL_H 1

#include "mangoz-python.h"
#include <mangoz/libmz/libmz.h>

#define MZ_BASE_TYPE_FLAGS (Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE)

/* libmz to Python converters */
#define mz2py_str(_s)						\
	(((_s) == NULL) ? NULL :				\
	 PyString_FromStringAndSize((_s)->str, (_s)->len))
#define mz2py_unicode(_s)					\
	(((_s) == NULL) ? NULL :				\
	 PyUnicode_FromStringAndSize((_s)->str, (_s)->len))
#define mz2py_bool(_b) ({						\
			PyObject *_bo = (_b) ? Py_True : Py_False;	\
			Py_INCREF(_bo);					\
			_bo;						\
		})
extern PyObject *mz2py_strlist(const struct mz_vector *v);

/* Python to libmz converters */
#define py2mz_true(_o) (((_o) == Py_False) ? false : true)
#define py2mz_false(_o) (((_o) == Py_True) ? true : false)

/* attributes helpers */
extern int mz_py_set_attribute(struct mz_vector *atts, PyObject *args);
extern PyObject *mz_py_get_attribute(struct mz_vector *atts, PyObject *args);
extern PyObject *mz_py_get_dtd_id(enum mz_dtd dtd);

/* Unicode */
extern PyObject *mz_py_unistrobj(PyObject *unistr, char **str, size_t *len);

/* misc */
extern void mz_py_check_init_threads(void);

#endif /* INCLUDE_MANGOZ_PYTHON_UTIL_H */

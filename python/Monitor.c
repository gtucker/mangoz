/* Mangoz - python/Monitor.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

/* Note: This may just be an interim solution until the Selector and Tree are
 * implemented in libmz, so it's not part of the API v1 */

#include "mangoz-python.h"
#include "mangoz-python-util.h"
#include "mangoz-python-private.h"
#include <pthread.h>

#define LOG(msg, ...)							\
	fprintf(stderr, "[Monitor %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

struct Monitor_object {
	PyObject_HEAD;
	struct mz_monitor *m;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	mz_bool running;
};
typedef struct Monitor_object Monitor;

static int monitor_cb(const struct mz_monitor_event *ev);
static int call_event(Monitor *self, const struct mz_monitor_event *ev);

static void Monitor_dealloc(Monitor *self)
{
	if (self->m != NULL)
		mz_monitor_free(self->m);

	pthread_mutex_destroy(&self->mutex);
	pthread_cond_destroy(&self->cond);
	self->ob_type->tp_free(self);
}

static PyObject *Monitor_new(PyTypeObject *type, PyObject *args, PyObject *kw)
{
	Monitor *self;

	self = (Monitor *)type->tp_alloc(type, 0);

	if (self == NULL)
		return NULL;

	self->m = NULL;

	return (PyObject *)self;
}

static int Monitor_init(Monitor *self, PyObject *args, PyObject *kw)
{
	mz_py_check_init_threads();

	if (pthread_mutex_init(&self->mutex, NULL) < 0)
		return -1;

	if (pthread_cond_init(&self->cond, NULL) < 0)
		return -1;

	if (mz_monitor_init(&self->m, monitor_cb, self) < 0)
		return -1;

	self->running = false;

	return 0;
}

/* - methods - */

PyObject *MonitorObj_addFile(Monitor *self, PyObject *args)
{
	char *file;
	int nfile;
	PyObject *ctx = NULL;
	mz_bool none_decref = false;
	int stat;

	if (!PyArg_ParseTuple(args, "s#|O", &file, &nfile, &ctx))
		return NULL;

	if (ctx == NULL) {
		Py_INCREF(Py_None);
		ctx = Py_None;
		none_decref = true;
	}

	stat = mz_monitor_add_file(self->m, file, nfile, ctx);

	if (none_decref)
		Py_DECREF(Py_None);

	if (stat < 0)
		return NULL;

	Py_RETURN_NONE;
}

PyObject *MonitorObj_removeFile(Monitor *self, PyObject *args)
{
	char *file;
	int nfile;

	if (!PyArg_ParseTuple(args, "s#", &file, &nfile))
		return NULL;

	if (mz_monitor_remove_file(self->m, file, nfile) < 0)
		return NULL;

	Py_RETURN_NONE;
}

PyObject *MonitorObj_run(Monitor *self)
{
	if (mz_monitor_run(self->m) < 0) {
		LOG("Oops");
		return NULL;
	}

	pthread_mutex_lock(&self->mutex);
	while (!self->running)
		pthread_cond_wait(&self->cond, &self->mutex);
	pthread_mutex_unlock(&self->mutex);

	Py_RETURN_NONE;
}

PyObject *MonitorObj_cancel(Monitor *self)
{
	if (mz_monitor_cancel(self->m) < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyMethodDef Monitor_methods[] = {
	{ "addFile", (PyCFunction)MonitorObj_addFile, METH_VARARGS,
	  "Add a file to the monitored list" },
	{ "removeFile", (PyCFunction)MonitorObj_removeFile, METH_VARARGS,
	  "Remove a file from the monitored list" },
	{ "run", (PyCFunction)MonitorObj_run, METH_NOARGS,
	  "Run the monitoring thread" },
	{ "cancel", (PyCFunction)MonitorObj_cancel, METH_NOARGS,
	  "Cancel the monitoring thread" },
	{ NULL }
};

PyTypeObject mangoz_MonitorType = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /* ob_size */
	"_mangoz.Monitor",                 /* tp_name */
	sizeof(Monitor),                   /* tp_basicsize */
	0,                                 /* tp_itemsize */
	(destructor)Monitor_dealloc,       /* tp_dealloc */
	0,                                 /* tp_print */
	0,                                 /* tp_getattr */
	0,                                 /* tp_setattr */
	0,                                 /* tp_compare */
	0,                                 /* tp_repr */
	0,                                 /* tp_as_number */
	0,                                 /* tp_as_sequence */
	0,                                 /* tp_as_mapping */
	0,                                 /* tp_hash  */
	0,                                 /* tp_call */
	0,                                 /* tp_str */
	0,                                 /* tp_getattro */
	0,                                 /* tp_setattro */
	0,                                 /* tp_as_buffer */
	MZ_BASE_TYPE_FLAGS,                /* tp_flags */
	"Mangoz File Monitor",             /* tp_doc */
	0,                                 /* tp_traverse */
	0,                                 /* tp_clear */
	0,                                 /* tp_richcompare */
	0,                                 /* tp_weaklistoffset */
	0,                                 /* tp_iter */
	0,                                 /* tp_iternext */
	Monitor_methods,                   /* tp_methods */
	0,                                 /* tp_members */
	0,                                 /* tp_getset */
	0,                                 /* tp_base */
	0,                                 /* tp_dict */
	0,                                 /* tp_descr_get */
	0,                                 /* tp_descr_set */
	0,                                 /* tp_dictoffset */
	(initproc)Monitor_init,            /* tp_init */
	0,                                 /* tp_alloc */
	Monitor_new,                       /* tp_new */
};

void mangoz_Monitor_init(PyObject *m)
{
	Py_INCREF((PyObject *)(&mangoz_MonitorType));
	PyModule_AddObject(m, "Monitor", (PyObject *)&mangoz_MonitorType);
}

/* ----------------------------------------------------------------------------
 * private functions
 */

static int monitor_cb(const struct mz_monitor_event *ev)
{
	Monitor *self = ev->ctx;
	int stat = 0;

	switch (ev->msg) {
	case MZ_MON_RUNNING:
		pthread_mutex_lock(&self->mutex);
		self->running = true;
		pthread_cond_signal(&self->cond);
		pthread_mutex_unlock(&self->mutex);
		break;
	case MZ_MON_STOPPED:
		pthread_mutex_lock(&self->mutex);
		self->running = false;
		pthread_cond_signal(&self->cond);
		pthread_mutex_unlock(&self->mutex);
		break;
	case MZ_MON_ERROR:
		assert(!"ERROR!"); /* ToDo: raise exception */
		break;
	case MZ_MON_FILE: {
		PyGILState_STATE gil;
		pthread_mutex_lock(&self->mutex);
		gil = PyGILState_Ensure();
		stat = call_event(self, ev);
		PyGILState_Release(gil);
		pthread_mutex_unlock(&self->mutex);
		break;
	}
	default:
		assert(!"invalid message");
		stat = -1;
		break;
	}

	return stat;
}

static int call_event(Monitor *self, const struct mz_monitor_event *ev)
{
	PyObject *file_name;
	PyObject *event_str;
	PyObject *ret;

	file_name = mz2py_str(ev->file_name);

	if (file_name == NULL) {
		LOG("Oops");
		return -1;
	}

	event_str = PyString_FromString("event");

	if (event_str == NULL) {
		LOG("Oops");
		return -1;
	}

	ret = PyObject_CallMethodObjArgs((PyObject *)self, event_str,
					 file_name, ev->file_ctx, NULL);

	Py_DECREF(file_name);
	Py_DECREF(event_str);

	if (ret == NULL) {
		LOG("Oops");
		return -1;
	}

	Py_DECREF(ret);

	return 0;
}

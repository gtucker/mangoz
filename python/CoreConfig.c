/* Mangoz - python/CoreConfig.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-python.h"
#include "mangoz-python-util.h"
#include "mangoz-python-private.h"
#include <structmember.h>

#define LOG(msg, ...)				\
	fprintf(stderr, "[CoreConf %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

#if 0 /* set to 1 to enable memory allocation logging */
# define LOG_ALLOC(msg, ...) LOG(msg, ##__VA_ARGS__)
#else
# define LOG_ALLOC(msg, ...)
#endif

struct CoreConfig_object {
	PyObject_HEAD;
	PyObject *dtd;
	PyObject *defaultLanguage;
	PyObject *staticDir;
	PyObject *baseURL;
	PyObject *pages;
	PyObject *textbox;
	PyObject *useSession;
};
typedef struct CoreConfig_object CoreConfig;

static void CoreConfig_dealloc(CoreConfig *self)
{
	LOG_ALLOC("%p core dealloc", self);

	Py_XDECREF(self->dtd);
	Py_XDECREF(self->defaultLanguage);
	Py_XDECREF(self->staticDir);
	Py_XDECREF(self->baseURL);
	Py_XDECREF(self->pages);
	Py_XDECREF(self->textbox);
	Py_XDECREF(self->useSession);
}

static int CoreConfig_init(CoreConfig *self, PyObject *args, PyObject *kw)
{
	Config *config;
	const struct mz_core_config *core;

	if (!PyArg_ParseTuple(args, "O!", &mangoz_ConfigurationType, &config))
		return -1;

	LOG_ALLOC("%p init %p", self, config);

	core = mz_config_get_core(&config->config);

	if (core == NULL) {
		LOG("Oops");
		return -1;
	}

	self->dtd = mz_py_get_dtd_id(core->dtd_id);
	self->defaultLanguage = mz2py_str(core->defaultLanguage);
	self->staticDir = mz2py_str(core->staticDir);
	self->baseURL = mz2py_str(core->baseURL);
	self->pages = mz2py_str(core->pages);
	self->textbox = mz2py_str(core->textbox);
	self->useSession = mz2py_bool(core->do_use_session);

	return 0;
}

static PyMemberDef CoreConfig_members[] = {
	{ "dtd", T_OBJECT, offsetof(CoreConfig, dtd), READONLY, "dtd" },
	{ "defaultLanguage", T_OBJECT, offsetof(CoreConfig, defaultLanguage),
	  READONLY, "defaultLanguage" },
	{ "staticDir", T_OBJECT, offsetof(CoreConfig, staticDir),
	  READONLY, "staticDir" },
	{ "baseURL", T_OBJECT, offsetof(CoreConfig, baseURL),
	  READONLY, "baseURL" },
	{ "pages", T_OBJECT, offsetof(CoreConfig, pages),
	  READONLY, "pages" },
	{ "textbox", T_OBJECT, offsetof(CoreConfig, textbox),
	  READONLY, "textbox" },
	{ "useSession", T_OBJECT, offsetof(CoreConfig, useSession),
	  READONLY, "useSession" },
	{ NULL }
};

PyTypeObject mangoz_CoreConfigType = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /* ob_size */
	"_mangoz.CoreConfig",              /* tp_name */
	sizeof(Config),                    /* tp_basicsize */
	0,                                 /* tp_itemsize */
	(destructor)CoreConfig_dealloc,    /* tp_dealloc */
	0,                                 /* tp_print */
	0,                                 /* tp_getattr */
	0,                                 /* tp_setattr */
	0,                                 /* tp_compare */
	0,                                 /* tp_repr */
	0,                                 /* tp_as_number */
	0,                                 /* tp_as_sequence */
	0,                                 /* tp_as_mapping */
	0,                                 /* tp_hash  */
	0,                                 /* tp_call */
	0,                                 /* tp_str */
	0,                                 /* tp_getattro */
	0,                                 /* tp_setattro */
	0,                                 /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT,                /* tp_flags */
	"Mangoz CoreConfig",               /* tp_doc */
	0,                                 /* tp_traverse */
	0,                                 /* tp_clear */
	0,                                 /* tp_richcompare */
	0,                                 /* tp_weaklistoffset */
	0,                                 /* tp_iter */
	0,                                 /* tp_iternext */
	0,                                 /* tp_methods */
	CoreConfig_members,                /* tp_members */
	0,                                 /* tp_getset */
	0,                                 /* tp_base */
	0,                                 /* tp_dict */
	0,                                 /* tp_descr_get */
	0,                                 /* tp_descr_set */
	0,                                 /* tp_dictoffset */
	(initproc)CoreConfig_init,         /* tp_init */
	0,                                 /* tp_alloc */
	0,                                 /* tp_new */
};

void mangoz_CoreConfig_init(PyObject *m)
{
	Py_INCREF((PyObject *)(&mangoz_CoreConfigType));
	PyModule_AddObject(m, "CoreConfig",
			   (PyObject *)&mangoz_CoreConfigType);
}

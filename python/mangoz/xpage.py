# Mangoz - python/mangoz/xpage.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import _mangoz as mz

class Xpage(mz.Xhtml):
    def __init__(self, conf, css=None, lang=None, icon=None):
        super(Xpage, self).__init__('html')
        c = mz.PageConfig(conf)
        self.setAttribute('xmlns', 'http://www.w3.org/1999/xhtml')

        if css is None:
            css = c.defaultCSS
        if lang is None:
            lang = c.defaultLanguage
        if icon is None:
            icon = c.defaultIcon

        self._lang = lang
        self._head = self.addLeaf('head')
        self._body = self.addLeaf('body')
        self._title = None
        self._desc = None

        self._head.addLeaf('meta').\
            setAttribute('http-equiv', 'Content-Type').\
            setAttribute('content',
                         '{0};charset={1}'.format(c.contentType, c.charset))

        if self._lang != 'en-US':
            self._head.addLeaf('meta').\
                setAttribute('http-equiv', 'Content-Language').\
                setAttribute('content', self._lang)

        if c.baseURL:
            self._head.addLeaf('base').setAttribute('href', c.baseURL)

        if icon:
            self._head.addLeaf('link').\
                setAttribute('href', icon).\
                setAttribute('rel', 'shortcut icon').\
                setAttribute('type', 'image/x-icon')

        if css:
            if isinstance(css, str):
                css = [css]
            for it in css:
                self.addCss(''.join([c.publicURL, it]))

    def importHeadNode(self, node):
        return self._head.importNode(node)

    def addCss(self, css):
        return self._head.addLeaf('link').\
            setAttribute('href', css).\
            setAttribute('rel', 'stylesheet').\
            setAttribute('type', 'text/css')

    def addJscript(self):
        return self._head.addJscript()

    def addExternalJscript(self, url):
        return self.addJscript().setAttribute('src', url)

    def addFileJscript(self, fileName):
        j = self.addJscript()
        j.addFile(fileName)
        return j

    def setTitle(self, title):
        if self._title is None:
            self._title = self._head.addLeaf('title')
        self._title.setText(title)
        return self

    def setDescription(self, description):
        if self._desc is None:
            self._desc = self._head.addLeaf('meta').\
                setAttribute('name', 'description')
        self._desc.setAttribute('content', description)
        return self

    def getLangTag(self):
        return self._lang

    @property
    def lang_tag(self):
        return self._lang

    def getBody(self):
        return self._body

    @property
    def body(self):
        return self._body

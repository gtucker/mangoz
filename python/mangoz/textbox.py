# Mangoz - python/mangoz/textbox.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import os
import threading
import _mangoz as mz

class TextBox(object):
    class Monitor(mz.Monitor):
        def __init__(self, mon, *args, **kwargs):
            super(TextBox.Monitor, self).__init__(*args, **kwargs)
            self._mon = mon

        def event(self, full_path, *args):
            self._mon.reload(full_path)

    def __init__(self, c):
        self._c = mz.CoreConfig(c)
        self._lock = threading.Lock()
        file_path = os.path.join(self._c.staticDir, self._c.textbox)
        self.reload(file_path)
        self._monitor = TextBox.Monitor(self)
        self._monitor.addFile(file_path)
        self._monitor.run()

    def lock(self):
        self._lock.acquire()

    def unlock(self):
        self._lock.release()

    def reload(self, full_path):
        self.lock()
        self._doc = mz.tb_parser.parseFile(full_path)
        self._root = self._doc.getRoot()
        self.unlock()

    def setDescription(self, boxId, description):
        box = self._get_box(boxId)
        if not box:
            box = self._root.addLeaf('box').setId(boxId)
        box.setAttribute('description', description)

    def set(self, boxId, lang, value):
        box = self._get_box(boxId)
        if not box:
            box = self._root.addLeaf('box').setId(boxId)
        text = TextBox._get_text(box, lang)
        if text is None:
            text = box.addLeaf('text', value, False).setAttribute('lang', lang)
        else:
            text.setText(value)
        return text

    def get(self, boxId, lang, nl2brParagraph=True):
        box = self._get_box(boxId)
        if not box:
            return boxId # ToDo: return None and add getDefault method

        text = TextBox._get_text(box, lang)
        if text is None:
            text = TextBox._get_text(box, self._c.defaultLanguage)
        if text is None:
            return boxId

        text_str = text.getText().strip()
        if nl2brParagraph:
            mode = box.getAttribute('mode')
            if mode and (mode == 'paragraph'):
                text_str = text_str.replace('\n', '<br />\n')

        return text_str

    def getBox(self, boxId):
        return self._get_box(boxId)

    def clear(self, boxId, lang=None):
        box = self._get_box(boxId)
        if not box:
            return

        if lang is not None:
            text = TextBox._get_text(box. lang)
            if text:
                box.removeNode(text)
        else:
            self._root.removeNode(box)

    def getDocument(self):
        return self._doc

    def getXmlFile(self):
        return self._c.textbox

    def getBoxes(self):
        return self._root.getLeaves('box')

    def _get_box(self, boxId):
        return self._root.getLeaf('box', 'id', boxId)

    @staticmethod
    def _get_text(box, lang):
        return box.getLeaf('text', 'lang', lang)

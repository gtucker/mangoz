# Mangoz - python/mangoz/xhtml.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import os
from urllib import urlencode
import _mangoz as mz

def fixup_api():
    fixup = {'addImage': addImage,
             'addLink': addLink,
             'addLinkParams': addLinkParams,
             'addJscript': addJscript,
             'addList': addList,
             'addTable': addTable,
             'addForm': addForm,
             'makeURLparams': makeURLparams,
             'id': nodeId,
             }
    mz.Xhtml.fixup_api(fixup)

def addImage(self, src, alt=None):
    if not alt:
        alt = src
    return self.addLeaf('img') \
        .setAttribute('src', src) \
        .setAttribute('alt', alt)

def addLink(self, href, text=None, title=None, do_format=True, do_short=False):
    link = self.addLeaf('a', text, do_format, do_short).\
        setAttribute('href', href)
    if title:
        link.setAttribute('title', title)
    return link

def addLinkParams(self, href, params, text=None, title=None,
                  do_format=True, do_short=False):
    return self.addLink(mz.Xhtml.makeURLparams(href, params), text, title,
                        do_format, do_short)

def addJscript(self):
    return self.addLeaf('script', None, True, False).\
        setAttribute('type', 'text/javascript')

def addList(self):
    return self.importNode(Xlist())

def addTable(self):
    return self.importNode(Xtable())

def addForm(self, action, method, charset='UTF-8'):
    return self.importNode(Xform(action, method, charset))

@staticmethod
def makeURLparams(href, params):
    return '?'.join([href, urlencode(params)])

@property
def nodeId(self):
    return self.getId()


class Xlist(mz.Xhtml):
    def __init__(self):
        super(Xlist, self).__init__('ul')

    def addPoint(self, text=None, doFormat=None):
        return self.addLeaf('li', text, doFormat)


class Xtable(mz.Xhtml):
    def __init__(self):
        super(Xtable, self).__init__('table')

    def addRow(self):
        return self.importNode(XtableRow())


class XtableRow(mz.Xhtml):
    def __init__(self):
        super(XtableRow, self).__init__('tr')

    def addCell(self, text=None, doFormat=None):
        return self.addLeaf('td', text, doFormat)

    def addHeader(self, text=None, doFormat=None):
        return self.addLeaf('th', text, doFormat)


class Xform(mz.Xhtml):
    def __init__(self, action, method, charset='UTF-8', doFormat=True):
        super(Xform, self).__init__('form', None, doFormat)
        self._id = 0
        self._auto_import = True
        # Warning: this may get out of sync with the mz_xhtml equivalent
        self._do_format = doFormat
        self.setAttribute('method', method)
        self.setAttribute('action', action)
        self.setAttribute('accept-charset', charset)
        self.setAttribute('accept', 'text/plain')

    def setInputId(self, inputId):
        self._id = inputId
        return self

    def getInputId(self):
        return self._id

    def incInputId(self):
        self._id += 1

    def setAutoImport(self, autoImport):
        self._auto_import = autoImport
        return self

    def getAutoImport(self):
        return self._auto_import

    def addEdit(self, name, size=None, value=None, maxLength=None):
        ret = self._make_reginput(name, 'text')
        if size is not None:
            ret.setAttribute('size', str(size))
        if maxLength is not None:
            ret.setAttribute('maxlength', str(maxLength))
        if value is not None:
            ret.setAttribute('value', value)
        return ret

    def addText(self, name, cols=None, rows=None, value=None):
        ret = self._make_input('textarea', name, False, False)
        if value is not None:
            ret.setText(value)
        if cols is None:
            ret.setAttribute('cols', '100%')
        else:
            ret.setAttribute('cols', str(cols))
        if rows is not None:
            ret.setAttribute('rows', str(rows))
        return ret

    def addPassword(self, name, size, maxLength=None):
        if maxLength is None:
            maxLength = size
        ret = self._make_reginput(name, 'password')
        ret.setAttribute('size', str(size))
        ret.setAttribute('maxlength', str(maxLength))
        return ret

    def addSelect(self, name, opts, selected=None):
        ret = self._make_input('select', name)
        if isinstance(opts, dict):
            opts = opts.items
        for k, v in opts:
            pair = ret.addLeaf('option', v).setAttribute('value', k)
            if selected is not None and selected == k:
                pair.setAttribute('selected', 'selected')
        return ret

    def addCheckBox(self, name, value, checked=False):
        ret = self._make_reginput(name, 'checkbox')
        ret.setAttribute('value', value)
        if checked:
            ret.setAttribute('checked', 'checked')
        return ret

    def addUpload(self, name):
        self.setAttribute('enctype', 'multipart/form-data')
        return self._make_reginput(name, 'file')

    def addHidden(self, name, value):
        return self._make_reginput(name, 'hidden').setAttribute('value', value)

    def addSubmit(self, label, name=None):
        return self._make_reginput(name, 'submit').setAttribute('value', label)

    def addSubmitImage(self, src, alt=None):
        if alt is None:
            alt = src
        ret = self._make_reginput('submit', 'image')
        ret.setAttribute('src', src)
        ret.setAttribute('alt', alt)
        return ret

    def _make_input(self, tag, name, do_format=None, do_short=True):
        if do_format is None:
            do_format = self._do_format
        args = (tag, None, do_format, do_short)
        if self._auto_import:
            ret = self.addLeaf(*args)
        else:
            ret = mz.Xhtml(*args)
        if name:
            if self._id:
                ret.setId(name+str(self._id))
                ret.setAttribute('name', name+'[]')
            else:
                ret.setAttribute('name', name)
        return ret

    def _make_reginput(self, name, input_type):
        return self._make_input('input', name).setAttribute('type', input_type)

# Mangoz - python/mangoz/__init__.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

from _mangoz import *
from tree import Tree
from xhtml import Xlist, Xtable, XtableRow, Xform
from xpage import Xpage
from textbox import TextBox
from http import Request, Response
from selector import Selector, Composer
try:
    from connector import django_connector, DjangoXform
except:
    pass

xhtml.fixup_api()

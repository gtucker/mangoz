# Mangoz - python/mangoz/selector.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import time
import threading
import _mangoz as mz
from tree import Tree
from xpage import Xpage
from xhtml import Xlist
from textbox import TextBox
from http import Response

class Selector(object):
    def __init__(self, conf, composers):
        self._conf = conf
        self._c = mz.CoreConfig(conf)
        self._lock = threading.Lock()
        self._composers = dict()
        for template, cls in composers.iteritems():
            self._composers[template] = cls(self._conf, self)
        self._std_composer = Composer(self._conf, self)
        self.reload()

    def reload(self):
        self._lock.acquire()
        self._tree = Tree(self._conf)
        if self._c.textbox is not None:
            self._tb = TextBox(self._conf)
        else:
            self._tb = None
        self._node = None
        self._cache = GetCache()
        self._nav = None
        self._lock.release()

    def lock(self):
        self._lock.acquire()
        self._tree.lock()
        if self._tb:
            self._tb.lock()

    def unlock(self):
        if self._tb:
            self._tb.unlock()
        self._tree.unlock()
        self._lock.release()

    def render(self, req):
        self.lock()
        self._prepare(req, True)
        template = self._get_template()
        resp = self._select(template, req)
        self.unlock()

        if resp.page is not None:
            resp.doc = mz.Xdocument(resp.page)
            resp.doc.setDoctype(self._c.dtd)
            if req.start_time:
                t = (time.time() - req.start_time) * 1000
                resp.page.addText("<!-- generated in {0:.3} msec -->"
                                   .format(t))
        else:
            resp.doc = None

        return resp

    @classmethod
    def produce(cls, resp, printer):
        if resp.doc is None:
            return

        printer.clear()
        resp.doc.produce(printer)
        if resp.start_time:
            t = (time.time() - resp.start_time) * 1000
            resp.start_time = None
            printer.produce("<!-- produced in {0:.3} msec -->".format(t))

    def getNodeTitle(self, node, lang=None):
        if lang is None:
            lang = self.language
        node = self._get_title_leaf(node, lang)
        if node is None:
            return None
        return node.getText()

    def fetchText(self, boxId, lang=None, nl2brParagraph=True):
        if self._tb is None:
            return boxId
        if lang is None:
            lang = self.language
        return self._tb.get(boxId, lang, nl2brParagraph)

    def getElements(self, index=0, absolute=False, lang=None):
        if lang is None:
            lang = self.language
        is_default = bool(self._c.defaultLanguage == lang)
        node = self._tree.getNode(index, absolute)
        if node is None:
            return None

        elements = []
        for x in node.getLeaves('xhtml'):
            x_lang = x.getAttribute('lang')
            if (x_lang is None and is_default) or (x_lang == lang):
                elements.append(x)
        return elements

    def getConfiguration(self):
        return self._conf
    configuration = property(getConfiguration)

    def getTitle(self):
        title = getattr(self._cache, 'title', None)
        if title is None:
            title = self.getNodeTitle(self._node)
            if title is None:
                title = self._node.id
            self._cache.title = title
        return title
    title = property(getTitle)

    def getDescription(self):
        desc_txt = getattr(self._cache, 'desc', None)
        if desc_txt is None:
            lang = self.language
            if lang is not None:
                desc = self._node.getLeaf('description', 'lang', lang)
            if desc is None and (lang == self._c.defaultLanguage):
                desc = self._node.getLeaf('description', 'lang', None)
            if desc is not None:
                desc_txt = desc.getText()
                self._cache.desc = desc_txt
        return desc_txt
    description = property(getDescription)

    @property
    def language(self):
        return self._cache.language

    @property
    def lang_list(self):
        return self._cache.lang_list

    @property
    def url(self):
        return ''.join([self._c.baseURL, self._tree.getPath()])

    def getNavigator(self):
        if self._nav is None:
            self._nav = Navigator(self, self._tree)
        return self._nav
    navigator = property(getNavigator)

    def _prepare(self, req, auto_path):
        self._cache = GetCache()
        self._tree.setPath(req.path, auto_path)
        self._node = self._tree.getNode()
        if self._node is None:
            raise Exception("Page not found ({0})".format(req.path))
        self._set_language(req)

    def _select(self, template, req):
        composer = self._composers.get(template, self._std_composer)
        return composer.compose(Xpage(self._conf), req)

    def _set_language(self, req):
        langs = self._node.getAttribute('langs')
        if langs is None:
            lang = self._c.defaultLanguage
            self._cache.lang_list = [lang]
        else:
            self._cache.lang_list = langs.strip().split(' ')
            lang = req.query.get(mz.QUERY_LANG, None)
            if lang is None:
                lang = req.session.get(mz.QUERY_LANG, None)
            if lang is None:
                pass # ToDo: parse req.header['HTTP_ACCEPT_LANGUAGE']
            if lang is None or lang not in self._cache.lang_list:
                lang = self._c.defaultLanguage
        req.session[mz.QUERY_LANG] = lang
        self._cache.language = lang

    def _get_template(self):
        t = self._node.getAttribute('template')
        if t is None:
            for i in range(self._tree.getOrigin(), -1, -1):
                t = self._tree.getLevel(i, True).getAttribute('template')
                if t is not None:
                    break
        return t

    def _get_title_leaf(self, node, lang):
        leaf = node.getLeaf('title', 'lang', lang)
        if leaf is None and (lang == self._c.defaultLanguage):
            leaf = node.getLeaf('title', 'lang', None)
        return leaf


class Composer(object):
    def __init__(self, conf, sel):
        self._conf = conf
        self._sel = sel

    def compose(self, page, req):
        page.setTitle(self._sel.title)
        self.importElements(page.body, self._sel.getElements())
        return Response(page)

    def importElements(self, root, ele):
        for e in ele:
            for leaf in e.getLeaves():
                root.importNode(leaf)

    def makeSubNav(self, nav, prefix, index):
        level = nav.getLevel(index, True)
        if level is None:
            return None

        sub = self.make_sub_list()
        for ele in level:
            px = ''.join(prefix, ele['name'])
            if ele['current']:
                point = self._add_sub_point(ele, sub, px)
                px.append('/')
                nxt = self.makeSubNav(nav, px, index+1)
                if nxt is not None:
                    point.importNode(nxt)
            else:
                self._add_sub_link(ele, sub, px)
        return sub

    def _make_sub_list(self):
        return Xlist()

    def _add_sub_point(self, ele, sub, px):
        return sub.addPoint(ele['title'])

    def _add_sub_link(self, ele, sub, px):
        return sub.addPoint().addLink(px, ele['title'])


class GetCache(object):
    pass


class Navigator(object):
    def __init__(self, sel, tree):
        self._sel = sel
        self._tree = tree

    def getLevel(self, index=0, absolute=False):
        level = self._tree.getLevel(index, absolute)
        if level is None:
            return None

        ret = []
        cur = self._tree.getNode(index, absolute)
        cur_id = cur.id
        for page in level.getLeaves('page'):
            title = self._sel.getNodeTitle(page)
            if title is not None:
                ret.append({'title': title, 'name': page.id,
                            'current': bool(page.id == cur_id)})
        return ret

    def getPageName(self, index, absolute):
        node = self._tree.getNode(index, absolute)
        if node is None:
            return None
        return node.id

# Mangoz - python/mangoz/connector.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import time
import _mangoz as mz
from http import Request
from xhtml import Xform
from selector import Selector
import django
from django.forms.fields import (CharField, IntegerField, TypedChoiceField,
                                 FileField)
from django.forms.models import ModelChoiceField
from django.forms.widgets import Textarea

def django_connector(sel, http_req, path):
    from django.core.context_processors import csrf
    from django.template import RequestContext

    class DjangoRequest(Request):
        def __init__(self, req):
            super(DjangoRequest, self).__init__(req.method, req.encoding)
            self.http_req = req
            self.query = req.GET
            self.post = req.POST
            self.session = req.session
            self.headers = req.META
            self.ctx = getattr(req, 'ctx', dict())

    req = DjangoRequest(http_req)
    req.start_time = time.time()
    req.path = path
    resp = sel.render(req)

    if resp.redirect is not None:
        django_resp = django.http.HttpResponseRedirect(resp.redirect)
    elif resp.allowed_methods is not None:
        django_resp = HttpResponseNotAllowed(resp.allowed_methods)
    else:
        resp.start_time = req.start_time
        sp = mz.StringPrinter()
        sel.produce(resp, sp)
        django_resp = django.http.HttpResponse(sp.getString())

    if resp.status is not None:
        django_resp.status_code = resp.status

    for k, v in resp.headers.items():
        django_resp[k] = v

    django_resp['Content-type'] = \
        '{0};charset={1}'.format(sel.configuration.get('contentType'),
                                 sel.configuration.get('charset'))

    return django_resp


class DjangoXform(Xform):
    def __init__(self, action, method, django_form, *args, **kw):
        super(DjangoXform, self).__init__(action, method, *args, **kw)
        self.django_form = django_form
        self.setAutoImport(False)

    def add_csrf(self, req):
        ctx = django.template.RequestContext(req.http_req)
        csrf = ctx.get('csrf_token', None)
        if csrf and csrf != 'NOTPROVIDED':
            div = self.addDiv().setAttribute('style', 'display:none')
            div.importNode(self.addHidden('csrfmiddlewaretoken', str(csrf)))

    def iter_input(self):
        for n, f in self.django_form.fields.items():
            yield n, self.add_input(n), f, self.django_form[n].errors

    def add_input(self, name):
        form_input = self.django_form.fields[name]
        try:
            handler = self.field_handlers[form_input.__class__]
        except KeyError:
            raise Exception('Unsupported form field {0}'.format(
                    form_input.__class__))
        initial = getattr(self.django_form, 'initial', dict())
        value = self.django_form.data.get(name, initial.get(name, None))
        if callable(value):
            value = value()
        return handler(self, name, form_input, value)

    def _add_CharField(self, name, form_input, value):
        if form_input.widget.__class__ == Textarea:
            return self.addText(name, value=value)
        return self.addEdit(name, value=value, maxLength=form_input.max_length)

    def _add_IntegerField(self, name, form_input, value):
        return self.addEdit(name, value=str(value), maxLength=32)

    def _add_ModelChoiceField(self, name, form_input, value):
        value = str(getattr(value, 'pk', value))
        return self.addSelect(name, self._str_choices(form_input), value)

    def _add_TypedChoiceField(self, name, form_input, value):
        return self.addSelect(name, self._str_choices(form_input), str(value))

    def _add_FileField(self, name, form_input, value):
        return self.addUpload(name)

    def _str_choices(self, form_input):
        for k, v in form_input.choices:
            yield str(k), str(v)

    field_handlers = {
        CharField: _add_CharField,
        IntegerField: _add_IntegerField,
        ModelChoiceField: _add_ModelChoiceField,
        TypedChoiceField: _add_TypedChoiceField,
        FileField: _add_FileField,
        }

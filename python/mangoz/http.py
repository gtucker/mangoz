# Mangoz - python/mangoz/http.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

class Request(object):
    def __init__(self, method, encoding='UTF-8'):
        self._method = method
        self._encoding = encoding
        self._path = None
        self._query = dict()
        self._post = dict()
        self._session = dict()
        self._headers = dict()
        self.start_time = None
        self.ctx = None

    def csrf(self, form):
        pass

    def getMethod(self):
        return self._method
    method = property(getMethod)

    def getEncoding(self):
        return self._encoding
    encoding = property(getEncoding)

    def setPath(self, path):
        self._path = path
    def getPath(self):
        return self._path
    path = property(getPath, setPath)

    def setQuery(self, query):
        Request._check_dict(query)
        self._query = query
    def getQuery(self):
        return self._query
    query = property(getQuery, setQuery)

    def setPost(self, post):
        Request._check_dict(post)
        self._post = post
    def getPost(self):
        return self._post
    post = property(getPost, setPost)

    def setSession(self, session):
        Request._check_dict(session)
        self._session = session
    def getSession(self):
        return self._session
    session = property(getSession, setSession)

    def setHeaders(self, headers):
        Request._check_dict(headers)
        self._headers = headers
    def getHeaders(self):
        return self._header
    headers = property(getHeaders, setHeaders)

    @staticmethod
    def _check_dict(obj):
        if not hasattr(obj, '__getitem__') or not hasattr(obj, '__setitem__'):
            raise Exception("Invalid dictionnary object")


class Response(object):
    def __init__(self, page=None, status=None, redirect=None,
                 allowed_methods=None):
        self._status = status
        self._redirect = redirect
        self._allowed_methods = allowed_methods
        self.page = page
        self.headers = dict()
        self.start_time = None

    @property
    def status(self):
        return self._status

    @property
    def redirect(self):
        return self._redirect

    @property
    def allowed_methods(self):
        return self._allowed_methods

# Mangoz - python/mangoz/tree.py
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

import os
import threading
import _mangoz as mz

class Tree(object):
    class Monitor(mz.Monitor):
        def __init__(self, tree, *args, **kw):
            super(Tree.Monitor, self).__init__(*args, **kw)
            self._tree = tree

        def event(self, full_path, file_name):
            self._tree.reload_file(full_path, file_name)

    # ToDo: mz.TreeConfig or simplify constructor
    # def __init__(self, staticDir, topPages):
    def __init__(self, conf):
        self._c = mz.CoreConfig(conf)
        file_path = os.path.join(self._c.staticDir, self._c.pages)
        self._doc = mz.tree_parser.parseFile(file_path)
        self._included = { self._c.pages: self._doc }
        self._xml_file = self._c.pages
        self.clear()
        self._lock = threading.Lock()
        self._monitor = Tree.Monitor(self)
        self._monitor.addFile(file_path, self._c.pages)
        self._monitor.run()

    def lock(self):
        self._lock.acquire()

    def unlock(self):
        self._lock.release()

    def clear(self):
        self._path = list()
        self._branch = list()
        self._origin = -1

    def reload_file(self, full_path, file_name):
        self.lock()
        if file_name == self._c.pages:
            self._doc = mz.tree_parser.parseFile(full_path)
            self._included[file_name] = self._doc
        else:
            self._monitor.removeFile(full_path)
            del(self._included[file_name])
        self.unlock()

    def setPath(self, pathStr, autoPath=True):
        pathStr = pathStr.strip('/')
        self._path = pathStr.split('/')
        path_len = len(self._path)
        self._branch = list()
        self._origin = -1
        xml_file = self._c.pages
        node = None
        index = 0
        level = self._doc.getRoot()

        while level is not None:
            if xml_file:
                self._xml_file = xml_file

            if index < path_len:
                step = self._path[index]
                if step == '':
                    step = None
            else:
                step = None

            if step is None and autoPath:
                step = level.getAttribute('auto')
                if step is not None:
                    node = level.getLeaf('page', 'id', step)
                else:
                    node = None

                if node is None:
                    node = level.getLeaf('page')
            elif step is not None:
                node = level.getLeaf('page', 'id', step)
            else:
                node = None

            self._branch.append({'level': level, 'node': node})
            self._origin += 1
            index += 1

            if (index == path_len) and not autoPath:
                level = None
            elif node:
                level, xml_file = self._get_sub_level(node, xml_file)
            else:
                level = None

        if index < path_len:
            raise Exception("invalid path ({0})".format(pathStr))

    def getPath(self, pos=0, absolute=False):
        if not absolute:
            pos += self._origin
        path= []
        for index in range(0, pos+1):
            node = self.getNode(index, True)
            if node:
                path.append(node.getId())
        path.append('')
        return '/'.join(path)
    path = property(getPath)

    def getOrigin(self):
        return self._origin
    origin = property(getOrigin)

    def getLevel(self, index=0, absolute=False):
        return self._get_ctx(index, absolute, 'level')

    def getNode(self, index=0, absolute=False):
        return self._get_ctx(index, absolute, 'node')

    def getNodeSubLevel(self, node=None):
        if not node:
            xml_file = self._xml_file
            node = self.getNode()
        return self._get_sub_level(node, xml_file)[0]

    def getDocument(self, xmlFile=None):
        if not xmlFile:
            xmlFile = self._xml_file
        return self._included[xmlFile]

    def getXmlFile(self):
        return self._xml_file

    @classmethod
    def splitPath(self, path):
        base, sep, page = path.rstrip('/').rpartition('/')
        return base, page

    def _get_sub_level(self, node, xml_file):
        level = None
        inc_node = node.getLeaf('include')
        if inc_node:
            inc = inc_node.getText()
        else:
            inc = None

        if inc:
            xml_file = inc
            if inc not in self._included:
                file_path = os.path.join(self._c.staticDir, inc)
                doc = mz.tree_parser.parseFile(file_path)
                self._included[inc] = doc
                self._monitor.addFile(file_path, inc)
            else:
                doc = self._included[inc]
            level = doc.getRoot()
        else:
            xml_file = self._xml_file
            level = node.getLeaf('pages')

        return level, xml_file

    def _get_ctx(self, index, absolute, item):
        if not absolute:
            index += self._origin
        if index < len(self._branch):
            return self._branch[index][item]
        else:
            return None

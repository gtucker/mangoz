/* Mangoz - python/Xdocument.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-python.h"
#include "mangoz-python-util.h"
#include "mangoz-python-private.h"
#include <structmember.h>

#define LOG(msg, ...)							\
	fprintf(stderr, "[Xdoc     %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

#if 0 /* set to 1 to enable memory allocation logging */
# define LOG_ALLOC(msg, ...) LOG(msg, ##__VA_ARGS__)
#else
# define LOG_ALLOC(msg, ...)
#endif

static void Xdoc_dealloc(Xdoc *self)
{
	Xhtml *r_obj = MZ_GET_PARENT(self->xdoc.root, Xhtml_object, xhtml);

	LOG_ALLOC("%p dealloc", &self->xdoc);

	if (self->xdoc.init) {
		Py_DECREF(r_obj);
		mz_xdoc_free(&self->xdoc);
	}

	self->ob_type->tp_free((PyObject *)self);
}

static int Xdoc_init(Xdoc *self, PyObject *args, PyObject *kw)
{
	Xhtml *root;

	if (!PyArg_ParseTuple(args, "O!", &mangoz_XhtmlType, &root))
		return -1;

	LOG_ALLOC("%p init %p", &self->xdoc, &root->xhtml);

	if (mz_xdoc_init(&self->xdoc, &root->xhtml) < 0)
		return -1;

	Py_INCREF(root);

	return 0;
}

static PyObject *Xdoc_new(PyTypeObject *type, PyObject *args, PyObject *kw)
{
	Xdoc *self;

	self = (Xdoc *)type->tp_alloc(type, 0);

	if (self == NULL)
		return NULL;

	MZ_CLEAR(self->xdoc);

	LOG_ALLOC("%p new", &self->xdoc);

	return (PyObject *)self;
}

/* - methods - */

static PyObject *XdocObj_getRoot(Xdoc *self)
{
	Xhtml *root;

	root = MZ_GET_PARENT(self->xdoc.root, Xhtml_object, xhtml);
	Py_INCREF(root);

	return (PyObject *)root;
}

static PyObject *XdocObj_setDoctype(Xdoc *self, PyObject *args)
{
	int dtd;

	if (!PyArg_ParseTuple(args, "i", &dtd))
		return NULL;

	if (mz_xdoc_set_std_doctype(&self->xdoc, dtd) < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject *XdocObj_getDoctype(Xdoc *self)
{
	if (self->xdoc.doctype == NULL)
		Py_RETURN_NONE;

	return mz_py_get_dtd_id(self->xdoc.doctype->dtd);
}

static PyObject *XdocObj_setDoctypeHeader(Xdoc *self, PyObject *args)
{
	char *header;
	int len;

	if (!PyArg_ParseTuple(args, "s#", &header, &len))
		return NULL;

	if (mz_xdoc_set_header(&self->xdoc, header, len) < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject *XdocObj_getDoctypeHeader(Xdoc *self)
{
	return mz2py_str(&self->xdoc.header);
}

static PyObject *XdocObj_setAttribute(Xdoc *self, PyObject *args)
{
	if (mz_py_set_attribute(&self->xdoc.atts, args) < 0)
		return NULL;

	Py_INCREF(self);

	return (PyObject *)self;
}

static PyObject *XdocObj_getAttribute(Xdoc *self, PyObject *args)
{
	return mz_py_get_attribute(&self->xdoc.atts, args);
}

static PyObject *XdocObj_produce(Xdoc *self, PyObject *args)
{
	PyObject *printer;

	if (!PyArg_ParseTuple(args, "O", &printer))
		return NULL;

	if (mz_xdoc_do_produce(&self->xdoc, printer, 0) < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject *XdocCls_getStdDoctypeHeader(PyTypeObject *_, PyObject *args)
{
	int dtd;

	if (!PyArg_ParseTuple(args, "i", &dtd))
		return NULL;

	return PyString_FromString(mz_xdoc_get_std_doctype(dtd)->header);
}

static PyMethodDef Xdoc_methods[] = {
	{ "getRoot", (PyCFunction)XdocObj_getRoot, METH_NOARGS,
	  "Get the root Xhtml." },
	{ "setDoctype", (PyCFunction)XdocObj_setDoctype, METH_VARARGS,
	  "Set the doctype by id." },
	{ "getDoctype", (PyCFunction)XdocObj_getDoctype, METH_NOARGS,
	  "Get the doctype id." },
	{ "setDoctypeHeader", (PyCFunction)XdocObj_setDoctypeHeader,
	  METH_VARARGS, "Directly set the doctype header." },
	{ "getDoctypeHeader", (PyCFunction)XdocObj_getDoctypeHeader,
	  METH_NOARGS, "Get the doctype header." },
	{ "setAttribute", (PyCFunction)XdocObj_setAttribute, METH_VARARGS,
	  "Set an attribute." },
	{ "getAttribute", (PyCFunction)XdocObj_getAttribute, METH_VARARGS,
	  "Get an attribute value." },
	{ "produce", (PyCFunction)XdocObj_produce, METH_VARARGS,
	  "Produce the XHTML." },
	{ "getStdDoctypeHeader", (PyCFunction)XdocCls_getStdDoctypeHeader,
	  METH_STATIC | METH_VARARGS,
	  "Get a standard doctype header for the given id." },
	{ NULL }
};

static PyGetSetDef Xdoc_getsetters[] = {
	{ NULL }
};

static PyMemberDef Xdoc_members[] = {
	{ NULL }
};

PyTypeObject mangoz_XdocumentType = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /* ob_size */
	"_mangoz.Xdocument",               /* tp_name */
	sizeof(Xdoc),                      /* tp_basicsize */
	0,                                 /* tp_itemsize */
	(destructor)Xdoc_dealloc,          /* tp_dealloc */
	0,                                 /* tp_print */
	0,                                 /* tp_getattr */
	0,                                 /* tp_setattr */
	0,                                 /* tp_compare */
	0,                                 /* tp_repr */
	0,                                 /* tp_as_number */
	0,                                 /* tp_as_sequence */
	0,                                 /* tp_as_mapping */
	0,                                 /* tp_hash  */
	0,                                 /* tp_call */
	0,                                 /* tp_str */
	0,                                 /* tp_getattro */
	0,                                 /* tp_setattro */
	0,                                 /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT,                /* tp_flags */
	"Mangoz Xdocument",                /* tp_doc */
	0,                                 /* tp_traverse */
	0,                                 /* tp_clear */
	0,                                 /* tp_richcompare */
	0,                                 /* tp_weaklistoffset */
	0,                                 /* tp_iter */
	0,                                 /* tp_iternext */
	Xdoc_methods,                      /* tp_methods */
	Xdoc_members,                      /* tp_members */
	Xdoc_getsetters,                   /* tp_getset */
	0,                                 /* tp_base */
	0,                                 /* tp_dict */
	0,                                 /* tp_descr_get */
	0,                                 /* tp_descr_set */
	0,                                 /* tp_dictoffset */
	(initproc)Xdoc_init,               /* tp_init */
	0,                                 /* tp_alloc */
	Xdoc_new,                          /* tp_new */
};

void mangoz_Xdocument_init(PyObject *m)
{
	Py_INCREF((PyObject *)(&mangoz_XdocumentType));
	PyModule_AddObject(m, "Xdocument", (PyObject *)&mangoz_XdocumentType);
}

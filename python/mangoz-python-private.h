/* Mangoz - python/mangoz-python-private.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MANGOZ_PYTHON_PRIVATE_H
#define INCLUDE_MANGOZ_PYTHON_PRIVATE_H 1

#include <Python.h>
#include <mangoz/libmz/libmz.h>

struct Xtext_object {
	PyObject_HEAD;
	struct mz_xtext xtext;
};
typedef struct Xtext_object Xtext;

struct Xhtml_object {
	PyObject_HEAD;
	struct mz_xhtml xhtml;
};
typedef struct Xhtml_object Xhtml;

struct Xdocument_object {
	PyObject_HEAD;
	struct mz_xdoc xdoc;
};
typedef struct Xdocument_object Xdoc;

struct Config_object {
	PyObject_HEAD;
	struct mz_config config;
};
typedef struct Config_object Config;

#endif /* INCLUDE_MANGOZ_PYTHON_PRIVATE_H */

/* Mangoz - python/mangoz-python.h
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#ifndef INCLUDE_MANGOZ_PYTHON_H
#define INCLUDE_MANGOZ_PYTHON_H 1

#include <Python.h>

extern PyObject *mangoz_Error;

extern PyObject *mangoz_XNODE_TEXT;
extern PyObject *mangoz_XNODE_HTML;
extern PyObject *mangoz_DTD_XHTML11;
extern PyObject *mangoz_DTD_STRICT;
extern PyObject *mangoz_DTD_TRANSITIONAL;
extern PyObject *mangoz_DTD_FRAMESET;
extern PyObject *mangoz_DTD_HTML;

extern PyTypeObject mangoz_XtextType;
extern PyTypeObject mangoz_XhtmlType;
extern PyTypeObject mangoz_XdocumentType;
extern PyTypeObject mangoz_StdPrinterType;
extern PyTypeObject mangoz_StringPrinterType;
extern PyTypeObject mangoz_ConfigurationType;
extern PyTypeObject mangoz_CoreConfigType;
extern PyTypeObject mangoz_PageConfigType;
extern PyTypeObject mangoz_XparserType;
extern PyTypeObject mangoz_MonitorType;

extern void mangoz_Xtext_init(PyObject *m);
extern void mangoz_Xhtml_init(PyObject *m);
extern void mangoz_Xdocument_init(PyObject *m);
extern void mangoz_StdPrinter_init(PyObject *m);
extern void mangoz_StringPrinter_init(PyObject *m);
extern void mangoz_Configuration_init(PyObject *m);
extern void mangoz_CoreConfig_init(PyObject *m);
extern void mangoz_PageConfig_init(PyObject *m);
extern void mangoz_Xparser_init(PyObject *m);
extern void mangoz_Monitor_init(PyObject *m);

extern unsigned long mangoz_Xtext_count;
extern unsigned long mangoz_Xhtml_count;

#endif /* INCLUDE_MANGOZ_PYTHON_H */

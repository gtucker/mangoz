/* Mangoz - python/Xhtml.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-python.h"
#include "mangoz-python-util.h"
#include "mangoz-python-private.h"
#include <structmember.h>

#define LOG(msg, ...)							\
	fprintf(stderr, "[Xhtml    %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

#if 0 /* set to 1 to enable memory allocation logging */
# define LOG_ALLOC(msg, ...) LOG(msg, ##__VA_ARGS__)
#else
# define LOG_ALLOC(msg, ...)
#endif

#define get_do_def_format(self, obj)					\
	((obj) == NULL) ? (self)->xhtml.xnode.do_format : py2mz_true((obj))

unsigned long mangoz_Xhtml_count = 0;

static PyObject *xnode_to_pyobj(struct mz_xnode *xnode);
static struct mz_xnode *pyobj_to_xnode(PyObject *pyobj);
static PyObject *add_helper_leaf(Xhtml *self, const char *tag,
				 size_t tag_len, PyObject *args);
static PyObject *add_leaf(Xhtml *self, const char *tag, size_t len,
			  PyObject *text_obj, PyObject *do_format_obj,
			  PyObject *do_short_obj);

static void Xhtml_dealloc(Xhtml *self)
{
	struct mz_xnode **node;

	--mangoz_Xhtml_count;
	LOG_ALLOC("%p dealloc (%lu)", &self->xhtml, mangoz_Xhtml_count);

	if (self->xhtml.xnode.init) {
		mz_vector_for_each(self->xhtml.nodes, node)
			Py_DECREF(xnode_to_pyobj(*node));

		mz_xhtml_free(&self->xhtml);
	}

	self->ob_type->tp_free((PyObject *)self);
}

static int Xhtml_init(PyObject *self, PyObject *args, PyObject *kw)
{
	char *tag;
	int tag_n;
	char *text = NULL;
	int text_n;
	PyObject *do_format = NULL;
	PyObject *do_short = NULL;

	if (!PyArg_ParseTuple(args, "s#|z#OO", &tag, &tag_n, &text, &text_n,
			      &do_format, &do_short))
		return -1;

	LOG_ALLOC("%p init %s", &((Xhtml *)self)->xhtml, tag);

	if (mz_xhtml_init(&((Xhtml *)self)->xhtml, tag, tag_n,
			  py2mz_true(do_format), py2mz_true(do_short)) < 0)
		return -1;

	if (text != NULL)
		if (mz_xhtml_add_text(&((Xhtml *)self)->xhtml, text, text_n,
				      py2mz_true(do_format)) < 0)
			return -1;

	return 0;
}

static PyObject *Xhtml_new(PyTypeObject *type, PyObject *args, PyObject *kw)
{
	Xhtml *self;

	self = (Xhtml *)type->tp_alloc(type, 0);

	if (self == NULL)
		return NULL;

	++mangoz_Xhtml_count;
	MZ_CLEAR(self->xhtml.xnode);

	LOG_ALLOC("%p new (%lu)", &self->xhtml, mangoz_Xhtml_count);

	return (PyObject *)self;
}

/* - methods - */

static PyObject *XhtmlObj_getTag(Xhtml *self)
{
	return mz2py_str(&self->xhtml.tag);
}

static PyObject *XhtmlObj_setAttribute(Xhtml *self, PyObject *args)
{
	if (mz_py_set_attribute(&self->xhtml.atts, args) < 0)
		return NULL;

	Py_INCREF(self);

	return (PyObject *)self;
}

static PyObject *XhtmlObj_setId(Xhtml *self, PyObject *args)
{
	char *id;
	int id_n;

	if (!PyArg_ParseTuple(args, "s#", &id, &id_n))
		return NULL;

	if (mz_attribute_set(&self->xhtml.atts, MZ_STRLEN("id"), id, id_n) < 0)
		return NULL;

	Py_INCREF(self);

	return (PyObject *)self;
}

static PyObject *XhtmlObj_setClass(Xhtml *self, PyObject *args)
{
	char *cls;
	int cls_n;

	if (!PyArg_ParseTuple(args, "s#", &cls, &cls_n))
		return NULL;

	if (mz_attribute_set(&self->xhtml.atts, MZ_STRLEN("class"),
			     cls, cls_n) < 0)
		return NULL;

	Py_INCREF(self);

	return (PyObject *)self;
}

static PyObject *XhtmlObj_getAttribute(Xhtml *self, PyObject *args)
{
	return mz_py_get_attribute(&self->xhtml.atts, args);
}

static PyObject *XhtmlObj_getAttributes(Xhtml *self)
{
	PyObject *dict;
	struct mz_attribute *att;

	dict = PyDict_New();

	if (dict == NULL)
		return NULL;

	mz_vector_for_each(self->xhtml.atts, att) {
		if (PyDict_SetItem(dict, mz2py_str(&att->name),
				   mz2py_str(&att->value)) < 0)
			return NULL;
	}

	return dict;
}

static PyObject *XhtmlObj_getId(Xhtml *self)
{
	const struct mz_string *id;

	id = mz_attribute_get(&self->xhtml.atts, MZ_STRLEN("id"));

	if (id == NULL)
		Py_RETURN_NONE;

	return mz2py_str(id);
}

static PyObject *XhtmlObj_getClass(Xhtml *self)
{
	const struct mz_string *cls;

	cls = mz_attribute_get(&self->xhtml.atts, MZ_STRLEN("class"));

	if (cls == NULL)
		return NULL;

	return mz2py_str(cls);
}

static PyObject *XhtmlObj_addText(Xhtml *self, PyObject *args)
{
	PyObject *text_obj;
	PyObject *do_format = NULL;
	PyObject *str_obj;
	struct mz_xtext *xtext;
	char *text;
	size_t text_n;
	Xtext *ret;

	if (!PyArg_ParseTuple(args, "O|O", &text_obj, &do_format))
		return NULL;

	str_obj = mz_py_unistrobj(text_obj, &text, &text_n);
	xtext = mz_xhtml_add_text(&self->xhtml, text, text_n,
				  get_do_def_format(self, do_format));
	Py_XDECREF(str_obj);

	if (xtext == NULL)
		return NULL;

	ret = MZ_GET_PARENT(xtext, Xtext_object, xtext);
	Py_INCREF(ret);

	return (PyObject *)ret;
}

static PyObject *XhtmlObj_setText(Xhtml *self, PyObject *args)
{
	PyObject *text_obj;
	PyObject *str_obj;
	char *text;
	size_t text_n;
	int ret;

	if (!PyArg_ParseTuple(args, "O", &text_obj))
		return NULL;

	str_obj = mz_py_unistrobj(text_obj, &text, &text_n);
	ret = mz_xhtml_set_text(&self->xhtml, text, text_n);
	Py_XDECREF(str_obj);

	if (ret < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject *XhtmlObj_getText(Xhtml *self, PyObject *args)
{
	long index = 0;
	struct mz_xtext *xtext;

	if (!PyArg_ParseTuple(args, "|l", &index))
		return NULL;

	xtext = mz_xhtml_get_text_node(&self->xhtml, index);

	if (xtext == NULL)
		Py_RETURN_NONE;

	return mz2py_str(&xtext->text);
}

static PyObject *XhtmlObj_getSplitText(Xhtml *self, PyObject *args)
{
	long index = 0;
	struct mz_xtext *xtext;
	struct mz_string *line;
	PyObject *list;
	size_t i;

	if (!PyArg_ParseTuple(args, "|l", &index))
		return NULL;

	xtext = mz_xhtml_get_text_node(&self->xhtml, index);

	if (xtext == NULL)
		Py_RETURN_NONE;

	list = PyList_New(xtext->split.n);

	if (list == NULL)
		return NULL;

	mz_vector_for_each_n(xtext->split, line, i) {
		if (PyList_SetItem(list, i, mz2py_str(line)) < 0)
			return NULL;
	}

	return list;
}

static PyObject *XhtmlObj_getTextNode(Xhtml *self, PyObject *args)
{
	long index = 0;
	struct mz_xtext *xtext;
	Xtext *ret;

	if (!PyArg_ParseTuple(args, "|l", &index))
		return NULL;

	xtext = mz_xhtml_get_text_node(&self->xhtml, index);

	if (xtext == NULL)
		Py_RETURN_NONE;

	ret = MZ_GET_PARENT(xtext, Xtext_object, xtext);
	Py_INCREF(ret);

	return (PyObject *)ret;
}

static PyObject *XhtmlObj_countText(Xhtml *self)
{
	size_t ntext;

	ntext = mz_xhtml_count_nodes(&self->xhtml, MZ_XNODE_TEXT);

	return Py_BuildValue("i", ntext);
}

static PyObject *XhtmlObj_addLeaf(Xhtml *self, PyObject *args)
{
	char *tag;
	int tag_n;
	PyObject *text = NULL;
	PyObject *do_format = NULL;
	PyObject *do_short = NULL;

	if (!PyArg_ParseTuple(args, "s#|OOO", &tag, &tag_n, &text,
			      &do_format, &do_short))
		return NULL;

	return add_leaf(self, tag, tag_n, text, do_format, do_short);
}

static PyObject *XhtmlObj_getIndexedLeaf(Xhtml *self, PyObject *args)
{
	char *tag;
	int tag_n;
	long index;
	struct mz_xhtml *found;
	Xhtml *ret;

	if (!PyArg_ParseTuple(args, "s#l", &tag, &tag_n, &index))
		return NULL;

	found = mz_xhtml_get_indexed_leaf(&self->xhtml, tag, tag_n, index);

	if (found == NULL)
		Py_RETURN_NONE;

	ret = MZ_GET_PARENT(found, Xhtml_object, xhtml);
	Py_INCREF(ret);

	return (PyObject *)ret;
}

static PyObject *XhtmlObj_getLeaf(Xhtml *self, PyObject *args)
{
	struct mz_xhtml *found;
	char *tag;
	int tag_n;
	char *name = NULL;
	int name_n;
	PyObject *value_obj = NULL;
	PyObject *str_obj;
	char *value;
	size_t value_n;
	Xhtml *ret;

	if (!PyArg_ParseTuple(args, "s#|z#O", &tag, &tag_n, &name, &name_n,
			      &value_obj))
		return NULL;

	str_obj = mz_py_unistrobj(value_obj, &value, &value_n);
	found = mz_xhtml_get_leaf(&self->xhtml, tag, tag_n, name, name_n,
				  value, value_n);
	Py_XDECREF(str_obj);

	if (found == NULL)
		Py_RETURN_NONE;

	ret = MZ_GET_PARENT(found, Xhtml_object, xhtml);
	Py_INCREF(ret);

	return (PyObject *)ret;
}

static PyObject *XhtmlObj_getLeaves(Xhtml *self, PyObject *args)
{
	char *tag = NULL;
	int tag_n;
	struct mz_xnode **xnode;
	PyObject *list;

	if (!PyArg_ParseTuple(args, "|z#", &tag, &tag_n))
		return NULL;

	list = PyList_New(0);

	if (list == NULL)
		return NULL;

	mz_vector_for_each(self->xhtml.nodes, xnode) {
		struct mz_xhtml *x;
		Xhtml *obj;

		if ((*xnode)->node_type != MZ_XNODE_HTML)
			continue;

		x = MZ_GET_PARENT(*xnode, mz_xhtml, xnode);

		if ((tag != NULL) && (mz_string_cmp(&x->tag, tag, tag_n)))
			continue;

		obj = MZ_GET_PARENT(x, Xhtml_object, xhtml);
		Py_INCREF(obj);
		PyList_Append(list, (PyObject *)obj);
	}

	return list;
}

static PyObject *XhtmlObj_countLeaves(Xhtml *self)
{
	size_t nleaves;

	nleaves = mz_xhtml_count_nodes(&self->xhtml, MZ_XNODE_HTML);

	return Py_BuildValue("i", nleaves);
}

static PyObject *XhtmlObj_importNode(Xhtml *self, PyObject *args)
{
	struct mz_xnode *xnode;
	PyObject *node_obj;

	if (!PyArg_ParseTuple(args, "O", &node_obj))
		return NULL;

	xnode = pyobj_to_xnode(node_obj);

	if (xnode == NULL)
		return NULL;

	if (mz_xhtml_import_node(&self->xhtml, xnode) < 0)
		return NULL;

	Py_INCREF(node_obj); /* one is imported */
	Py_INCREF(node_obj); /* one is returned */

	return node_obj;
}

static PyObject *XhtmlObj_insertNode(Xhtml *self, PyObject *args)
{
	PyObject *node;
	long index = 0;
	struct mz_xnode *xnode;

	if (!PyArg_ParseTuple(args, "O|l", &node, &index))
		return NULL;

	xnode = pyobj_to_xnode(node);

	if (xnode == NULL)
		return NULL;

	if (mz_vector_insert(&self->xhtml.nodes, &xnode, index) < 0)
		return NULL;

	Py_INCREF(node);

	return node;
}

static PyObject *XhtmlObj_removeNode(Xhtml *self, PyObject *args)
{
	PyObject *node;
	struct mz_xnode *xnode;

	if (!PyArg_ParseTuple(args, "O", &node))
		return NULL;

	xnode = pyobj_to_xnode(node);

	if (xnode == NULL)
		return NULL;

	if (mz_xhtml_remove_node(&self->xhtml, xnode) < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject *XhtmlObj_getNode(Xhtml *self, PyObject *args)
{
	long index;
	struct mz_xnode **found;
	PyObject *ret;

	if (!PyArg_ParseTuple(args, "l", &index))
		return NULL;

	found = mz_vector_get(&self->xhtml.nodes, index);

	if (found == NULL)
		Py_RETURN_NONE;

	ret = xnode_to_pyobj(*found);
	Py_INCREF(ret);

	return ret;
}

static PyObject *XhtmlObj_getNodes(Xhtml *self)
{
	struct mz_xnode **xnode;
	PyObject *list;
	size_t i;

	list = PyList_New(self->xhtml.nodes.n);

	if (list == NULL)
		return NULL;

	mz_vector_for_each_n(self->xhtml.nodes, xnode, i) {
		PyObject *obj;

		obj = xnode_to_pyobj(*xnode);

		if (obj == NULL)
			return NULL;

		if (PyList_SetItem(list, i, obj) < 0)
			return NULL;

		Py_INCREF(obj);
	}

	return list;
}

static PyObject *XhtmlObj_countNodes(Xhtml *self)
{
	return Py_BuildValue("l", self->xhtml.nodes.n);
}

static PyObject *XhtmlObj_addP(Xhtml *self, PyObject *args)
{
	return add_helper_leaf(self, MZ_STRLEN("p"), args);
}

static PyObject *XhtmlObj_addDiv(Xhtml *self, PyObject *args)
{
	return add_helper_leaf(self, MZ_STRLEN("div"), args);
}

static PyObject *XhtmlObj_addSpan(Xhtml *self, PyObject *args)
{
	return add_helper_leaf(self, MZ_STRLEN("span"), args);
}

static PyObject *XhtmlObj_addFile(Xhtml *self, PyObject *args)
{
	char *file_name;
	PyObject *do_format;
	struct mz_xtext *xtext;
	Xtext *ret_obj;

	if (!PyArg_ParseTuple(args, "s|O", &file_name, &do_format))
		return NULL;

	xtext = mz_xhtml_add_text(&self->xhtml, MZ_STRLEN(""),
				  get_do_def_format(self, do_format));

	if (xtext == NULL)
		return NULL;

	if (mz_xtext_set_from_file(xtext, file_name) < 0)
		return NULL;

	ret_obj = MZ_GET_PARENT(xtext, Xtext_object, xtext);
	Py_INCREF(ret_obj);

	return (PyObject *)ret_obj;
}

static PyObject *XhtmlObj_getNodeType(Xhtml *self)
{
	Py_INCREF(mangoz_XNODE_HTML);

	return mangoz_XNODE_HTML;
}

static PyObject *XhtmlObj_equals(Xhtml *self, PyObject *args)
{
	Xhtml *other;

	if (!PyArg_ParseTuple(args, "O!", &mangoz_XhtmlType, &other))
		return NULL;

	if (!mz_xhtml_equals(&self->xhtml, &other->xhtml))
		Py_RETURN_FALSE;

	Py_RETURN_TRUE;
}

static PyObject *XhtmlObj_produce(Xhtml *self, PyObject *args)
{
	PyObject *printer;

	if (!PyArg_ParseTuple(args, "O", &printer))
		return NULL;

	if (mz_xhtml_do_produce(&self->xhtml, printer, 0, false) < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject *XhtmlCls_fixup_api(PyTypeObject *type, PyObject *args)
{
	PyObject *fixup;

	if (!PyArg_ParseTuple(args, "O!", &PyDict_Type, &fixup))
		return NULL;

	if (PyDict_Update(type->tp_dict, fixup) < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyMethodDef Xhtml_methods[] = {
	{ "getTag", (PyCFunction)XhtmlObj_getTag, METH_NOARGS,
	  "Get the tag string." },
	{ "setAttribute", (PyCFunction)XhtmlObj_setAttribute, METH_VARARGS,
	  "Set an attribute." },
	{ "setId", (PyCFunction)XhtmlObj_setId, METH_VARARGS,
	  "Set the id attribute." },
	{ "setClass", (PyCFunction)XhtmlObj_setClass, METH_VARARGS,
	  "Set the class attribute." },
	{ "getAttribute", (PyCFunction)XhtmlObj_getAttribute, METH_VARARGS,
	  "Get an attribute value." },
	{ "getAttributes", (PyCFunction)XhtmlObj_getAttributes, METH_NOARGS,
	  "Get all the attributes in a dictionary." },
	{ "getId", (PyCFunction)XhtmlObj_getId, METH_NOARGS,
	  "Get the value of the id attribute." },
	{ "getClass", (PyCFunction)XhtmlObj_getClass, METH_NOARGS,
	  "Get the value of the class attribute." },
	{ "addText", (PyCFunction)XhtmlObj_addText, METH_VARARGS,
	  "Add a text node." },
	{ "setText", (PyCFunction)XhtmlObj_setText, METH_VARARGS,
	  "Set the value of the first text node." },
	{ "getText", (PyCFunction)XhtmlObj_getText, METH_VARARGS,
	  "Get the value of the first text node." },
	{ "getTextNode", (PyCFunction)XhtmlObj_getTextNode, METH_VARARGS,
	  "Get an existing text node." },
	{ "getSplitText", (PyCFunction)XhtmlObj_getSplitText, METH_VARARGS,
	  "Get the value of the first text node split into a list of lines." },
	{ "countText", (PyCFunction)XhtmlObj_countText, METH_NOARGS,
	  "Get the number of text nodes." },
	{ "addLeaf", (PyCFunction)XhtmlObj_addLeaf, METH_VARARGS,
	  "Add a leaf." },
	{ "getIndexedLeaf", (PyCFunction)XhtmlObj_getIndexedLeaf, METH_VARARGS,
	  "Get a leaf by its index." },
	{ "getLeaf", (PyCFunction)XhtmlObj_getLeaf, METH_VARARGS,
	  "Get a leaf by tag and attribute values." },
	{ "getLeaves", (PyCFunction)XhtmlObj_getLeaves, METH_VARARGS,
	  "Get a list with all the leaves." },
	{ "countLeaves", (PyCFunction)XhtmlObj_countLeaves, METH_NOARGS,
	  "Get the number of leaves." },
	{ "importNode", (PyCFunction)XhtmlObj_importNode, METH_VARARGS,
	  "Import a node." },
	{ "insertNode", (PyCFunction)XhtmlObj_insertNode, METH_VARARGS,
	  "Insert a node." },
	{ "removeNode", (PyCFunction)XhtmlObj_removeNode, METH_VARARGS,
	  "Remove a node." },
	{ "getNode", (PyCFunction)XhtmlObj_getNode, METH_VARARGS,
	  "Get a node by its index." },
	{ "getNodes", (PyCFunction)XhtmlObj_getNodes, METH_NOARGS,
	  "Get a list with all the nodes." },
	{ "countNodes", (PyCFunction)XhtmlObj_countNodes, METH_NOARGS,
	  "Get the number of nodes." },
	{ "addP", (PyCFunction)XhtmlObj_addP, METH_VARARGS,
	  "Add a paragraph leaf." },
	{ "addDiv", (PyCFunction)XhtmlObj_addDiv, METH_VARARGS,
	  "Add a division leaf." },
	{ "addSpan", (PyCFunction)XhtmlObj_addSpan, METH_VARARGS,
	  "Add a span leaf." },
	{ "addFile", (PyCFunction)XhtmlObj_addFile, METH_VARARGS,
	  "Add a text leaf with the contents of a given file." },
	{ "getNodeType", (PyCFunction)XhtmlObj_getNodeType, METH_VARARGS,
	  "Get the node type." },
	{ "equals", (PyCFunction)XhtmlObj_equals, METH_VARARGS,
	  "Test if equals in value the given XHTML node." },
	{ "produce", (PyCFunction)XhtmlObj_produce, METH_VARARGS,
	  "Produce the XHTML." },
	{ "fixup_api", (PyCFunction)XhtmlCls_fixup_api,
	  METH_VARARGS | METH_CLASS, "Add attributes to the Xhtml class." },
	{ NULL }
};

static PyMemberDef Xhtml_members[] = {
	{ NULL }
};

static PyGetSetDef Xhtml_getsetters[] = {
	{ NULL }
};

PyTypeObject mangoz_XhtmlType = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /* ob_size */
	"_mangoz.Xhtml",                   /* tp_name */
	sizeof(Xhtml),                     /* tp_basicsize */
	0,                                 /* tp_itemsize */
	(destructor)Xhtml_dealloc,         /* tp_dealloc */
	0,                                 /* tp_print */
	0,                                 /* tp_getattr */
	0,                                 /* tp_setattr */
	0,                                 /* tp_compare */
	0,                                 /* tp_repr */
	0,                                 /* tp_as_number */
	0,                                 /* tp_as_sequence */
	0,                                 /* tp_as_mapping */
	0,                                 /* tp_hash  */
	0,                                 /* tp_call */
	0,                                 /* tp_str */
	0,                                 /* tp_getattro */
	0,                                 /* tp_setattro */
	0,                                 /* tp_as_buffer */
	MZ_BASE_TYPE_FLAGS,                /* tp_flags */
	"Mangoz Xhtml",                    /* tp_doc */
	0,                                 /* tp_traverse */
	0,                                 /* tp_clear */
	0,                                 /* tp_richcompare */
	0,                                 /* tp_weaklistoffset */
	0,                                 /* tp_iter */
	0,                                 /* tp_iternext */
	Xhtml_methods,                     /* tp_methods */
	Xhtml_members,                     /* tp_members */
	Xhtml_getsetters,                  /* tp_getset */
	0,                                 /* tp_base */
	0,                                 /* tp_dict */
	0,                                 /* tp_descr_get */
	0,                                 /* tp_descr_set */
	0,                                 /* tp_dictoffset */
	Xhtml_init,                        /* tp_init */
	0,                                 /* tp_alloc */
	Xhtml_new,                         /* tp_new */
};

void mangoz_Xhtml_init(PyObject *m)
{
	Py_INCREF((PyObject *)&mangoz_XhtmlType);
	PyModule_AddObject(m, "Xhtml", (PyObject *)&mangoz_XhtmlType);
}

struct mz_xhtml *mangoz_py_create_xhtml(const char *tag, size_t len,
					mz_bool do_format, mz_bool do_short)
{
	Xhtml *self;

	self = (Xhtml *)mangoz_XhtmlType.tp_alloc(&mangoz_XhtmlType, 0);

	if (self == NULL)
		return NULL;

	++mangoz_Xhtml_count;
	LOG_ALLOC("%p create (%lu)", &((Xhtml *)self)->xhtml,
		  mangoz_Xhtml_count);
	MZ_CLEAR(self->xhtml.xnode);

	if (mz_xhtml_init(&self->xhtml, tag, len, do_format, do_short) < 0) {
		Py_DECREF(self);
		return NULL;
	}

	return &self->xhtml;
}

/* ----------------------------------------------------------------------------
 * private functions
 */

static PyObject *xnode_to_pyobj(struct mz_xnode *xnode)
{
	PyObject *ret = NULL;

	switch (xnode->node_type) {
	case MZ_XNODE_TEXT: {
		struct mz_xtext *xtext = MZ_GET_PARENT(xnode, mz_xtext, xnode);
		ret = MZ_GET_PARENT(xtext, Xtext_object, xtext);
		break;
	}
	case MZ_XNODE_HTML: {
		struct mz_xhtml *xhtml = MZ_GET_PARENT(xnode, mz_xhtml, xnode);
		ret = MZ_GET_PARENT(xhtml, Xhtml_object, xhtml);
		break;
	}
	}

	return ret;
}

static struct mz_xnode *pyobj_to_xnode(PyObject *pyobj)
{
	struct mz_xnode *ret;

	if (PyObject_IsInstance(pyobj, (PyObject *)&mangoz_XtextType))
		ret = &((Xtext *)pyobj)->xtext.xnode;
	else if (PyObject_IsInstance(pyobj, (PyObject *)&mangoz_XhtmlType))
		ret = &((Xtext *)pyobj)->xtext.xnode;
	else
		ret = NULL;

	return ret;
}

/* ToDo: add keyword arguments esp. for do_format and do_short */
static PyObject *add_helper_leaf(Xhtml *self, const char *tag, size_t tag_n,
				 PyObject *args)
{
	PyObject *text = NULL;
	PyObject *do_format = NULL;
	PyObject *do_short = NULL;

	if (!PyArg_ParseTuple(args, "|OOO", &text, &do_format, &do_short))
		return NULL;

	return add_leaf(self, tag, tag_n, text, do_format, do_short);
}

static PyObject *add_leaf(Xhtml *self, const char *tag, size_t tag_len,
			  PyObject *text_obj, PyObject *do_format,
			  PyObject *do_short)
{
	struct mz_xhtml *xhtml;
	PyObject *str_obj;
	PyObject *ret;
	char *text;
	size_t text_n;

	str_obj = mz_py_unistrobj(text_obj, &text, &text_n);
	xhtml = mz_xhtml_add_leaf(&self->xhtml, tag, tag_len, text, text_n,
				  get_do_def_format(self, do_format),
				  py2mz_true(do_short));
	Py_XDECREF(str_obj);

	if (xhtml == NULL)
		return NULL;

	ret = MZ_GET_PARENT(xhtml, Xhtml_object, xhtml);
	Py_INCREF(ret);

	return (PyObject *)ret;
}

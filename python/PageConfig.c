/* Mangoz - python/PageConfig.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-python.h"
#include "mangoz-python-util.h"
#include "mangoz-python-private.h"
#include <structmember.h>

#define LOG(msg, ...)				\
	fprintf(stderr, "[PageConf %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

#if 0 /* set to 1 to enable memory allocation logging */
# define LOG_ALLOC(msg, ...) LOG(msg, ##__VA_ARGS__)
#else
# define LOG_ALLOC(msg, ...)
#endif

struct PageConfig_object {
	PyObject_HEAD;
	PyObject *defaultLanguage;
	PyObject *charset;
	PyObject *defaultIcon;
	PyObject *baseURL;
	PyObject *publicURL;
	PyObject *defaultCSS;
	PyObject *contentType;
};
typedef struct PageConfig_object PageConfig;

static void PageConfig_dealloc(PageConfig *self)
{
	LOG_ALLOC("%p page dealloc", self);

	Py_XDECREF(self->defaultLanguage);
	Py_XDECREF(self->charset);
	Py_XDECREF(self->defaultIcon);
	Py_XDECREF(self->baseURL);
	Py_XDECREF(self->publicURL);
	Py_XDECREF(self->defaultCSS);
	Py_XDECREF(self->contentType);
}

static int PageConfig_init(PageConfig *self, PyObject *args, PyObject *kw)
{
	Config *config;
	const struct mz_page_config *page;

	if (!PyArg_ParseTuple(args, "O!", &mangoz_ConfigurationType, &config))
		return -1;

	LOG_ALLOC("%p init %p", self, config);

	page = mz_config_get_page(&config->config);

	if (page == NULL)
		return -1;

	self->defaultLanguage = mz2py_str(page->defaultLanguage);
	self->charset = mz2py_str(page->charset);
	self->defaultIcon = mz2py_str(page->defaultIcon);
	self->baseURL = mz2py_str(page->baseURL);
	self->publicURL = mz2py_str(page->publicURL);
	self->defaultCSS = mz2py_strlist(&page->css_list);
	self->contentType = mz2py_str(page->contentType);

	return 0;
}

static PyMemberDef PageConfig_members[] = {
	{ "defaultLanguage", T_OBJECT, offsetof(PageConfig, defaultLanguage),
	  READONLY, "defaultLanguage" },
	{ "charset", T_OBJECT, offsetof(PageConfig, charset),
	  READONLY, "charset" },
	{ "defaultIcon", T_OBJECT, offsetof(PageConfig, defaultIcon),
	  READONLY, "defaultIcon" },
	{ "baseURL", T_OBJECT, offsetof(PageConfig, baseURL),
	  READONLY, "baseURL" },
	{ "publicURL", T_OBJECT, offsetof(PageConfig, publicURL),
	  READONLY, "publicURL" },
	{ "defaultCSS", T_OBJECT, offsetof(PageConfig, defaultCSS),
	  READONLY, "defaultCSS" },
	{ "contentType", T_OBJECT, offsetof(PageConfig, contentType),
	  READONLY, "contentType" },
	{ NULL }
};

PyTypeObject mangoz_PageConfigType = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /* ob_size */
	"_mangoz.PageConfig",              /* tp_name */
	sizeof(Config),                    /* tp_basicsize */
	0,                                 /* tp_itemsize */
	(destructor)PageConfig_dealloc,    /* tp_dealloc */
	0,                                 /* tp_print */
	0,                                 /* tp_getattr */
	0,                                 /* tp_setattr */
	0,                                 /* tp_compare */
	0,                                 /* tp_repr */
	0,                                 /* tp_as_number */
	0,                                 /* tp_as_sequence */
	0,                                 /* tp_as_mapping */
	0,                                 /* tp_hash  */
	0,                                 /* tp_call */
	0,                                 /* tp_str */
	0,                                 /* tp_getattro */
	0,                                 /* tp_setattro */
	0,                                 /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT,                /* tp_flags */
	"Mangoz PageConfig",               /* tp_doc */
	0,                                 /* tp_traverse */
	0,                                 /* tp_clear */
	0,                                 /* tp_richcompare */
	0,                                 /* tp_weaklistoffset */
	0,                                 /* tp_iter */
	0,                                 /* tp_iternext */
	0,                                 /* tp_methods */
	PageConfig_members,                /* tp_members */
	0,                                 /* tp_getset */
	0,                                 /* tp_base */
	0,                                 /* tp_dict */
	0,                                 /* tp_descr_get */
	0,                                 /* tp_descr_set */
	0,                                 /* tp_dictoffset */
	(initproc)PageConfig_init,         /* tp_init */
	0,                                 /* tp_alloc */
	0,                                 /* tp_new */
};

void mangoz_PageConfig_init(PyObject *m)
{
	Py_INCREF((PyObject *)(&mangoz_PageConfigType));
	PyModule_AddObject(m, "PageConfig",
			   (PyObject *)&mangoz_PageConfigType);
}

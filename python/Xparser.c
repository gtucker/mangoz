/* Mangoz - python/Xparser.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-python.h"
#include "mangoz-python-util.h"
#include "mangoz-python-private.h"

#define LOG(msg, ...)							\
	fprintf(stderr, "[Xparser  %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

#if 0 /* set to 1 to enable memory allocation logging */
# define LOG_ALLOC(msg, ...) LOG(msg, ##__VA_ARGS__)
#else
# define LOG_ALLOC(msg, ...)
#endif

struct Xparser_object {
	PyObject_HEAD;
	struct mz_xparser xparser;
};
typedef struct Xparser_object Xparser;

static mz_bool format(void *ctx, struct mz_xhtml *x, mz_bool do_format);

static void Xparser_dealloc(Xparser *self)
{
	LOG_ALLOC("%p dealloc", self);

	if (self->xparser.init)
		mz_xparser_free(&((Xparser *)self)->xparser);

	self->ob_type->tp_free(self);
}

static int Xparser_init(Xparser *self, PyObject *args, PyObject *kw)
{
	LOG_ALLOC("%p init", self);

	return mz_xparser_init(&self->xparser, format, self);
}

static PyObject *Xparser_new(PyTypeObject *type, PyObject *args, PyObject *kw)
{
	Xparser *self;

	self = (Xparser *)type->tp_alloc(type, 0);

	if (self == NULL)
		return NULL;

	MZ_CLEAR(self->xparser);

	LOG_ALLOC("%p new", &self->xparser);

	return (PyObject *)self;
}

/* - methods - */

static PyObject *XparserObj_parseFile(Xparser *self, PyObject *args)
{
	Xdoc *xdoc;
	char *file_name;
	PyObject *do_format = NULL;

	if (!PyArg_ParseTuple(args, "s|O!", &file_name,
			      &PyBool_Type, &do_format))
		return NULL;

	xdoc = (Xdoc *)mangoz_XdocumentType.tp_alloc(&mangoz_XdocumentType, 0);

	if (xdoc == NULL)
		return NULL;

	if (mz_xparser_parse_file(&self->xparser, &xdoc->xdoc, file_name,
				  py2mz_true(do_format)) < 0)
		return NULL;

	return (PyObject *)xdoc;
}

static PyObject *XparserObj_parseString(Xparser *self, PyObject *args)
{
	Xdoc *xdoc;
	char *str;
	int len;
	PyObject *do_format = NULL;

	if (!PyArg_ParseTuple(args, "s#|O!", &str, &len,
			      &PyBool_Type, &do_format))
		return NULL;

	xdoc = (Xdoc *)mangoz_XdocumentType.tp_alloc(&mangoz_XdocumentType, 0);

	if (xdoc == NULL)
		return NULL;

	if (mz_xparser_parse_string(&self->xparser, &xdoc->xdoc, str, len,
				    py2mz_true(do_format)) < 0)
		return NULL;

	return (PyObject *)xdoc;
}

static PyObject *XparserObj_handleFormat(Xparser *self, PyObject *args)
{
	Xhtml *x_obj;
	PyObject *f_obj;
	mz_bool do_format;

	if (!PyArg_ParseTuple(args, "O!O!", &mangoz_XhtmlType, &x_obj,
			      &PyBool_Type, &f_obj))
		return NULL;

	do_format = mz_std_handle_format(self->xparser.ctx, &x_obj->xhtml,
					 py2mz_true(f_obj));

	return mz2py_bool(do_format);
}

static PyMethodDef Xparser_methods[] = {
	{ "parseFile", (PyCFunction)XparserObj_parseFile, METH_VARARGS,
	  "Parse the given file into a document instance." },
	{ "parseString", (PyCFunction)XparserObj_parseString, METH_VARARGS,
	  "Parse the given string into a document instance." },
	{ "handleFormat", (PyCFunction)XparserObj_handleFormat, METH_VARARGS,
	  "Handle the format of a just-parsed Xhtml instance." },
	{ NULL }
};

PyTypeObject mangoz_XparserType = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /* ob_size */
	"_mangoz.Xparser",                 /* tp_name */
	sizeof(Xparser),                   /* tp_basicsize */
	0,                                 /* tp_itemsize */
	(destructor)Xparser_dealloc,       /* tp_dealloc */
	0,                                 /* tp_print */
	0,                                 /* tp_getattr */
	0,                                 /* tp_setattr */
	0,                                 /* tp_compare */
	0,                                 /* tp_repr */
	0,                                 /* tp_as_number */
	0,                                 /* tp_as_sequence */
	0,                                 /* tp_as_mapping */
	0,                                 /* tp_hash  */
	0,                                 /* tp_call */
	0,                                 /* tp_str */
	0,                                 /* tp_getattro */
	0,                                 /* tp_setattro */
	0,                                 /* tp_as_buffer */
	MZ_BASE_TYPE_FLAGS,                /* tp_flags */
	"Mangoz Xparser",                  /* tp_doc */
	0,                                 /* tp_traverse */
	0,                                 /* tp_clear */
	0,                                 /* tp_richcompare */
	0,                                 /* tp_weaklistoffset */
	0,                                 /* tp_iter */
	0,                                 /* tp_iternext */
	Xparser_methods,                   /* tp_methods */
	0,                                 /* tp_members */
	0,                                 /* tp_getset */
	0,                                 /* tp_base */
	0,                                 /* tp_dict */
	0,                                 /* tp_descr_get */
	0,                                 /* tp_descr_set */
	0,                                 /* tp_dictoffset */
	(initproc)Xparser_init,            /* tp_init */
	0,                                 /* tp_alloc */
	Xparser_new,                       /* tp_new */
};

void mangoz_Xparser_init(PyObject *m)
{
	struct parser_instance {
		const char *name;
		mz_handle_format_t format;
	};
	static const struct parser_instance parsers[] = {
		{ "std_parser", mz_std_handle_format },
		{ "tree_parser", mz_tree_handle_format },
		{ "tb_parser", mz_tb_handle_format },
		{ NULL, NULL }
	};
	const struct parser_instance *parser;

	mangoz_XparserType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&mangoz_XparserType) < 0)
		return;

	Py_INCREF((PyObject *)&mangoz_XparserType);
	PyModule_AddObject(m, "Xparser", (PyObject *)&mangoz_XparserType);

	for (parser = parsers; parser->name != NULL; ++parser) {
		Xparser *p = (Xparser *)mangoz_XparserType.tp_alloc
			(&mangoz_XparserType, 0);
		mz_xparser_init(&p->xparser, parser->format, NULL);
		PyModule_AddObject(m, parser->name, (PyObject *)p);
		LOG_ALLOC("%p %s", p, parser->name);
	}
}

/* ----------------------------------------------------------------------------
 * private functions
 */

static mz_bool format(void *ctx, struct mz_xhtml *x, mz_bool do_format)
{
	PyObject *x_obj;
	PyObject *f_obj;
	PyObject *method_str;
	PyObject *ret;

	method_str = PyString_FromString("handleFormat");

	if (method_str == NULL)
		return do_format;

	x_obj = MZ_GET_PARENT(x, Xhtml_object, xhtml);
	Py_INCREF(x_obj);
	f_obj = mz2py_bool(do_format);
	ret = PyObject_CallMethodObjArgs(ctx, method_str, x_obj, f_obj, NULL);
	Py_DECREF(x_obj);
	Py_DECREF(f_obj);
	Py_DECREF(method_str);

	if ((ret == NULL) || !PyBool_Check(ret))
	    return do_format;

	do_format = (ret == Py_True) ? true : false;
	Py_DECREF(ret);

	return do_format;
}

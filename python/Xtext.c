/* Mangoz - python/Xtext.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include "mangoz-python.h"
#include "mangoz-python-util.h"
#include "mangoz-python-private.h"
#include <structmember.h>

#if 0 /* set to 1 to enable memory allocation logging */
# define LOG(msg, ...)							\
	fprintf(stderr, "[Xtext    %4i] "msg"\n", __LINE__, ##__VA_ARGS__)
# define LOG_ALLOC(msg, ...) LOG(msg, ##__VA_ARGS__)
#else
# define LOG_ALLOC(msg, ...)
#endif

unsigned long mangoz_Xtext_count = 0;

static void Xtext_dealloc(Xtext *self)
{
	--mangoz_Xtext_count;
	LOG_ALLOC("%p dealloc (%lu)", self->xtext, mangoz_Xtext_count);

	if (self->xtext.xnode.init)
		mz_xtext_free(&((Xtext *)self)->xtext);

	self->ob_type->tp_free(self);
}

static PyObject *Xtext_new(PyTypeObject *type, PyObject *args, PyObject *kw)
{
	Xtext *self;

	self = (Xtext *)type->tp_alloc(type, 0);

	if (self == NULL)
		return NULL;

	++mangoz_Xtext_count;
	MZ_CLEAR(self->xtext.xnode);

	LOG_ALLOC("%p new (%lu)", &self->xtext, mangoz_Xtext_count);

	return (PyObject *)self;
}

static int Xtext_init(PyObject *self, PyObject *args, PyObject *kw)
{
	char *text;
	int text_len;
	PyObject *do_format = NULL;

	if (!PyArg_ParseTuple(args, "s#|O", &text, &text_len, &do_format))
		return -1;

	LOG_ALLOC("%p init %s", &((Xtext *)self)->xtext, text);

	if (mz_xtext_init(&((Xtext *)self)->xtext, text, text_len,
			  py2mz_true(do_format)) < 0)
		return -1;

	return 0;
}

/* - methods - */

static PyObject *XtextObj_setText(Xtext *self, PyObject *args)
{
	PyObject *text_obj;
	PyObject *str_obj;
	char *text;
	size_t text_n;
	int stat;

	if (!PyArg_ParseTuple(args, "O", &text_obj))
		return NULL;

	str_obj = mz_py_unistrobj(text_obj, &text, &text_n);
	stat = mz_xtext_set(&self->xtext, text, text_n);
	Py_XDECREF(str_obj);

	if (stat < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyObject *XtextObj_getText(Xtext *self, PyObject *args)
{
	return mz2py_str(&self->xtext.text);
}

static PyObject *XtextObj_getSplitText(Xtext *self, PyObject *args)
{
	PyObject *list;
	struct mz_string *line;
	size_t i;

	list = PyList_New(self->xtext.split.n);

	if (list == NULL)
		return NULL;

	mz_vector_for_each_n(self->xtext.split, line, i) {
		if (PyList_SetItem(list, i, mz2py_str(line)) < 0)
			return NULL;
	}

	return list;
}

static PyObject *XtextObj_getNodeType(Xtext *self, PyObject *args)
{
	Py_INCREF(mangoz_XNODE_TEXT);

	return mangoz_XNODE_TEXT;
}

static PyObject *XtextObj_equals(Xtext *self, PyObject *args)
{
	Xtext *other;

	if (!PyArg_ParseTuple(args, "O!", &mangoz_XtextType, &other))
		return NULL;

	if (!mz_xtext_equals(&self->xtext, &other->xtext))
		Py_RETURN_FALSE;

	Py_RETURN_TRUE;
}

static PyObject *XtextObj_produce(Xtext *self, PyObject *args)
{
	PyObject *printer;

	if (!PyArg_ParseTuple(args, "O", &printer))
		return NULL;

	if (mz_xtext_do_produce(&self->xtext, printer, 0) < 0)
		return NULL;

	Py_RETURN_NONE;
}

static PyMethodDef Xtext_methods[] = {
	{ "setText", (PyCFunction)XtextObj_setText, METH_VARARGS,
	  "Set the text value." },
	{ "getText", (PyCFunction)XtextObj_getText, METH_VARARGS,
	  "Get the text value" },
	{ "getSplitText", (PyCFunction)XtextObj_getSplitText, METH_VARARGS,
	  "Get the text value split into a list of lines." },
	{ "getNodeType", (PyCFunction)XtextObj_getNodeType, METH_VARARGS,
	  "Get the node type" },
	{ "equals", (PyCFunction)XtextObj_equals, METH_VARARGS,
	  "Test if equals in value the given text node." },
	{ "produce", (PyCFunction)XtextObj_produce, METH_VARARGS,
	  "Produce the XHTML." },
	{ NULL }
};

static PyMemberDef Xtext_members[] = {
	{ NULL }
};

static PyGetSetDef Xtext_getsetters[] = {
	{ NULL }
};

PyTypeObject mangoz_XtextType = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /* ob_size */
	"_mangoz.Xtext",                   /* tp_name */
	sizeof(Xtext),                     /* tp_basicsize */
	0,                                 /* tp_itemsize */
	(destructor)Xtext_dealloc,         /* tp_dealloc */
	0,                                 /* tp_print */
	0,                                 /* tp_getattr */
	0,                                 /* tp_setattr */
	0,                                 /* tp_compare */
	0,                                 /* tp_repr */
	0,                                 /* tp_as_number */
	0,                                 /* tp_as_sequence */
	0,                                 /* tp_as_mapping */
	0,                                 /* tp_hash  */
	0,                                 /* tp_call */
	0,                                 /* tp_str */
	0,                                 /* tp_getattro */
	0,                                 /* tp_setattro */
	0,                                 /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT,                /* tp_flags */
	"Mangoz Xtext",                    /* tp_doc */
	0,                                 /* tp_traverse */
	0,                                 /* tp_clear */
	0,                                 /* tp_richcompare */
	0,                                 /* tp_weaklistoffset */
	0,                                 /* tp_iter */
	0,                                 /* tp_iternext */
	Xtext_methods,                     /* tp_methods */
	Xtext_members,                     /* tp_members */
	Xtext_getsetters,                  /* tp_getset */
	0,                                 /* tp_base */
	0,                                 /* tp_dict */
	0,                                 /* tp_descr_get */
	0,                                 /* tp_descr_set */
	0,                                 /* tp_dictoffset */
	Xtext_init,                        /* tp_init */
	0,                                 /* tp_alloc */
	Xtext_new,                         /* tp_new */
};

void mangoz_Xtext_init(PyObject *m)
{
	Py_INCREF((PyObject *)(&mangoz_XtextType));
	PyModule_AddObject(m, "Xtext", (PyObject *)&mangoz_XtextType);
}

struct mz_xtext *mangoz_py_create_xtext(const char *text, size_t len,
					mz_bool do_format)
{
	Xtext *self;

	self = (Xtext *)mangoz_XtextType.tp_alloc(&mangoz_XtextType, 0);

	if (self == NULL)
		return NULL;

	++mangoz_Xtext_count;
	LOG_ALLOC("%p create (%lu)", &((Xtext *)self)->xtext,
		  mangoz_Xtext_count);
	MZ_CLEAR(self->xtext.xnode);

	if (mz_xtext_init(&self->xtext, text, len, do_format) < 0) {
		Py_DECREF(self);
		return NULL;
	}

	return &self->xtext;
}

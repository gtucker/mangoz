/* Mangoz - libmz/std.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/libmz.h>

int mz_callback_error;

void *(*mz_malloc)(size_t size) = malloc;
void *(*mz_realloc)(void *ptr, size_t size) = realloc;
void (*mz_free)(void *ptr) = free;

const char *mz_strerror(enum mz_error error)
{
	static const char *error_strings[] = {
		"Memory allocation failed",     /* MZ_EALLOC */
		"Object not found",             /* MZ_ENOTFOUND */
		"Index out of bounds",          /* MZ_EINDEX */
		"Input/output error",           /* MZ_EIO */
		"Standard system error",        /* MZ_ESTD */
		"Callback error",               /* MZ_ECALLBACK */
		"XML handling error",           /* MZ_EXML */
	};
	static const int max_index = MZ_ARRAY_SZ(error_strings) - 1;
	int error_index = error - 1;

	if ((error_index < 0) || (error_index > max_index))
		return "Invalid error code";

	return error_strings[error_index];
}

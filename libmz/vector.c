/* Mangoz - libmz/vector.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>
#include <assert.h>
#include <string.h>

#define fixup_index(v, index, err_ret) do {		\
		if (index < 0)				\
			index += v->n;			\
		if ((index < 0) || (index >= v->n))	\
			return err_ret;			\
	} while (0)

static void clear_vector(struct mz_vector *v, size_t o_size);
static int grow_vector(struct mz_vector *v, size_t n);
static int resize_vector(struct mz_vector *v, size_t length);

int mz_vector_init(struct mz_vector *v, size_t o_size, size_t init_length)
{
	assert(v != NULL);
	assert(o_size);
	assert(init_length > 0);

	clear_vector(v, o_size);

	return resize_vector(v, init_length);
}

void mz_vector_free(struct mz_vector *v)
{
	assert(v != NULL);
	assert(v->data != NULL);

	mz_free(v->data);
	v->data = NULL;
}

void mz_vector_clear(struct mz_vector *v)
{
	assert(v != NULL);
	assert(v->data != NULL);

	v->n = 0;
	v->end = v->data;
}

int mz_vector_append(struct mz_vector *v, const void *o)
{
	size_t size;
	void *dest;

	assert(v != NULL);
	assert(v->data != NULL);
	assert(v->length > 0);
	assert(o != NULL);

	MZE(grow_vector(v, 1));
	size = v->object_size;
	dest = v->data + (v->n * size);

	if (size == sizeof (unsigned long))
		*(unsigned long *)dest = *(unsigned long *)o;
	else
		memcpy(dest, o, size);

	++v->n;
	v->end = v->data + (v->n * size);

	return 0;
}

int mz_vector_insert(struct mz_vector *v, const void *o, int index)
{
	size_t size;
	void *it;
	void *prev;
	void *insert;

	assert(v != NULL);
	assert(v->data != NULL);
	assert(v->length > 0);
	assert(o != NULL);

	fixup_index(v, index, -MZ_EINDEX);
	MZE(grow_vector(v, 1));
	size = v->object_size;
	insert = v->data + (index * size);

	for (it = v->data + (v->n * size), prev = it - size;
	     it != insert;
	     it = prev, prev -= size) {
		memcpy(it, prev, size);
	}

	memcpy(insert, o, size);
	++v->n;
	v->end = v->data + (v->n * size);

	return 0;
}

void *mz_vector_alloc_end(struct mz_vector *v, size_t n)
{
	void *p;

	assert(v != NULL);

	if (grow_vector(v, n) < 0)
	    return NULL;

	p = v->data + (v->n * v->object_size);
	v->n += n;
	v->end = v->data + (v->n * v->object_size);

	return p;
}

#if 0
void *mz_vector_alloc_at(struct mz_vector *v, size_t n, int index)
{
	assert(v != NULL);

	fixup_index(v, index, NULL);

	/* ToDo */

	return NULL;
}
#endif

int mz_vector_remove(struct mz_vector *v, int index)
{
	void *it;
	void *next;
	const void *end;
	size_t size;

	assert(v != NULL);

	fixup_index(v, index, -MZ_EINDEX);
	size = v->object_size;
	end = v->data + (v->n * size);

	for (it = v->data + (index * size), next = it + size;
	     next != end;
	     it = next, next += size) {
		memcpy(it, next, size);
	}

	--v->n;
	v->end -= size;

	return 0;
}

int mz_vector_remove_ptr(struct mz_vector *v, const void *ptr)
{
	size_t offset;
	size_t index;

	assert(v != NULL);

	if ((ptr < v->data) || (ptr >= v->end))
		return -MZ_EINDEX;

	offset = ptr - v->data;

	if (offset % v->object_size)
		return -MZ_EINDEX;

	index = offset / v->object_size;

	return mz_vector_remove(v, index);
}

void *mz_vector_get(const struct mz_vector *v, int index)
{
	assert(v != NULL);

	fixup_index(v, index, NULL);

	return v->data + (index * v->object_size);
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static void clear_vector(struct mz_vector *v, size_t o_size)
{
	const struct mz_vector v_init = {
		.data = NULL,
		.end = NULL,
		.n = 0,
		.length = 0,
		.object_size = o_size,
	};

	memcpy(v, &v_init, sizeof (struct mz_vector));
}

static int grow_vector(struct mz_vector *v, size_t n)
{
	size_t new_n = v->n + n;
	size_t new_length;

	if (new_n <= v->length)
		return 0;

	if (v->length == 1)
		new_length = 2;
	else if (v->length == 2)
		new_length = 4;
	else
		new_length = v->length + (v->length / 2);

	if (new_length < new_n)
		new_length = new_n;

	return resize_vector(v, new_length);
}

static int resize_vector(struct mz_vector *v, size_t length)
{
	size_t size;

	size = length * v->object_size;

	if (v->data != NULL)
		v->data = mz_realloc(v->data, size);
	else
		v->data = mz_malloc(size);

	if (v->data == NULL)
		return -MZ_EALLOC;

	v->length = length;
	v->end = v->data + (v->n * v->object_size);

	return 0;
}

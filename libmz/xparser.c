/* Mangoz - libmz/xparser.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>
#include <libxml/xmlreader.h>
#include <string.h>
#include <assert.h>

/* debug */
#include <stdio.h>
#define LOG(msg, ...) \
	fprintf(stderr, "[xparser  %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

static int do_parse(struct mz_xparser *xparser, struct mz_xdoc *xdoc,
		    xmlTextReaderPtr reader, mz_bool do_format);
static struct mz_xhtml *convert(struct mz_xparser *xparser,
				xmlTextReaderPtr reader, mz_bool root_format);
static int convert_next(struct mz_xparser *xparser, struct mz_xhtml *ele,
			xmlTextReaderPtr reader, mz_bool do_format);
static int parse_attributes(struct mz_xhtml *ele, xmlTextReaderPtr reader);

static long g_instances = 0;

int mz_xparser_init(struct mz_xparser *x, mz_handle_format_t format, void *ctx)
{
	assert(x != NULL);
	assert(x->init == false);

	x->format = format;
	x->ctx = ctx;

	if (!g_instances++)
		LIBXML_TEST_VERSION;

	x->init = true;

	return 0;
}

void mz_xparser_free(struct mz_xparser *x)
{
	assert(x != NULL);
	assert(x->init == true);

	if (!--g_instances)
		xmlCleanupParser();
}

mz_bool mz_std_handle_format(void *ctx, struct mz_xhtml *x, mz_bool do_format)
{
	const struct mz_string *xml_space;

#if 1 /* ToDo: fix XML namespace issue */
	xml_space = mz_attribute_get(&x->atts, MZ_STRLEN("space"));
#else
	xml_space = mz_attribute_get(&x->atts, MZ_STRLEN("xml:space"));
#endif

	if (xml_space == NULL)
		return do_format;

	if (!mz_string_cmp(xml_space, MZ_STRLEN("preserve")))
		return false;

	if (!mz_string_cmp(xml_space, MZ_STRLEN("default")))
		return true;

	return do_format;
}

mz_bool mz_tree_handle_format(void *ctx, struct mz_xhtml *x, mz_bool do_format)
{
	if (!mz_string_cmp(&x->tag, MZ_STRLEN("xhtml")))
		return false;

	return mz_std_handle_format(ctx, x, do_format);
}

mz_bool mz_tb_handle_format(void *ctx, struct mz_xhtml *x, mz_bool do_format)
{
	if (!mz_string_cmp(&x->tag, MZ_STRLEN("text")))
		return false;

	return mz_std_handle_format(ctx, x, do_format);
}

int mz_xparser_parse_file(struct mz_xparser *xparser, struct mz_xdoc *xdoc,
			  const char *file_name, mz_bool do_format)
{
	xmlTextReaderPtr reader;
	int ret;

	assert(xparser != NULL);
	assert(xparser->init);
	assert(xdoc != NULL);
	assert(file_name != NULL);

	reader = xmlReaderForFile(file_name, NULL, 0);

	if (reader == NULL)
		return -MZ_EXML;

	ret = do_parse(xparser, xdoc, reader, do_format);
	xmlFreeTextReader(reader);

	return ret;
}

int mz_xparser_parse_string(struct mz_xparser *xparser, struct mz_xdoc *xdoc,
			    const char *str, size_t len, mz_bool do_format)
{
	xmlTextReaderPtr reader;
	int ret;

	assert(xparser != NULL);
	assert(xparser->init);
	assert(xdoc != NULL);
	assert(str != NULL);

	reader = xmlReaderForDoc((const unsigned char *)str, NULL, NULL, 0);

	if (reader == NULL)
		return -MZ_EXML;

	ret = do_parse(xparser, xdoc, reader, do_format);
	xmlFreeTextReader(reader);

	return ret;
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static int do_parse(struct mz_xparser *xparser, struct mz_xdoc *xdoc,
		    xmlTextReaderPtr reader, mz_bool do_format)
{
	struct mz_xhtml *root;

	if (xmlTextReaderRead(reader) != 1)
		return -MZ_EXML;

	if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT)
		return -MZ_EXML;

	root = convert(xparser, reader, do_format);

	if (root == NULL)
		return -MZ_EXML;

	return mz_xdoc_init(xdoc, root);
}

static struct mz_xhtml *convert(struct mz_xparser *xparser,
				xmlTextReaderPtr reader, mz_bool root_format)
{
	struct mz_xhtml *ele;
	const char *tag;
	int ret;
	int is_empty;
	mz_bool do_format;

	tag = (const char *)xmlTextReaderConstName(reader);

	if (tag == NULL)
		return NULL;

	ele = mz_callbacks.create_xhtml((const char *)tag,
					strlen((const char *)tag),
					true, true);

	if (ele == NULL)
		return NULL;

	is_empty = xmlTextReaderIsEmptyElement(reader);

	if (xmlTextReaderHasAttributes(reader))
		if (parse_attributes(ele, reader) < 0)
			return NULL;

	do_format = xparser->format(xparser->ctx, ele, root_format);
	ele->xnode.do_format = do_format;

	if (is_empty)
		return ele;

	while ((ret = convert_next(xparser, ele, reader, do_format) == 1));

	if (ret < 0)
		return NULL;

	return ele;
}

static int convert_next(struct mz_xparser *xparser, struct mz_xhtml *ele,
			xmlTextReaderPtr reader, mz_bool do_format)
{
	int ret;
	int node_type;

	ret = xmlTextReaderRead(reader);

	if (ret == 0)
		return 0;

	if (ret != 1)
		return -MZ_EXML;

	node_type = xmlTextReaderNodeType(reader);

	switch (node_type) {
	case XML_READER_TYPE_SIGNIFICANT_WHITESPACE:
		if (do_format)
			break;
	case XML_READER_TYPE_TEXT: {
		const char *str;
		struct mz_xtext *xtext;
		xtext = mz_xhtml_add_text(ele, MZ_STRLEN(""), do_format);
		if (xtext == NULL)
			return -MZ_EXML;
		str = (const char *)xmlTextReaderConstValue(reader);
		ret = mz_xtext_set_html(xtext, str);
		if (ret < 0)
			return -MZ_EXML;
		break;
	}
	case XML_READER_TYPE_ELEMENT: {
		struct mz_xhtml *leaf = convert(xparser, reader, do_format);
		if (leaf == NULL)
			return -MZ_EXML;
		MZE(mz_xhtml_import_node(ele, &leaf->xnode));
		break;
	}
	case XML_READER_TYPE_END_ELEMENT: {
#ifndef NDEBUG
		const char *tag = (const char *)xmlTextReaderConstName(reader);
		if (strcmp(tag, ele->tag.str))
			assert(!"tag mismatch");
#endif
		return 0;
	}
	case XML_READER_TYPE_NONE:
	case XML_READER_TYPE_ATTRIBUTE:
	case XML_READER_TYPE_CDATA:
	case XML_READER_TYPE_ENTITY_REFERENCE:
	case XML_READER_TYPE_ENTITY:
	case XML_READER_TYPE_PROCESSING_INSTRUCTION:
	case XML_READER_TYPE_COMMENT:
	case XML_READER_TYPE_DOCUMENT:
	case XML_READER_TYPE_DOCUMENT_TYPE:
	case XML_READER_TYPE_DOCUMENT_FRAGMENT:
	case XML_READER_TYPE_NOTATION:
	case XML_READER_TYPE_WHITESPACE:
	case XML_READER_TYPE_END_ENTITY:
	case XML_READER_TYPE_XML_DECLARATION:
		break;
	default:
		assert(!"invalid node type");
		return -MZ_EXML;
	}

	return 1;
}

static int parse_attributes(struct mz_xhtml *ele, xmlTextReaderPtr reader)
{
	int n_atts = xmlTextReaderAttributeCount(reader);
	int i;

	for (i = 0; i < n_atts; ++i) {
		const char *name;
		const char *value;

		/* ToDo: MoveToFirstAttribute / MoveToNextAttribute */
		if (xmlTextReaderMoveToAttributeNo(reader, i) != 1)
			return -MZ_EXML;

		name = (const char *)xmlTextReaderLocalName(reader);
		value = (const char *)xmlTextReaderValue(reader);

		if ((name == NULL) || (value == NULL))
			return -MZ_EXML;

		if (mz_attribute_set(&ele->atts, name, strlen(name),
				     value, strlen(value)) < 0)
			return -MZ_EXML;
	}

	return 0;
}

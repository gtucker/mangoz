/* Mangoz - libmz/configuration.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>
#include <assert.h>

#define get_default(c, name, def) do {					\
		c->name = do_get_default(c->config, MZ_STRLEN(#name),	\
					 MZ_STRLEN(def));		\
	} while (0)

#define get_required(c, name) do {					\
		c->name = mz_attribute_get(&c->config->opt,		\
					   MZ_STRLEN(#name));		\
		if (c->name == NULL)					\
			return -1;					\
	} while (0)

#define get_option(c, name) do {					\
		c->name = mz_attribute_get(&c->config->opt,		\
					   MZ_STRLEN(#name));		\
	} while (0)

static int init_core_config(struct mz_core_config *c);
static int init_page_config(struct mz_page_config *c);
static const struct mz_string *do_get_default(
	struct mz_config *c, const char *key, size_t len,
	const char *def, size_t dlen);
static enum mz_dtd get_dtd_id(const struct mz_string *dtd_name);

int mz_config_init(struct mz_config *c)
{
	assert(c != NULL);
	assert(c->init == false);

	MZE(mz_vector_init(&c->opt, sizeof(struct mz_attribute), 12));
	c->core.config = c;
	c->core.valid = false;
	c->page.config = c;
	c->page.valid = false;
	c->page.css_list.data = NULL;
	c->init = true;

	return 0;
}

void mz_config_free(struct mz_config *c)
{
	assert(c != NULL);
	assert(c->init == true);

	mz_vector_free(&c->opt);

	if (c->page.css_list.data != NULL)
		mz_vector_free(&c->page.css_list);
}

const struct mz_core_config *mz_config_get_core(struct mz_config *c)
{
	assert(c != NULL);
	assert(c->init);

	if (!c->core.valid) {
		if (init_core_config(&c->core) < 0)
			return NULL;

		c->core.valid = true;
	}

	return &c->core;
}

const struct mz_page_config *mz_config_get_page(struct mz_config *c)
{
	assert(c != NULL);
	assert(c->init);

	if (!c->page.valid) {
		if (init_page_config(&c->page) < 0)
			return NULL;

		c->page.valid = true;
	}

	return &c->page;
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static int init_core_config(struct mz_core_config *c)
{
	get_default(c, dtd, "xhtml11");
	c->dtd_id = get_dtd_id(c->dtd);
	get_required(c, defaultLanguage);
	get_required(c, baseURL);
	get_required(c, staticDir);
	get_default(c, pages, "pages.xml");
	get_option(c, textbox);
	get_default(c, useSession, "true");
	c->do_use_session = mz_string_to_bool(c->useSession);

	return 0;
}

static int init_page_config(struct mz_page_config *c)
{
	get_required(c, defaultLanguage);
	get_default(c, charset, "UTF-8");
	get_option(c, defaultIcon);
	get_option(c, baseURL);
	get_default(c, contentType, "text/html");
	get_option(c, defaultCSS);

	if (c->defaultCSS == NULL)
		get_option(c, publicURL);
	else
		get_required(c, publicURL);

	if (c->css_list.data == NULL)
		MZE(mz_vector_init(&c->css_list, sizeof(struct mz_string), 1));

	if (c->defaultCSS == NULL)
		return 0;

	return mz_string_split(c->defaultCSS, &c->css_list, ' ');
}

static const struct mz_string *do_get_default(struct mz_config *c,
					      const char *key, size_t len,
					      const char *def, size_t dlen)
{
	const struct mz_string *value;

	value = mz_attribute_get(&c->opt, key, len);

	if (value != NULL)
		return value;

	if (mz_attribute_set(&c->opt, key, len, def, dlen) < 0)
		return NULL;

	return mz_attribute_get(&c->opt, key, len);
}

static enum mz_dtd get_dtd_id(const struct mz_string *dtd_name)
{
	struct mz_dtd_id {
		enum mz_dtd id;
		const char *name;
		size_t len;
	};
	static const struct mz_dtd_id id_table[] = {
		{ MZ_DTD_XHTML11,       MZ_STRLEN("xhtml11") },
		{ MZ_DTD_STRICT,        MZ_STRLEN("strict")  },
		{ MZ_DTD_TRANSITIONAL,  MZ_STRLEN("transitional") },
		{ MZ_DTD_FRAMESET,      MZ_STRLEN("frameset") },
		{ MZ_DTD_HTML,          MZ_STRLEN("html") },
		{ 0, NULL, 0 },
	};
	const struct mz_dtd_id *it;

	for (it = id_table; it->id; ++it) {
		if (!mz_string_cmp(dtd_name, it->name, it->len))
			return it->id;
	}

	return 0;
}

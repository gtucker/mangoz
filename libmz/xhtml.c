/* Mangoz - libmz/xhtml.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>
#include <mangoz/libmz/private.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define LOG(msg, ...) \
	fprintf(stderr, "[xhtml    %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

static void cleanup(struct mz_xhtml *x);
static struct mz_xtext *add_text(struct mz_xhtml *x, const char *str,
				 size_t len, mz_bool do_format);

int mz_xhtml_init(struct mz_xhtml *x, const char *tag, size_t tag_len,
		  mz_bool do_format, mz_bool do_short)
{
	assert(x != NULL);
	assert(x->xnode.init == false);
	assert(tag != NULL);
	assert(tag_len != 0);

#if MZ_LOG_ALLOC
	LOG("%p init (%s)", &x->xnode, tag);
#endif

	x->tag.str = NULL;
	x->atts.data = NULL;
	x->nodes.data = NULL;
	x->out.str = NULL;
	x->do_short = do_short;

	MZX(x, mz_xnode_init(&x->xnode, MZ_XNODE_HTML, do_format));
	MZX(x, mz_string_init(&x->tag, tag_len));
	MZX(x, mz_string_set(&x->tag, tag, tag_len));
	MZX(x, mz_vector_init(&x->atts, sizeof(struct mz_attribute), 4));
	MZX(x, mz_vector_init(&x->nodes, sizeof(struct mz_xnode *), 8));

	return 0;
}

void mz_xhtml_free(struct mz_xhtml *x)
{
	assert(x != NULL);
	assert(x->xnode.init == true);

#if MZ_LOG_ALLOC
	LOG("%p free", &x->xnode);
#endif
	cleanup(x);
}

struct mz_xtext *mz_xhtml_add_text(struct mz_xhtml *x, const char *str,
				   size_t len, mz_bool do_format)
{
	assert(x != NULL);
	assert(x->xnode.init);
	assert(str != NULL);

	return add_text(x, str, len, do_format);
}

int mz_xhtml_set_text(struct mz_xhtml *x, const char *str, size_t len)
{
	struct mz_xnode **node;

	assert(x != NULL);
	assert(x->xnode.init);

	mz_vector_for_each(x->nodes, node) {
		if ((*node)->node_type == MZ_XNODE_TEXT) {
			struct mz_xtext *xtext;

			xtext = MZ_GET_PARENT(*node, mz_xtext, xnode);

			return mz_xtext_set(xtext, str, len);
		}
	}

	if (add_text(x, str, len, x->xnode.do_format) == NULL)
		return -MZ_EALLOC;

	return 0;
}

struct mz_xtext *mz_xhtml_get_text_node(struct mz_xhtml *x, unsigned i)
{
	struct mz_xnode **node;

	assert(x != NULL);
	assert(x->xnode.init);

	mz_vector_for_each(x->nodes, node)
		if (((*node)->node_type == MZ_XNODE_TEXT) && !i--)
			return MZ_GET_PARENT(*node, mz_xtext, xnode);

	return NULL;
}

struct mz_xhtml *mz_xhtml_add_leaf(struct mz_xhtml *x, const char *tag,
				   size_t tag_len, const char *text,
				   size_t text_len, mz_bool do_format,
				   mz_bool do_short)
{
	struct mz_xhtml *xhtml;
	struct mz_xnode *xnode;

	assert(x != NULL);
	assert(x->xnode.init);

	xhtml = mz_callbacks.create_xhtml(tag, tag_len, do_format, do_short);

	if (xhtml == NULL)
		return NULL;

	if (text != NULL)
		if (add_text(xhtml, text, text_len, do_format) < 0)
			goto bail_out;

#if MZ_LOG_ALLOC
	LOG("%p add_leaf %p", &x->xnode, &xhtml->xnode);
#endif

	xnode = &xhtml->xnode;

	if (mz_vector_append(&x->nodes, &xnode) < 0)
		goto bail_out;

	return xhtml;

bail_out:
	cleanup(xhtml);

	return NULL;
}

struct mz_xhtml *mz_xhtml_get_indexed_leaf(struct mz_xhtml *x, const char *tag,
					   size_t tag_len, size_t index)
{
	struct mz_xnode **node;

	assert(x != NULL);
	assert(x->xnode.init);

	mz_vector_for_each(x->nodes, node) {
		struct mz_xhtml *leaf;

		if ((*node)->node_type != MZ_XNODE_HTML)
			continue;

		leaf = MZ_GET_PARENT(*node, mz_xhtml, xnode);

		if (mz_string_cmp(&leaf->tag, tag, tag_len))
			continue;

		if (index--)
			continue;

		return leaf;
	}

	return NULL;
}

struct mz_xhtml *mz_xhtml_get_leaf(struct mz_xhtml *x,
				   const char *tag, size_t tag_len,
				   const char *att_name, size_t att_name_len,
				   const char *att_value, size_t att_value_len)
{
	struct mz_xnode **node;

	assert(x != NULL);
	assert(x->xnode.init);
	assert(tag != NULL);

	mz_vector_for_each(x->nodes, node) {
		const struct mz_string *found_att;
		struct mz_xhtml *x;

		if ((*node)->node_type != MZ_XNODE_HTML)
			continue;

		x = MZ_GET_PARENT(*node, mz_xhtml, xnode);

		if (mz_string_cmp(&x->tag, tag, tag_len))
			continue;

		if (att_name == NULL)
			return x;

		found_att = mz_attribute_get(&x->atts, att_name, att_name_len);

		if (found_att == NULL) {
			if (att_value == NULL)
				return x;
			continue;
		}

		if (!mz_string_cmp(found_att, att_value, att_value_len))
			return x;
	}

	return NULL;
}

int mz_xhtml_import_node(struct mz_xhtml *x, struct mz_xnode *node)
{
	assert(x != NULL);
	assert(x->xnode.init);
	assert(node != NULL);
	assert(node->init);

#if MZ_LOG_ALLOC
	LOG("%p import_node %p", &x->xnode, node);
#endif

	return mz_vector_append(&x->nodes, &node);
}

int mz_xhtml_remove_node(struct mz_xhtml *x, struct mz_xnode *node)
{
	struct mz_xnode **it;
	size_t index;

	assert(x != NULL);
	assert(node != NULL);
	assert(x->xnode.init);

	index = -1;

	mz_vector_for_each(x->nodes, it) {
		++index;

		if (*it == node)
			break;
	}

	if (index < 0)
		return -MZ_ENOTFOUND;

	return mz_vector_remove(&x->nodes, index);
}

unsigned long mz_xhtml_count_nodes(struct mz_xhtml *x,
				   enum mz_xnode_type node_type)
{
	unsigned long count = 0;
	struct mz_xnode **node;

	assert(x != NULL);
	assert(x->xnode.init);

	mz_vector_for_each(x->nodes, node)
		if ((*node)->node_type == node_type)
			++count;

	return count;
}

mz_bool mz_xhtml_equals(struct mz_xhtml *x, struct mz_xhtml *y)
{
	struct mz_attribute *att;
	struct mz_xnode * const *a;
	struct mz_xnode * const *b;
	int n;
	int i;

	assert(x != NULL);
	assert(y != NULL);
	assert(x->xnode.init);
	assert(y->xnode.init);

	if (mz_string_cmp_str(&x->tag, &y->tag))
		return false;

	if (x->atts.n != y->atts.n)
		return false;

	mz_vector_for_each(x->atts, att) {
		const struct mz_string *found;

		found = mz_attribute_get(&y->atts, att->name.str,
					 att->name.len);

		if (found == NULL)
			return false;

		if (mz_string_cmp_str(&att->value, found))
			return false;
	}

	n = x->nodes.n;

	if (n != y->nodes.n)
		return false;

	for (i = 0, a = x->nodes.data, b = y->nodes.data; i < n; ++i,++a,++b) {
		const enum mz_xnode_type node_type = (*a)->node_type;

		if (node_type != (*b)->node_type)
			return false;

		switch (node_type) {
		case MZ_XNODE_TEXT:
			if (!mz_xtext_equals(
				    MZ_GET_PARENT(*a, mz_xtext, xnode),
				    MZ_GET_PARENT(*b, mz_xtext, xnode)))
				return 0;
			break;
		case MZ_XNODE_HTML:
			if (!mz_xhtml_equals(
				    MZ_GET_PARENT(*a, mz_xhtml, xnode),
				    MZ_GET_PARENT(*b, mz_xhtml, xnode)))
				return 0;
			break;
		default:
			assert(!"invalid node type");
			return false;
		}
	}

	return true;
}

int mz_xhtml_do_produce(struct mz_xhtml *x, void *printer, unsigned indent,
			mz_bool root_format)
{
	int (* const produce)(void *printer, const char *str, size_t len) =
		mz_callbacks.printer_produce;
	struct mz_attribute *att;
	const struct mz_attribute *att_end;
	struct mz_xnode **node;
	struct mz_xnode *first;
	const unsigned next_indent = indent + 1;
	struct mz_string *out;
	struct mz_string indent_str;
	mz_bool top_format;

	assert(x != NULL);
	assert(x->xnode.init);

	att_end = x->atts.end;
	first = *((struct mz_xnode **)x->nodes.data);
	out = &x->out;
	top_format = (x->xnode.do_format || root_format) ? true : false;

	if (out->str == NULL)
		MZE(mz_string_init(out, 0));
	else
		mz_string_clear(out);

	if (x->xnode.do_format)
		MZE(mz_make_indent_str(&indent_str, indent));
	else
		indent_str.str = NULL;

	if (x->xnode.do_format)
		MZE(mz_string_cat_str(out, &indent_str));

	MZE(mz_string_cat(out, MZ_STRLEN("<")));
	MZE(mz_string_cat_str(out, &x->tag));

	for (att = x->atts.data; att != att_end; ++att) {
		MZE(mz_string_cat(out, MZ_STRLEN(" ")));
		MZE(mz_string_cat_str(out, &att->name));
		MZE(mz_string_cat(out, MZ_STRLEN("=\"")));
		MZE(mz_string_cat_html(out, att->value.str));
		MZE(mz_string_cat(out, MZ_STRLEN("\"")));
	}

	if (x->do_short && (x->nodes.n == 0)) {
		if (x->xnode.do_format)
			MZE(mz_string_cat(out, MZ_STRLEN(" />\n")));
		else
			MZE(mz_string_cat(out, MZ_STRLEN(" />")));

		MZE(produce(printer, out->str, out->len));

		return 0;
	}

	if ((x->nodes.n == 1) && (first->node_type == MZ_XNODE_TEXT)) {
		struct mz_xtext *xtext = MZ_GET_PARENT(first, mz_xtext, xnode);

		MZE(mz_string_cat(out, MZ_STRLEN(">")));
		MZE(produce(printer, out->str, out->len));
		MZE(mz_xtext_do_produce_single(xtext, printer, next_indent));
		mz_string_clear(out);
		MZE(mz_string_cat(out, MZ_STRLEN("</")));
		MZE(mz_string_cat_str(out, &x->tag));

		if (top_format)
			MZE(mz_string_cat(out, MZ_STRLEN(">\n")));
		else
			MZE(mz_string_cat(out, MZ_STRLEN(">")));

		MZE(produce(printer, out->str, out->len));

		return 0;
	}

	if (x->xnode.do_format)
		MZE(mz_string_cat(out, MZ_STRLEN(">\n")));
	else
		MZE(mz_string_cat(out, MZ_STRLEN(">")));

	MZE(produce(printer, out->str, out->len));
	mz_string_clear(out);

	mz_vector_for_each(x->nodes, node) {
		switch ((*node)->node_type) {
		case MZ_XNODE_TEXT:
			MZE(mz_xtext_do_produce(
				    MZ_GET_PARENT(*node, mz_xtext, xnode),
				    printer, next_indent));
			break;
		case MZ_XNODE_HTML:
			MZE(mz_xhtml_do_produce(
				    MZ_GET_PARENT(*node, mz_xhtml, xnode),
				    printer, next_indent, x->xnode.do_format));
			break;
		default:
			assert(!"invalid node type");
			return -1;
		}
	}

	if (x->xnode.do_format)
		MZE(mz_string_cat_str(out, &indent_str));

	MZE(mz_string_cat(out, MZ_STRLEN("</")));
	MZE(mz_string_cat_str(out, &x->tag));

	if (top_format)
		MZE(mz_string_cat(out, MZ_STRLEN(">\n")));
	else
		MZE(mz_string_cat(out, MZ_STRLEN(">")));

	MZE(produce(printer, out->str, out->len));

	if (indent_str.str != NULL)
		mz_string_free(&indent_str);

	return 0;
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static void cleanup(struct mz_xhtml *x)
{
	if (x->tag.str != NULL)
		mz_string_free(&x->tag);

	if (x->atts.data != NULL)
		mz_vector_free(&x->atts);

	if (x->nodes.data != NULL)
		mz_vector_free(&x->nodes);

	if (x->out.str != NULL)
		mz_string_free(&x->out);

	if (x->xnode.init)
		mz_xnode_free(&x->xnode);
}

static struct mz_xtext *add_text(struct mz_xhtml *x, const char *str,
				 size_t len, mz_bool do_format)
{
	struct mz_xtext *xtext;
	struct mz_xnode *xnode;

	xtext = mz_callbacks.create_xtext(str, len, do_format);

	if (xtext == NULL)
		return NULL;

#if MZ_LOG_ALLOC
	LOG("%p add_text %p", &x->xnode, &xtext->xnode);
#endif

	xnode = &xtext->xnode;

	if (mz_vector_append(&x->nodes, &xnode) < 0)
		return NULL;

	return xtext;
}

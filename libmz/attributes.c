/* Mangoz - libmz/attributes.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>
#include <assert.h>

static int add_attribute(struct mz_vector *atts, const char *name, size_t nlen,
			 const char *value, size_t vlen);
static int del_attribute(struct mz_vector *atts, const char *name,
			 size_t nlen);
static struct mz_attribute *get_attribute(
	const struct mz_vector *atts, const char *name, size_t nlen);

const struct mz_string *mz_attribute_get(const struct mz_vector *atts,
					 const char *name, size_t nlen)
{
	struct mz_attribute *att;

	assert(atts != NULL);
	assert(name != NULL);

	att = get_attribute(atts, name, nlen);

	if (att == NULL)
		return NULL;

	return &att->value;
}

int mz_attribute_set(struct mz_vector *atts, const char *name,
		     size_t nlen, const char *value, size_t vlen)
{
	struct mz_attribute *att;

	assert(atts != NULL);

	if (value == NULL)
		return del_attribute(atts, name, nlen);

	att = get_attribute(atts, name, nlen);

	if (att == NULL)
		return add_attribute(atts, name, nlen, value, vlen);

	return mz_string_set_nolen(&att->value, value);
}

int mz_attribute_set_default(struct mz_vector *atts, const char *name,
			     size_t nlen, const char *value, size_t vlen)
{
	struct mz_attribute *att;

	assert(atts != NULL);
	assert(name != NULL);
	assert(value != NULL);

	att = get_attribute(atts, name, nlen);

	if (att != NULL)
		return 0;

	return add_attribute(atts, name, nlen, value, vlen);
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static int add_attribute(struct mz_vector *atts, const char *name, size_t nlen,
			 const char *value, size_t vlen)
{
	struct mz_attribute *att;

	att = mz_vector_alloc_end(atts, 1);

	if (att == NULL)
		return -MZ_EALLOC;

	MZE(mz_string_init(&att->name, nlen));
	MZE(mz_string_set(&att->name, name, nlen));
	MZE(mz_string_init(&att->value, vlen));
	MZE(mz_string_set(&att->value, value, vlen));

	return 0;
}

static int del_attribute(struct mz_vector *atts, const char *name, size_t nlen)
{
	struct mz_attribute *att;

	att = get_attribute(atts, name, nlen);

	if (att == NULL)
		return -MZ_ENOTFOUND;

	mz_string_free(&att->name);
	mz_string_free(&att->value);

	return mz_vector_remove_ptr(atts, att);
}

static struct mz_attribute *get_attribute(
	const struct mz_vector *atts, const char *name, size_t nlen)
{
	struct mz_attribute *att;

	mz_vector_for_each_p(atts, att)
		if (!mz_string_cmp(&att->name, name, nlen))
			return att;

	return NULL;
}

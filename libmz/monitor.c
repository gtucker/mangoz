/* Mangoz - libmz/monitor.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/private.h>
#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>
#include <sys/inotify.h>
#include <sys/select.h>
#include <pthread.h>
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#define LOG(msg, ...) \
	fprintf(stderr, "[monitor  %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

static const uint8_t MONITOR_MSG_STOP = 1;
static const uint8_t MONITOR_MSG_RUNNING = 2;

struct mz_monitor_watch {
	struct mz_string file_name;
	void *file_ctx;
	int wd;
};

struct mz_monitor {
	int fd;
	int pipe[2];
	mz_bool running;
	struct mz_vector files; /* mz_monitor_watch */
	mz_monitor_callback_t callback;
	void *ctx;
	pthread_t thread;
	pthread_mutex_t mutex;
	int std_error;
};

static struct mz_monitor_watch *find_file(struct mz_monitor *m,
					  const char *file_name,
					  size_t len);
static struct mz_monitor_watch *find_watch(struct mz_monitor *m, int wd);
static int cancel_monitor(struct mz_monitor *m);
static void *monitor(void *ctx);
static int handle_message(struct mz_monitor *m);
static int handle_inotify(struct mz_monitor *m);
static void report_error(struct mz_monitor *m);

int mz_monitor_init(struct mz_monitor **p, mz_monitor_callback_t cb, void *ctx)
{
	struct mz_monitor *m;
	int stat;

	assert(p != NULL);
	assert(cb != NULL);

	m = mz_malloc(sizeof(struct mz_monitor));

	if (m == NULL) {
		stat = -MZ_EALLOC;
		goto err_exit;
	}

	stat = mz_vector_init(&m->files, sizeof(struct mz_monitor_watch), 4);

	if (stat)
		goto err_free_m;

	stat = pthread_mutex_init(&m->mutex, NULL);

	if (stat) {
		m->std_error = stat;
		stat = -MZ_ESTD;
		goto err_free_vector;
	}

	m->fd = inotify_init();

	if (m->fd < 0) {
		m->std_error = errno;
		stat = -MZ_ESTD;
		goto err_destroy_pthread;
	}

	stat = pipe(m->pipe);

	if (stat) {
		m->std_error = errno;
		stat = -MZ_ESTD;
		goto err_close_inotify;
	}

	m->callback = cb;
	m->ctx = ctx;
	m->running = false;
	*p = m;

	return 0;

err_close_inotify:
	close(m->fd);
err_destroy_pthread:
	pthread_mutex_destroy(&m->mutex);
err_free_vector:
	mz_vector_free(&m->files);
err_free_m:
	mz_free(m);
err_exit:

	return stat;
}

int mz_monitor_get_errno(const struct mz_monitor *m)
{
	assert(m != NULL);

	return m->std_error;
}

void mz_monitor_free(struct mz_monitor *m)
{
	assert(m != NULL);

	pthread_mutex_lock(&m->mutex);

	if (m->running)
		cancel_monitor(m);

	pthread_mutex_unlock(&m->mutex);

	close(m->fd);
	close(m->pipe[0]);
	close(m->pipe[1]);
	pthread_mutex_destroy(&m->mutex);
	mz_vector_free(&m->files);
	mz_free(m);
}

int mz_monitor_add_file(struct mz_monitor *m, const char *file_name,
			size_t len, void *file_ctx)
{
	struct mz_monitor_watch *found;
	struct mz_monitor_watch *new_file;
	int stat;

	assert(m != NULL);

	pthread_mutex_lock(&m->mutex);

	found = find_file(m, file_name, len);

	if (found != NULL) {
		LOG("file %s already monitored", file_name);
		stat = -MZ_ENOTFOUND;
		goto exit_unlock;
	}

	new_file = mz_vector_alloc_end(&m->files, 1);

	if (new_file == NULL) {
		stat = -MZ_EALLOC;
		goto exit_unlock;
	}

	stat = mz_string_init(&new_file->file_name, 0);

	if (stat)
		goto exit_unlock;

	stat = mz_string_set(&new_file->file_name, file_name, len);

	if (stat)
		goto exit_unlock;

	new_file->wd = inotify_add_watch(m->fd, file_name, IN_CLOSE_WRITE);

	if (new_file->wd < 0) {
		LOG("inotify_add_watch: %s", strerror(errno));
		m->std_error = errno;
		stat = -MZ_ESTD;
		goto exit_unlock;
	}

	new_file->file_ctx = file_ctx;

exit_unlock:
	pthread_mutex_unlock(&m->mutex);

	return stat;
}

int mz_monitor_remove_file(struct mz_monitor *m, const char *file_name,
			   size_t len)
{
	struct mz_monitor_watch *found;
	int stat = 0;

	assert(m != NULL);

	pthread_mutex_lock(&m->mutex);

	found = find_file(m, file_name, len);

	if (found == NULL) {
		LOG("file not monitored: %s", file_name);
		stat = -MZ_ENOTFOUND;
		goto exit_unlock;
	}

	stat = mz_vector_remove_ptr(&m->files, found);

exit_unlock:
	pthread_mutex_unlock(&m->mutex);

	return stat;
}

int mz_monitor_run(struct mz_monitor *m)
{
	int stat = 0;

	assert(m != NULL);

	pthread_mutex_lock(&m->mutex);

	assert(!m->running);

	stat = pthread_create(&m->thread, NULL, monitor, m);

	if (stat) {
		m->std_error = stat;
		stat = -MZ_ESTD;
		goto err_unlock;
	}

	m->running = true;

	pthread_mutex_unlock(&m->mutex);

	if (write(m->pipe[1], &MONITOR_MSG_RUNNING, 1) != 1) {
		LOG("failed to write to pipe");
		return -MZ_EIO;
	}

	return 0;

err_unlock:
	pthread_mutex_unlock(&m->mutex);

	return stat;
}

int mz_monitor_cancel(struct mz_monitor *m)
{
	int stat = 0;

	assert(m != NULL);

	pthread_mutex_lock(&m->mutex);

	if (!m->running)
		goto exit_unlock;

	stat = cancel_monitor(m);

exit_unlock:
	pthread_mutex_unlock(&m->mutex);

	return stat;
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static struct mz_monitor_watch *
find_file(struct mz_monitor *m, const char *file_name, size_t len)
{
	struct mz_monitor_watch *it;

	mz_vector_for_each(m->files, it)
		if (!mz_string_cmp(&it->file_name, file_name, len))
			return it;

	return NULL;
}

static struct mz_monitor_watch *
find_watch(struct mz_monitor *m, int wd)
{
	struct mz_monitor_watch *it;

	mz_vector_for_each(m->files, it)
		if (it->wd == wd)
			return it;

	return NULL;
}

static int cancel_monitor(struct mz_monitor *m)
{
	void *status;
	int stat;

	if (write(m->pipe[1], &MONITOR_MSG_STOP, 1) != 1) {
		LOG("failed to write to pipe");
		return -MZ_EIO;
	}

	stat = pthread_join(m->thread, &status);

	if (stat) {
		m->std_error = stat;
		stat = -MZ_ESTD;
	}

	m->running = false;

	return stat;
}

/* ToDo: report errors using callback */
static void *monitor(void *ctx)
{
	struct mz_monitor *m = ctx;
	struct mz_monitor_event mz_ev;
	int stat = 0;
	fd_set set;

	mz_ev.ctx = m->ctx;

	FD_ZERO(&set);

	for (;;) {
		FD_SET(m->fd, &set);
		FD_SET(m->pipe[0], &set);

		do {
			stat = select(FD_SETSIZE, &set, NULL, NULL, NULL);
		} while ((stat < 0) && (errno == -EINTR));

		if (stat < 0) {
			report_error(m);
			break;
		}

		if (FD_ISSET(m->pipe[0], &set))
			stat = handle_message(m);
		else if (FD_ISSET(m->fd, &set))
			stat = handle_inotify(m);
		else
			continue;

		if (stat < 0) {
			report_error(m);
			break;
		} else if (stat == 1) {
			break;
		}
	}

	pthread_exit(NULL);
}

static int handle_message(struct mz_monitor *m)
{
	struct mz_monitor_event mz_ev;
	uint8_t msg;
	int stat;

	if (read(m->pipe[0], &msg, 1) != 1) {
		LOG("failed to read message");
		return -MZ_EIO;
	}

	if (msg == MONITOR_MSG_RUNNING) {
		mz_ev.msg = MZ_MON_RUNNING;
		mz_ev.ctx = m->ctx;
		mz_ev.file_name = NULL;
		mz_ev.file_ctx = NULL;
		stat = m->callback(&mz_ev);
	} else if (msg == MONITOR_MSG_STOP) {
		mz_ev.msg = MZ_MON_STOPPED;
		mz_ev.ctx = m->ctx;
		mz_ev.file_name = NULL;
		mz_ev.file_ctx = NULL;
		stat = m->callback(&mz_ev) ? -1 : 1;
	} else {
		assert("unexpected message");
		stat = -MZ_EIO;
	}

	return stat;
}

static int handle_inotify(struct mz_monitor *m)
{
	struct mz_monitor_watch *watch;
	struct mz_monitor_event mz_ev;
	struct inotify_event ev;
	ssize_t nread;

	nread = read(m->fd, &ev, sizeof ev);

	if (nread != sizeof ev) {
		LOG("read error");
		return -MZ_EIO;
	}

	watch = find_watch(m, ev.wd);

	if (watch == NULL) {
		LOG("wd not found");
		return -MZ_ENOTFOUND;
	}

	mz_ev.msg = MZ_MON_FILE;
	mz_ev.ctx = m->ctx;
	mz_ev.file_name = &watch->file_name;
	mz_ev.file_ctx = watch->file_ctx;

	return m->callback(&mz_ev);
}

static void report_error(struct mz_monitor *m)
{
	struct mz_monitor_event mz_ev;

	mz_ev.msg = MZ_MON_ERROR;
	mz_ev.file_name = NULL;
	mz_ev.file_ctx = NULL;
	(void) m->callback(&mz_ev);
}

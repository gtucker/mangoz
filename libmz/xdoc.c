/* Mangoz - libmz/xdoc.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>
#include <assert.h>

#define DOCTYPE_XHTML11						\
	"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" "	\
	"\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">"

#define DOCTYPE_STRICT							\
	"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" "	\
	"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"

#define DOCTYPE_TRANSITIONAL						\
	"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" " \
	"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"

#define DOCTYPE_FRAMESET						\
	"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Frameset//EN\" "	\
	"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd\">"

#define DOCTYPE_HTML				\
	"<!DOCTYPE html>"

static const struct mz_doctype doctypes[] = {
	{
		.dtd = MZ_DTD_XHTML11,
		.header = DOCTYPE_XHTML11,
		.len = MZ_CSTRLEN(DOCTYPE_XHTML11),
	},
	{
		.dtd = MZ_DTD_STRICT,
		.header = DOCTYPE_STRICT,
		.len = MZ_CSTRLEN(DOCTYPE_STRICT),
	},
	{
		.dtd = MZ_DTD_TRANSITIONAL,
		.header = DOCTYPE_TRANSITIONAL,
		.len = MZ_CSTRLEN(DOCTYPE_TRANSITIONAL),
	},
	{
		.dtd = MZ_DTD_FRAMESET,
		.header = DOCTYPE_FRAMESET,
		.len = MZ_CSTRLEN(DOCTYPE_FRAMESET),
	},
	{
		.dtd = MZ_DTD_HTML,
		.header = DOCTYPE_HTML,
		.len = MZ_CSTRLEN(DOCTYPE_HTML),
	}
};

static void cleanup(struct mz_xdoc *x);
static const struct mz_doctype *get_std_doctype(enum mz_dtd dtd);

int mz_xdoc_init(struct mz_xdoc *x, struct mz_xhtml *root)
{
	assert(x != NULL);
	assert(x->init == false);
	assert(root != NULL);

	x->atts.data = NULL;
	x->header.str = NULL;
	x->doctype = NULL;
	x->root = root;
	MZX(x, mz_vector_init(&x->atts, sizeof(struct mz_attribute), 2));
	MZX(x, mz_string_init(&x->header, 0));
	x->init = true;

	return 0;
}

void mz_xdoc_free(struct mz_xdoc *x)
{
	assert(x != NULL);
	assert(x->init == true);

	cleanup(x);
}

int mz_xdoc_set_std_doctype(struct mz_xdoc *x, enum mz_dtd dtd)
{
	assert(x != NULL);
	assert(x->init);

	x->doctype = get_std_doctype(dtd);

	return mz_string_set(&x->header, x->doctype->header, x->doctype->len);
}

const struct mz_doctype *mz_xdoc_get_std_doctype(enum mz_dtd dtd)
{
	return get_std_doctype(dtd);
}

int mz_xdoc_set_header(struct mz_xdoc *x, const char *hdr, size_t len)
{
	assert(x != NULL);
	assert(x->init);
	assert(hdr != NULL);

	x->doctype = NULL;

	return mz_string_set(&x->header, hdr, len);
}

int mz_xdoc_do_produce(struct mz_xdoc *x, void *printer, int indent)
{
	assert(x != NULL);
	assert(x->init);

	if (x->header.len) {
		MZCB(mz_callbacks.printer_produce(printer, x->header.str,
						  x->header.len));
		MZCB(mz_callbacks.printer_produce(printer, MZ_STRLEN("\n")));
	} else {
		struct mz_string out;
		const struct mz_attribute *att;

		MZE(mz_string_init(&out, 0));
		MZE(mz_string_set(&out, MZ_STRLEN("<?xml")));
		MZE(mz_attribute_set_default(&x->atts, MZ_STRLEN("version"),
					     MZ_STRLEN("1.0")));
		MZE(mz_attribute_set_default(&x->atts, MZ_STRLEN("encoding"),
					     MZ_STRLEN("UTF-8")));

		mz_vector_for_each(x->atts, att) {
			MZE(mz_string_cat(&out, MZ_STRLEN(" ")));
			MZE(mz_string_cat_str(&out, &att->name));
			MZE(mz_string_cat(&out, MZ_STRLEN("=\"")));
			MZE(mz_string_cat_str(&out, &att->value));
			MZE(mz_string_cat(&out, MZ_STRLEN("\"")));
		}

		MZE(mz_string_cat(&out, MZ_STRLEN("?>\n")));
		MZCB(mz_callbacks.printer_produce(printer, out.str, out.len));
		mz_string_free(&out);
	}

	return mz_xhtml_do_produce(x->root, printer, indent, true);
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static void cleanup(struct mz_xdoc *x)
{
	if (x->atts.data != NULL)
		mz_vector_free(&x->atts);

	if (x->header.str != NULL)
		mz_string_free(&x->header);

	x->init = false;
}

static const struct mz_doctype *get_std_doctype(enum mz_dtd dtd)
{
	const struct mz_doctype *it;
	const struct mz_doctype *end;

	end = &doctypes[MZ_ARRAY_SZ(doctypes) - 1];

	for (it = doctypes; it != end; ++it)
		if (it->dtd == dtd)
			break;

	return it;
}

/* Mangoz - libmz/string.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>

static int grow_string(struct mz_string *s, size_t len);
#if 1
#define is_whitespace(c) isspace(c)
#else
static mz_bool is_whitespace(char c);
#endif

static char *empty = "";

int mz_string_init(struct mz_string *s, size_t size)
{
	assert(s != NULL);

	if (size) {
		s->str = mz_malloc(size);

		if (s->str == NULL)
			return -MZ_EALLOC;

		*s->str = '\0';
	} else {
		s->str = empty;
	}

	s->len = 0;
	s->n = size;

	return 0;
}

void mz_string_free(struct mz_string *s)
{
	assert(s != NULL);
	assert(s->str != NULL);

	if (s->str != empty)
		mz_free(s->str);
}

void mz_string_clear(struct mz_string *s)
{
	assert(s != NULL);
	assert(s->str != NULL);

	if (s->str != empty) {
		s->str[0] = '\0';
		s->len = 0;
	}
}

int mz_string_set(struct mz_string *s, const char *str, size_t len)
{
	assert(s != NULL);
	assert(str != NULL);

	MZE(grow_string(s, len + 1));
	memcpy(s->str, str, len);
	s->len = len;
	s->str[len] = '\0';

	return 0;
}

int mz_string_cat(struct mz_string *s, const char *str, size_t len)
{
	size_t total_len;

	assert(s != NULL);
	assert(str != NULL);

	total_len = s->len + len;
	MZE(grow_string(s, total_len + 1));
	memcpy(s->str + s->len, str, len);
	s->str[total_len] = '\0';
	s->len = total_len;

	return 0;
}

int mz_string_cat_str(struct mz_string *s, const struct mz_string *z)
{
	assert(s != NULL);
	assert(z != NULL);

	return mz_string_cat(s, z->str, z->len);
}

int mz_string_cat_html(struct mz_string *s, const char *str)
{
	struct html_entity {
		char c;
		char *entity;
		size_t len;
	};
	static const struct html_entity entities[] = {
		{ '"',  MZ_STRLEN("&quot;") },
		{ '<',  MZ_STRLEN("&lt;")   },
		{ '>',  MZ_STRLEN("&gt;")   },
		{ '&',  MZ_STRLEN("&amp;")  },
	};
	static const struct html_entity * const end = MZ_ARRAY_END(entities);
	const char *start;
	const char *cur;
	const struct html_entity *it;

	assert(s != NULL);
	assert(str != NULL);

	for (cur = start = str; *cur != '\0'; ++cur) {
		for (it = entities; it != end; ++it) {
			if (it->c == *cur) {
				MZE(mz_string_cat(s, start, cur - start));
				MZE(mz_string_cat(s, it->entity, it->len));
				start = cur + 1;
			}
		}
	}

	if (cur > start)
		MZE(mz_string_cat(s, start, (cur - start)));

	return 0;
}

int mz_string_cat_file(struct mz_string *s, const char *file_name)
{
	FILE *f;
	long fsize;
	size_t n_read;
	int ret;

	assert(s != NULL);
	assert(file_name != NULL);

	f = fopen(file_name, "r");

	if (f == NULL)
		return -MZ_EIO;

	if (fseek(f, 0, SEEK_END)) {
		fclose(f);
		return -MZ_EIO;
	}

	fsize = ftell(f);

	if (fsize < 0) {
		fclose(f);
		return -MZ_EIO;
	}

	ret = grow_string(s, s->len + fsize + 1);

	if (ret) {
		fclose(f);
		return ret;
	}

	rewind(f);
	n_read = fread(s->str + s->len, 1, fsize, f);

	if (n_read != fsize) {
		ret = -MZ_EIO;
	} else {
		ret = 0;
		s->len += n_read;
		s->str[s->len] = '\0';
	}

	fclose(f);

	return ret;
}

#if MZ_USE_SNPRINTF
/* ToDo: make it build in ANSI (no libc vsnprintf) */
int mz_string_printf(struct mz_string *s, const char *fmt, ...)
{
	va_list ap;

	assert(s != NULL);

	va_start(ap, fmt);

	s->len = vsnprintf(s->str, s->n, fmt, ap);

	if (s->len >= s->n) {
		MZE(grow_string(s, s->len + 1));
		s->len = vsnprintf(s->str, s->n, fmt, ap);
	}

	assert(s->len < s->n);
	assert(s->str[s->len] == '\0');

	va_end(ap);

	return 0;
}
#endif

int mz_string_cmp(const struct mz_string *s, const char *z, size_t len)
{
	size_t i;
	size_t cmp_len;
	const char *str;

	assert(s != NULL);
	assert(z != NULL);

	cmp_len = (s->len < len) ? s->len : len;
	str = s->str;

	for (i = 0; (i < cmp_len) && (str[i] == z[i]); ++i);

	if (s->len >= len)
		return s->len - i;
	else
		return i - len;
}

int mz_string_cmp_str(const struct mz_string *s, const struct mz_string *z)
{
	assert(s != NULL);
	assert(z != NULL);

	return mz_string_cmp(s, z->str, z->len);
}

mz_bool mz_string_is_whitespace(const char *str, size_t len)
{
	assert(str != NULL);

	while (len--)
		if (is_whitespace(*str++))
			return false;

	return true;
}

void mz_string_get_trimmed(const char *str, size_t str_len,
			   const char **tmd, size_t *tmd_len)
{
	const char *it;
	const char *end;

	assert(str != NULL);
	assert(tmd != NULL);
	assert(tmd_len != NULL);

	end = str + str_len;

	for (it = str; it != end; ++it)
#if 0
		if (!isspace(*it))
#else
		if (!is_whitespace(*it))
#endif
			break;

	if (it == end) {
		*tmd = NULL;
		*tmd_len = 0;
		return;
	}

	*tmd = it;

	for (it = (end - 1); it != *tmd; --it)
		if (!is_whitespace(*it))
			break;

	*tmd_len = it + 1 - *tmd;
}

void mz_string_get_trimmed_str(const struct mz_string *s, const char **tmd,
			       size_t *tmd_len)
{
	assert(s != NULL);

	mz_string_get_trimmed(s->str, s->len, tmd, tmd_len);
}

int mz_string_split(const struct mz_string *s, struct mz_vector *v, char sep)
{
	const char *line, *it, *last, *end;

	assert(s != NULL);
	assert(v != NULL);

	line = s->str;
	last = line + s->len;
	end = last + 1;

	for (it = line; it != end; ++it) {
		size_t line_len;
		const char *tmd;
		size_t tmd_len;

		if ((*it != sep) && (it != last))
			continue;

		line_len = it - line;
		mz_string_get_trimmed(line, line_len, &tmd, &tmd_len);

		if (tmd != NULL) {
			struct mz_string *s = mz_vector_alloc_end(v, 1);

			if (s == NULL)
				return -MZ_EALLOC;

			MZE(mz_string_init(s, tmd_len + 1));
			MZE(mz_string_set(s, tmd, tmd_len));
		}

		line = it + 1;
	}

	return 0;
}

mz_bool mz_string_to_bool(const struct mz_string *s)
{
	assert(s != NULL);

	if (!mz_string_cmp(s, MZ_STRLEN("true")))
		return true;

	return false;
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static int grow_string(struct mz_string *s, size_t n)
{
	if (s->n >= n)
		return 0;

	if (s->str == empty)
		s->str = mz_malloc(n);
	else
		s->str = mz_realloc(s->str, n);

	if (s->str == NULL) {
		s->n = 0;
		return -MZ_EALLOC;
	}

	s->n = n;

	return 0;
}

#if 0
static mz_bool is_whitespace(char c)
{
	static const char spaces[] = { 0x20, 0x0A, 0x09, 0x0D, 0x00, 0x0B };
	static const size_t len = sizeof(spaces);
	int i;

	for (i = 0; i < len; ++i)
		if (c == spaces[i])
			return true;

	return false;
}
#endif

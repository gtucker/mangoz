/* Mangoz - libmz/xtext.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>
#include <mangoz/libmz/private.h>
#include <assert.h>
#include <string.h>
#include <stdio.h> /* debug */

#define LOG(msg, ...) \
	fprintf(stderr, "[xtext    %4i] "msg"\n", __LINE__, ##__VA_ARGS__)

static int set_text(struct mz_xtext *x, const char *text, size_t len);
static void cleanup(struct mz_xtext *x);
static int update_split(struct mz_xtext *x);
static int produce_split(struct mz_xtext *x, void *printer, unsigned indent,
			 mz_bool single);

int mz_xtext_init(struct mz_xtext *x, const char *text, size_t len,
		  mz_bool do_format)
{
	assert(x != NULL);
	assert(x->xnode.init == false);
	assert(text != NULL);

#if MZ_LOG_ALLOC
	LOG("%p init (%s)", &x->xnode, text);
#endif

	x->text.str = NULL;
	x->split.data = NULL;

	MZX(x, mz_xnode_init(&x->xnode, MZ_XNODE_TEXT, do_format));
	MZX(x, mz_string_init(&x->text, (len + 1)));
	MZX(x, set_text(x, text, len));

	return 0;
}

void mz_xtext_free(struct mz_xtext *x)
{
	assert(x != NULL);
	assert(x->xnode.init == true);

#if MZ_LOG_ALLOC
	LOG("%p free", &x->xnode);
#endif

	cleanup(x);
}

int mz_xtext_set(struct mz_xtext *x, const char *text, size_t len)
{
	assert(x != NULL);
	assert(x->xnode.init);
	assert(text != NULL);

	return set_text(x, text, len);
}

int mz_xtext_set_html(struct mz_xtext *x, const char *str)
{
	assert(x != NULL);
	assert(x->xnode.init);
	assert(str != NULL);

	mz_string_clear(&x->text);
	MZE(mz_string_cat_html(&x->text, str));

	return update_split(x);
}

int mz_xtext_set_from_file(struct mz_xtext *x, const char *file_name)
{
	assert(x != NULL);
	assert(x->xnode.init);
	assert(file_name != NULL);

	mz_string_clear(&x->text);
	MZE(mz_string_cat_file(&x->text, file_name));

	return update_split(x);
}

mz_bool mz_xtext_equals(struct mz_xtext *x, struct mz_xtext *y)
{
	struct mz_string *a;
	struct mz_string *b;
	int n;
	int i;

	assert(x != NULL);
	assert(x->xnode.init);
	assert(y != NULL);
	assert(y->xnode.init);

	n = x->split.n;

	if (n != y->split.n)
		return false;

	for (i = 0, a = x->split.data, b = y->split.data; i < n; ++i, ++a, ++b)
		if (mz_string_cmp_str(a, b))
		    return false;

	return true;
}

int mz_xtext_do_produce(struct mz_xtext *x, void *printer, unsigned indent)
{
	assert(x != NULL);
	assert(x->xnode.init);

	if (!x->xnode.do_format) {
		return mz_callbacks.printer_produce(
			printer, x->text.str, x->text.len);
	}

	return produce_split(x, printer, indent, false);
}

int mz_xtext_do_produce_single(struct mz_xtext *x, void *printer,
			       unsigned indent)
{
	int (* const produce)(void *printer, const char *str, size_t len) =
		mz_callbacks.printer_produce;

	assert(x != NULL);
	assert(x->xnode.init);

	if (!x->xnode.do_format) {
		MZCB(produce(printer, x->text.str, x->text.len));
	} else if (x->split.n == 1) {
		struct mz_string *str = x->split.data;
		MZCB(produce(printer, str->str, str->len));
	} else {
		MZCB(produce(printer, MZ_STRLEN("\n")));
		MZE(produce_split(x, printer, indent, true));
	}

	return 0;
}

int mz_make_indent_str(struct mz_string *s, size_t indent)
{
	const size_t total_len = (indent * (sizeof(INDENT) - 1)) + 1;
	int i;

	assert(s != NULL);

	MZE(mz_string_init(s, total_len));

	for (i = 0; i < indent; ++i)
		MZE(mz_string_cat(s, MZ_STRLEN(INDENT)));

	return 0;
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static int set_text(struct mz_xtext *x, const char *text, size_t len)
{
	MZE(mz_string_set(&x->text, text, len));

	return update_split(x);
}

static void cleanup(struct mz_xtext *x)
{
	if (x->text.str != NULL)
		mz_string_free(&x->text);

	if (x->split.data != NULL)
		mz_vector_free(&x->split);

	if (x->xnode.init)
		mz_xnode_free(&x->xnode);
}

static int update_split(struct mz_xtext *x)
{
	if (x->split.data == NULL) {
		int res;

		res = mz_vector_init(&x->split, sizeof(struct mz_string), 1);

		if (res < 0)
			return res;
	} else {
		mz_vector_clear(&x->split);
	}

	return mz_string_split(&x->text, &x->split, '\n');
}

static int produce_split(struct mz_xtext *x, void *printer, unsigned indent,
			 mz_bool single)
{
	int (* const produce)(void *printer, const char *str, size_t len) =
		mz_callbacks.printer_produce;
	struct mz_string *line;
	struct mz_string indent_str;

	MZE(mz_make_indent_str(&indent_str, indent));

	mz_vector_for_each(x->split, line) {
		MZE(produce(printer, indent_str.str, indent_str.len));
		MZE(produce(printer, line->str, line->len));
		MZE(produce(printer, MZ_STRLEN("\n")));
	}

	if (indent && single) {
		struct mz_string indent_str_2;

		MZE(mz_make_indent_str(&indent_str_2, indent - 1));
		MZE(produce(printer, indent_str_2.str, indent_str_2.len));
		mz_string_free(&indent_str_2);
	}

	mz_string_free(&indent_str);

	return 0;
}

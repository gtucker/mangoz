/* Mangoz - libmz/xnode.c
   Copyright (C) 2011, 2012 Guillaume Tucker
   This program is released under the terms defined in the GNU General Public
   Licence v3.  See COPYRIGHT and LICENSE for more details.
*/

#include <mangoz/libmz/libmz.h>
#include <mangoz/libmz/util.h>
#include <assert.h>

int mz_xnode_init(struct mz_xnode *x, enum mz_xnode_type node_type,
		  mz_bool do_format)
{
	x->node_type = node_type;
	x->do_format = do_format;
	x->init = true;

	return 0;
}

void mz_xnode_free(struct mz_xnode *x)
{
	x->init = false;
}

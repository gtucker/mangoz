# Mangoz - Makefile
# Copyright (C) 2011, 2012 Guillaume Tucker
# This program is released under the terms defined in the GNU General Public
# Licence v3.  See COPYRIGHT and LICENSE for more details.

subdirs := libmz boost php python test

CFLAGS += -Werror

all: print-arch sub-make

include $(MZ_BUILD)/builder.mk
